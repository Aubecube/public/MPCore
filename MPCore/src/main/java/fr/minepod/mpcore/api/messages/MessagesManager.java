/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.messages;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minepod.mpcore.MPCore;

// TODO: Auto-generated Javadoc
/**
 * The Class MessagesManager.
 */
public class MessagesManager {

  /** The formatting. */
  String formatting;

  /** The prefix. */
  String prefix;

  /** The message. */
  String message;

  /**
   * Instantiates a new messages manager.
   */
  public MessagesManager() {
    this("");
  }

  /**
   * Instantiates a new messages manager.
   * 
   * @param prefix the prefix
   */
  public MessagesManager(String prefix) {
    this(ChatColor.DARK_PURPLE + "[" + prefix + "] ", "%prefix%" + ChatColor.GOLD + "%message%",
        "");
  }

  /**
   * Instantiates a new messages manager.
   * 
   * @param prefix the prefix
   * @param message the message
   */
  public MessagesManager(String prefix, String message) {
    this(ChatColor.DARK_PURPLE + "[" + prefix + "] ", "%prefix%" + ChatColor.GOLD + "%message%",
        message);
  }

  /**
   * Instantiates a new messages manager.
   * 
   * @param prefix the prefix
   * @param formatting the formatting
   * @param message the message
   */
  public MessagesManager(String prefix, String formatting, String message) {
    this.prefix = prefix;
    this.formatting = formatting;
    this.message = message;
  }

  /**
   * Gets the formatting.
   * 
   * @return the formatting
   */
  public String getFormatting() {
    return formatting;
  }

  /**
   * Sets the formatting.
   * 
   * @param formatting the new formatting
   */
  public void setFormatting(String formatting) {
    this.formatting = formatting;
  }

  /**
   * Gets the prefix.
   * 
   * @return the prefix
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * Sets the prefix.
   * 
   * @param prefix the new prefix
   */
  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  /**
   * Gets the message.
   * 
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * Sets the message.
   * 
   * @param message the new message
   */
  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * Gets the formatted message.
   * 
   * @param message the message
   * @return the formatted message
   */
  public String getFormatted(String message) {
    this.message = message;
    return getFormatted();
  }

  /**
   * Gets the formatted message.
   * 
   * @return the formatted message
   */
  public String getFormatted() {
    if (MPCore.getConfigData().isMessagesPluginPrefixEnabled()) {
      return formatting.replace("%message%", message).replace("%prefix%", prefix);
    } else {
      return formatting.replace("%message%", message).replace("%prefix%", "");
    }
  }

  /**
   * Sends the formatted message.
   * 
   * @param sender the sender
   */
  public void sendFormatted(CommandSender sender) {
    sender.sendMessage(getFormatted());
  }

  /**
   * Sends the formatted message.
   * 
   * @param player the player
   */
  public void sendFormatted(Player player) {
    player.sendMessage(getFormatted());
  }

  /**
   * Sends the formatted message.
   * 
   * @param sender the sender
   * @param message the message
   */
  public void sendFormatted(CommandSender sender, String message) {
    this.message = message;
    sendFormatted(sender);
  }

  /**
   * Sends the formatted message.
   * 
   * @param player the player
   * @param message the message
   */
  public void sendFormatted(Player player, String message) {
    this.message = message;
    sendFormatted(player);
  }
}
