/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Player;

import fr.minepod.mpcore.api.nms.internal.IPacketPlayOutPlayerListHeaderFooterManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.api.nms.internal.PlayerConnectionWrapper;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class PlayerListHeaderFooterManipulator.
 */
public class PlayerListHeaderFooterManipulator {

  /** The internal. */
  static IPacketPlayOutPlayerListHeaderFooterManipulator internal;

  static {
    try {
      internal = NMSBase.getVersionedClass("PacketPlayOutPlayerListHeaderFooterManipulator",
          IPacketPlayOutPlayerListHeaderFooterManipulator.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Gets the packet.
   *
   * @param header the header
   * @param footer the footer
   * @return the packet
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws ClassNotFoundException the class not found exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object getPacket(String header, String footer) throws IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, InstantiationException,
      ClassNotFoundException, NoSuchMethodException, SecurityException, NoSuchFieldException {
    Constructor<?> packet = internal.getPacketPlayOutPlayerListHeaderFooterClass()
        .getDeclaredConstructor(ChatSerializerManipulator.getIChatComponentClass());

    Object instance = packet.newInstance(ChatSerializerManipulator.getIChatBaseComponent(header));

    Reflection.setDeclaredField(instance, internal.footerField(),
        ChatSerializerManipulator.getIChatBaseComponent(footer));

    return instance;
  }

  /**
   * Send packet.
   *
   * @param player the player
   * @param header the header
   * @param footer the footer
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   * @throws ClassNotFoundException the class not found exception
   * @throws InstantiationException the instantiation exception
   */
  public static void sendPacket(Player player, String header, String footer)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchFieldException,
      ClassNotFoundException, InstantiationException {
    PlayerConnectionWrapper.sendPacket(player,
        internal.getPacketPlayOutPlayerListHeaderFooterClass(), getPacket(header, footer));
  }

  /**
   * Gets the packet play out player list header footer class.
   *
   * @return the packet play out player list header footer class
   * @throws ClassNotFoundException the class not found exception
   */
  public static Class<?> getPacketPlayOutPlayerListHeaderFooterClass()
      throws ClassNotFoundException {
    return internal.getPacketPlayOutPlayerListHeaderFooterClass();
  }
}
