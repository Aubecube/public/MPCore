/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data.configs;

import java.lang.annotation.Annotation;

// TODO: Auto-generated Javadoc
/**
 * The Class DynamicConfigDataLoader.
 */
public class DynamicConfigDataLoader implements ConfigDataLoader {

  /** The special. */
  boolean special;

  /** The path. */
  String path;

  /** The type. */
  ConfigDataLoaderType type;

  /** The transform. */
  boolean transform;

  /**
   * Instantiates a new dynamic config data loader.
   *
   * @param loader the loader
   */
  public DynamicConfigDataLoader(ConfigDataLoader loader) {
    this(loader.special(), loader.path(), loader.type(), loader.transform());
  }

  /**
   * Instantiates a new dynamic config data loader.
   *
   * @param special the special
   * @param path the path
   * @param type the type
   * @param transform the transform
   */
  public DynamicConfigDataLoader(boolean special, String path, ConfigDataLoaderType type,
      boolean transform) {
    this.special = special;
    this.path = path;
    this.type = type;
    this.transform = transform;
  }

  /**
   * Sets the special.
   *
   * @param special the new special
   */
  public void setSpecial(boolean special) {
    this.special = special;
  }

  /**
   * Sets the path.
   *
   * @param path the new path
   */
  public void setPath(String path) {
    this.path = path;
  }

  /**
   * Sets the type.
   *
   * @param type the new type
   */
  public void setType(ConfigDataLoaderType type) {
    this.type = type;
  }

  /**
   * Sets the transform.
   *
   * @param transform the new transform
   */
  public void setTransform(boolean transform) {
    this.transform = transform;
  }

  /**
   * Annotation type.
   *
   * @return the class<? extends annotation>
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.annotation.Annotation#annotationType()
   */
  @Override
  public Class<? extends Annotation> annotationType() {
    return DynamicConfigDataLoader.class;
  }

  /**
   * Special.
   *
   * @return true, if successful
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.configs.ConfigDataLoader#special()
   */
  @Override
  public boolean special() {
    return special;
  }

  /**
   * Path.
   *
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.configs.ConfigDataLoader#path()
   */
  @Override
  public String path() {
    return path;
  }

  /**
   * Type.
   *
   * @return the config data loader type
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.configs.ConfigDataLoader#type()
   */
  @Override
  public ConfigDataLoaderType type() {
    return type;
  }

  /**
   * Transform.
   *
   * @return true, if successful
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.configs.ConfigDataLoader#transform()
   */
  @Override
  public boolean transform() {
    return transform;
  }
}
