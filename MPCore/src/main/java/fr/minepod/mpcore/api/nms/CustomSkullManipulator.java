/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.util.Base64;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class CustomSkullManipulator.
 */
public class CustomSkullManipulator {

  /** The url. */
  String url;

  /** The name. */
  String name;

  /**
   * Instantiates a new custom skull manipulator.
   *
   * @param url the url
   * @param name the name
   */
  @Deprecated
  public CustomSkullManipulator(String url, String name) {
    this.url = url;
    this.name = name;
  }

  /**
   * Sets the skull and returns a new itemstack.
   *
   * @return the item stack
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalStateException the illegal state exception
   */
  @Deprecated
  public ItemStack giveSkull() throws NoSuchFieldException, SecurityException,
      IllegalArgumentException, IllegalAccessException, IllegalStateException {
    return giveSkull(url, name);
  }

  /**
   * Give skull.
   *
   * @param url the url
   * @param display the display
   * @return the item stack
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalStateException the illegal state exception
   */
  public static ItemStack giveSkull(String url, String display) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException, IllegalStateException {
    byte[] data =
        Base64.getEncoder().encode(("{textures:{SKIN:{url:\"" + url + "\"}}}").getBytes());

    char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    StringBuilder builder = new StringBuilder();
    Random random = new Random();
    for (int i = 0; i < 10; i++) {
      char c = chars[random.nextInt(chars.length)];
      builder.append(c);
    }

    return giveSkull(new Property("textures", new String(data)), display);
  }

  /**
   * Give skull.
   *
   * @param textures the textures
   * @param display the display
   * @return the item stack
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalStateException the illegal state exception
   */
  public static ItemStack giveSkull(Property textures, String display) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException, IllegalStateException {
    char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    StringBuilder builder = new StringBuilder();
    Random random = new Random();
    for (int i = 0; i < 10; i++) {
      char c = chars[random.nextInt(chars.length)];
      builder.append(c);
    }

    GameProfile profile = new GameProfile(UUID.randomUUID(), builder.toString());
    if (profile.getProperties() == null) {
      throw new IllegalStateException("No properties");
    }

    profile.getProperties().put("textures", textures);

    ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);

    SkullMeta meta = (SkullMeta) item.getItemMeta();

    Reflection.setDeclaredField(meta, "profile", profile);
    meta.setDisplayName(display);

    item.setItemMeta(meta);

    return item;
  }
}
