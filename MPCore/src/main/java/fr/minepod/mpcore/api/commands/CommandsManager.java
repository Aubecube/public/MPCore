/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.commands;

import org.bukkit.command.CommandSender;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandsManager.
 */
public class CommandsManager {

  /** The sender. */
  CommandSender sender;

  /** The args. */
  String[] args;

  /**
   * Instantiates a new commands manager.
   * 
   * @param sender the sender
   * @param args the args
   */
  public CommandsManager(CommandSender sender, String... args) {
    setSender(sender);
    setArgs(args);
  }

  /**
   * Gets the sender.
   * 
   * @return the sender
   */
  public CommandSender getSender() {
    return sender;
  }

  /**
   * Sets the sender.
   * 
   * @param sender the new sender
   */
  public void setSender(CommandSender sender) {
    this.sender = sender;
  }

  /**
   * Gets the args.
   * 
   * @return the args
   */
  public String[] getArgs() {
    return args;
  }

  /**
   * Sets the args.
   * 
   * @param args the new args
   */
  public void setArgs(String... args) {
    this.args = args;
  }
}
