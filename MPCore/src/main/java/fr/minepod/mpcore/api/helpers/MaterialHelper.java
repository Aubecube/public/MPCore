package fr.minepod.mpcore.api.helpers;

import org.bukkit.Material;

public class MaterialHelper {
  public static boolean isSign(Material material, SignType type) {
    switch (type) {
      case WALL:
        return material.toString().contains("_WALL_SIGN");
      case POST:
        return material.toString().contains("_SIGN") && !material.toString().contains("_WALL_SIGN");
      case EITHER:
        return material.toString().contains("_SIGN");
      default:
        return false;
    }
  }
}
