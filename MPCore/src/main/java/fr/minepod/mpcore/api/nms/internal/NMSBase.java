/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.internal;

import org.bukkit.Bukkit;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class NMSBase.
 */
public class NMSBase {

  /** The Constant nms. */
  public static final String nms = "net.minecraft.server";

  /** The Constant latest. */
  public static final String latest = "v1_14_R1";

  /**
   * Gets the full bukkit package.
   *
   * @return the full bukkit package
   */
  public static String getBukkit() {
    return Bukkit.getServer().getClass().getPackage().getName();
  }

  /**
   * Gets the NMS version.
   *
   * @return the NMS version
   */
  public static String getNMSVersion() {
    return getBukkit().substring(getBukkit().lastIndexOf(".") + 1);
  }

  /**
   * Gets the full NMS package.
   *
   * @return the NMS package
   */
  public static String getNMS() {
    return nms + "." + getNMSVersion();
  }

  /**
   * Gets the versioned class.
   *
   * @param <T> the generic type
   * @param name the name
   * @param type the type
   * @return the versioned class
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws ClassNotFoundException the class not found exception
   */
  public static <T> T getVersionedClass(String name, Class<T> type)
      throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    Class<?> clazz =
        Reflection.getFoundClass("fr.minepod.mpcore.api.nms." + getNMSVersion() + "." + name);

    if (clazz == null) {
      clazz = Reflection.getFoundClass("fr.minepod.mpcore.api.nms." + latest + "." + name);

      if (clazz == null) {
        throw new ClassNotFoundException("The class " + name + " couldn't be found and casted to "
            + type.getCanonicalName() + ".");
      } else {
        MPCore.get().getLogger()
            .info("Using fallback NMS module (" + latest + ") for class " + name + ".");
      }
    } else {
      MPCore.get().getLogger()
          .info("Using dedicated NMS module (" + getNMSVersion() + ") for class " + name + ".");
    }

    return type.cast(clazz.newInstance());
  }
}
