/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Player;

import fr.minepod.mpcore.api.nms.internal.IPacketPlayOutWorldBorderManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.api.nms.internal.PlayerConnectionWrapper;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class WorldBorderPacketManipulator.
 */
public class WorldBorderPacketManipulator {

  /** The border. */
  static Object border;

  /** The action. */
  static Class<?> action;

  /** The packet. */
  static Constructor<?> packet;

  /** The internal. */
  static IPacketPlayOutWorldBorderManipulator internal;

  static {
    try {
      internal = NMSBase.getVersionedClass("PacketPlayOutWorldBorderManipulator",
          IPacketPlayOutWorldBorderManipulator.class);

      border = internal.getWorldBorderClass().newInstance();
      action = internal.getEnumWorldBorderActionClass();
      packet = internal.getPacketPlayOutWorldBorderClass().getDeclaredConstructor(border.getClass(),
          action);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Sets the center.
   *
   * @param x the x
   * @param z the z
   * @return the packet
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object setCenter(double x, double z) throws NoSuchMethodException,
      SecurityException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, InstantiationException, NoSuchFieldException {
    Reflection.getInvokeDeclaredMethod(border, internal.setCenterMethod(x, z), x, z);

    return packet.newInstance(border, Reflection.getDeclaredField(action, "SET_CENTER"));
  }

  /**
   * Sets the initial size.
   *
   * @param size the size
   * @return the packet
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object setInitialSize(double size) throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      InstantiationException, NoSuchFieldException {
    Reflection.getInvokeDeclaredMethod(border, internal.setInitialSizeMethod(size), size);

    return packet.newInstance(border, Reflection.getDeclaredField(action, "SET_SIZE"));
  }

  /**
   * Changes the size.
   *
   * @param oldSize the old size
   * @param newSize the new size
   * @param speed the speed
   * @return the packet
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object changeSize(double oldSize, double newSize, long speed)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, InstantiationException,
      NoSuchFieldException {
    Reflection.getInvokeDeclaredMethod(border, internal.changeSizeMethod(oldSize, newSize, speed),
        oldSize, newSize, speed);

    return packet.newInstance(border, Reflection.getDeclaredField(action, "LERP_SIZE"));
  }

  /**
   * Sets the warning blocks.
   *
   * @param blocks the blocks
   * @return the packet
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object setWarningBlocks(int blocks) throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      InstantiationException, NoSuchFieldException {
    Reflection.getInvokeDeclaredMethod(border, internal.setWarningBlocksMethod(blocks), blocks);

    return packet.newInstance(border, Reflection.getDeclaredField(action, "SET_WARNING_BLOCKS"));
  }

  /**
   * Sets the warning time.
   *
   * @param time the time
   * @return the packet
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object setWarningTime(int time) throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      InstantiationException, NoSuchFieldException {
    Reflection.getInvokeDeclaredMethod(border, internal.setWarningTimeMethod(time), time);

    return packet.newInstance(border, Reflection.getDeclaredField(action, "SET_WARNING_TIME"));
  }

  /**
   * Gets the initialize packet.
   *
   * @return the initialize packet
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object getInitializePacket()
      throws InstantiationException, IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, NoSuchMethodException, SecurityException, NoSuchFieldException {
    return packet.newInstance(border, Reflection.getDeclaredField(action, "INITIALIZE"));
  }

  /**
   * Sends the initialize packet to the player.
   *
   * @param player the player
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws NoSuchFieldException the no such field exception
   * @throws ClassNotFoundException the class not found exception
   */
  public static void sendInitializePacket(Player player) throws InstantiationException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      NoSuchMethodException, SecurityException, NoSuchFieldException, ClassNotFoundException {
    PlayerConnectionWrapper.sendPacket(player, internal.getPacketPlayOutWorldBorderClass(),
        getInitializePacket());
  }

  /**
   * Gets the packet play out world border class.
   *
   * @return the packet play out world border class
   * @throws ClassNotFoundException the class not found exception
   */
  public static Class<?> getPacketPlayOutWorldBorderClass() throws ClassNotFoundException {
    return internal.getPacketPlayOutWorldBorderClass();
  }
}
