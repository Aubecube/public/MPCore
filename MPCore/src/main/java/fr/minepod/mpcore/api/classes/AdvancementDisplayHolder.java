/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.classes;

import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new advancement display holder.
 */

/**
 * Instantiates a new advancement display holder.
 *
 * @param item the item
 * @param title the title
 * @param description the description
 * @param background the background
 * @param frame the frame
 * @param show_toast the show toast
 * @param hidden the hidden
 * @param x the x
 * @param y the y
 */

/**
 * Instantiates a new advancement display holder.
 *
 * @param item the item
 * @param title the title
 * @param description the description
 * @param background the background
 * @param frame the frame
 * @param show_toast the show toast
 * @param hidden the hidden
 * @param x the x
 * @param y the y
 */

/**
 * Instantiates a new advancement display holder.
 *
 * @param item the item
 * @param title the title
 * @param description the description
 * @param background the background
 * @param frame the frame
 * @param show_toast the show toast
 * @param hidden the hidden
 * @param x the x
 * @param y the y
 */
@RequiredArgsConstructor

/**
 * Gets the y.
 *
 * @return the y
 */

/**
 * Gets the y.
 *
 * @return the y
 */

/**
 * Gets the y.
 *
 * @return the y
 */
@Getter
public class AdvancementDisplayHolder {

  /** The item. */
  private final ItemStack item;

  /** The title. */
  private final String title;

  /** The description. */
  private final String description;

  /** The background. */
  // can be null
  private final String background;

  /** The frame. */
  private final FrameTypeHolder frame;

  /** The show toast. */
  // field "f"
  private final boolean show_toast;

  /** The g. */
  // useless field
  private final boolean g = false;

  /** The hidden. */
  // field "h
  private final boolean hidden;

  /** The x. */
  // field "i"
  private final float x;

  /** The y. */
  // field "j"
  private final float y;
}
