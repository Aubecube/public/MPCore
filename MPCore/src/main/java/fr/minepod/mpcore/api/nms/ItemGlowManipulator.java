/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class ItemGlowManipulator.
 */
public class ItemGlowManipulator {

  /** The item stack. */
  ItemStack itemStack;

  /** The compound. */
  static Object compound;

  /** The Craft item stack clazz. */
  static Class<?> CraftItemStackClazz;

  /** The NMS item stack clazz. */
  static Class<?> NMSItemStackClazz;

  /** The NBT tag compound clazz. */
  static Class<?> NBTTagCompoundClazz;

  /** The NBT tag list clazz. */
  static Class<?> NBTTagListClazz;

  /** The NBT base clazz. */
  static Class<?> NBTBaseClazz;

  static {
    try {
      CraftItemStackClazz = Class.forName(NMSBase.getBukkit() + ".inventory.CraftItemStack");

      NMSItemStackClazz = Class.forName(NMSBase.getNMS() + ".ItemStack");
      NBTTagCompoundClazz = Class.forName(NMSBase.getNMS() + ".NBTTagCompound");
      NBTTagListClazz = Class.forName(NMSBase.getNMS() + ".NBTTagList");
      NBTBaseClazz = Class.forName(NMSBase.getNMS() + ".NBTBase");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Instantiates a new item glow manipulator.
   *
   * @param item the item
   * @throws ClassNotFoundException the class not found exception
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  @Deprecated
  public ItemGlowManipulator(ItemStack item) throws ClassNotFoundException, InstantiationException,
      IllegalAccessException, NoSuchMethodException, SecurityException, IllegalArgumentException,
      InvocationTargetException {
    CraftItemStackClazz = Class.forName(NMSBase.getBukkit() + ".inventory.CraftItemStack");

    NMSItemStackClazz = Class.forName(NMSBase.getNMS() + ".ItemStack");
    NBTTagCompoundClazz = Class.forName(NMSBase.getNMS() + ".NBTTagCompound");
    NBTTagListClazz = Class.forName(NMSBase.getNMS() + ".NBTTagList");
    NBTBaseClazz = Class.forName(NMSBase.getNMS() + ".NBTBase");

    itemStack = item;
  }

  /**
   * Adds the glowing effect and returns the glowing new itemstack.
   *
   * @return the item stack
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  @Deprecated
  public ItemStack addGlowing()
      throws NoSuchMethodException, SecurityException, InstantiationException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    return addGlowingEffect(itemStack);
  }

  /**
   * Adds the glowing effect and returns the glowing new itemstack.
   *
   * @param item the item
   * @return the item stack
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws InstantiationException the instantiation exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static ItemStack addGlowingEffect(ItemStack item)
      throws NoSuchMethodException, SecurityException, InstantiationException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    try {
      // 1.11+
      item.addUnsafeEnchantment(Enchantment.LUCK, 1);

      ItemMeta meta = item.getItemMeta();
      meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
      item.setItemMeta(meta);

      return item;
    } catch (Exception ignored) {
      // Older
      Object nmsItemStack = Reflection.invokeMethod(null,
          Reflection.getDeclaredMethod(CraftItemStackClazz, "asNMSCopy", ItemStack.class), item);

      Object tag = Reflection.getInvokeDeclaredMethod(nmsItemStack, "getTag");

      if (tag == null) {
        tag = NBTTagCompoundClazz.newInstance();
      }

      Reflection.invokeMethod(tag,
          Reflection.getDeclaredMethod(NBTTagCompoundClazz, "set", String.class, NBTBaseClazz),
          "ench", NBTTagListClazz.newInstance());

      Reflection.getInvokeDeclaredMethod(nmsItemStack, "setTag", tag);

      return (ItemStack) Reflection.invokeMethod(null,
          Reflection.getDeclaredMethod(CraftItemStackClazz, "asCraftMirror", NMSItemStackClazz),
          nmsItemStack);
    }
  }
}
