/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.v1_11_R1;

import fr.minepod.mpcore.api.nms.internal.IPacketPlayOutTitleManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;

// TODO: Auto-generated Javadoc
/**
 * The Class PacketPlayOutTitleManipulator.
 */
public class PacketPlayOutTitleManipulator implements IPacketPlayOutTitleManipulator {

  /**
   * Gets the packet play out title class.
   *
   * @return the packet play out title class
   * @throws ClassNotFoundException the class not found exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IPacketPlayOutTitleManipulator#
   * getPacketPlayOutTitleClass()
   */
  @Override
  public Class<?> getPacketPlayOutTitleClass() throws ClassNotFoundException {
    return Class.forName(NMSBase.getNMS() + ".PacketPlayOutTitle");
  }

  /**
   * Gets the enum title action class.
   *
   * @return the enum title action class
   * @throws ClassNotFoundException the class not found exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IPacketPlayOutTitleManipulator#
   * getEnumTitleActionClass()
   */
  @Override
  public Class<?> getEnumTitleActionClass() throws ClassNotFoundException {
    return Class.forName(NMSBase.getNMS() + ".PacketPlayOutTitle$EnumTitleAction");
  }
}
