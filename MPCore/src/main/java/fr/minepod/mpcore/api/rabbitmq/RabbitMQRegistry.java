/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.rabbitmq;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.AMQP.BasicProperties;
import com.rabbitmq.client.AMQP.BasicProperties.Builder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import fr.minepod.mpcore.MPCore;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.jodah.lyra.ConnectionOptions;
import net.jodah.lyra.Connections;
import net.jodah.lyra.config.Config;
import net.jodah.lyra.config.RecoveryPolicy;
import net.jodah.lyra.config.RetryPolicy;
import net.jodah.lyra.util.Duration;

// TODO: Auto-generated Javadoc
/**
 * The Class RabbitMQRegistry.
 */
public class RabbitMQRegistry {

  /** The config. */
  private static Config config;

  /** The options. */
  private static ConnectionOptions options;

  /** The connection. */
  private static Connection connection;

  /** The channels. */
  private static Set<Channel> channels = Collections.synchronizedSet(new HashSet<Channel>());

  /** The threads. */
  private static Set<Thread> threads = Collections.synchronizedSet(new HashSet<Thread>());

  /** The listeners. */
  private static Set<ListenerHolder> listeners =
      Collections.synchronizedSet(new HashSet<ListenerHolder>());

  /** The enabled. */
  private static volatile boolean enabled = false;

  /** The received. */
  private static LoadingCache<String, Long> received =
      Caffeine.newBuilder().expireAfterWrite(5, TimeUnit.MINUTES).build(key -> {
        return null;
      });

  /**
   * Inits the.
   */
  public static void init() {
    config = new Config()
        .withRecoveryPolicy(
            new RecoveryPolicy().withBackoff(Duration.seconds(1), Duration.seconds(30)))
        .withRetryPolicy(new RetryPolicy().withInterval(Duration.seconds(1)));

    options = new ConnectionOptions().withHost(MPCore.getConfigData().getRabbitMQHost())
        .withPort(MPCore.getConfigData().getRabbitMQPort())
        .withUsername(MPCore.getConfigData().getRabbitMQUsername())
        .withPassword(MPCore.getConfigData().getRabbitMQPassword());
  }

  /**
   * Connect.
   *
   * @throws IOException Signals that an I/O exception has occurred.
   * @throws TimeoutException the timeout exception
   */
  public static void connect() throws IOException, TimeoutException {
    try {
      connection = Connections.create(options, config);

      for (ListenerHolder listener : listeners) {
        addListenerInternal(listener.getPlugin(), listener.getTag(), listener.getDuration(),
            listener.isSync(), listener.getLambda());
      }

      enabled = true;
    } catch (IOException e) {
      enabled = false;

      throw e;
    } catch (TimeoutException e) {
      enabled = false;

      throw e;
    }
  }

  /**
   * Reconnect.
   *
   * @throws IOException Signals that an I/O exception has occurred.
   * @throws TimeoutException the timeout exception
   */
  public static void reconnect() throws IOException, TimeoutException {
    close();

    connect();
  }

  /**
   * Close.
   */
  public static void close() {
    for (Channel channel : channels) {
      try {
        channel.close();
      } catch (Exception ignored) {
      }
    }

    if (connection != null) {
      try {
        connection.close();
      } catch (Exception ignored) {
      }
    }

    for (Thread thread : threads) {
      thread.interrupt();

      try {
        thread.join(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    channels.clear();
    threads.clear();
  }

  /**
   * Clear.
   */
  public static void clear() {
    listeners.clear();
  }

  /**
   * Checks if is enabled.
   *
   * @return true, if is enabled
   */
  public static boolean isEnabled() {
    return enabled;
  }

  /**
   * Sets the enabled.
   *
   * @param enabled the new enabled
   */
  public static void setEnabled(boolean enabled) {
    RabbitMQRegistry.enabled = enabled;
  }

  /**
   * Publish.
   *
   * @param plugin the plugin
   * @param tag the tag
   * @param duration the duration
   * @param map the map
   * @return true, if successful
   * @throws IOException Signals that an I/O exception has occurred.
   * @throws TimeoutException the timeout exception
   */
  public static boolean publish(JavaPlugin plugin, String tag, Duration duration,
      Map<String, String> map) throws IOException, TimeoutException {
    if (!isEnabled()) {
      return false;
    }

    Thread thread = new Thread(() -> {
      try {
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(plugin.getName(), "direct");

        map.put("tag", tag);
        map.put("timestamp", String.valueOf(System.currentTimeMillis()));

        String message =
            MPCore.getConfigData().getRabbitMQProfile() + ":" + new JSONObject(map).toJSONString();

        Builder builder = new AMQP.BasicProperties.Builder();

        if (duration != null) {
          builder.expiration(String.valueOf(duration.toMilliseconds()));
        }

        channel.basicPublish(plugin.getName(), tag, builder.build(), message.getBytes("UTF-8"));

        channel.close();
      } catch (IOException | TimeoutException e) {
        e.printStackTrace();
      }
    });
    thread.start();

    threads.add(thread);

    return true;
  }

  /**
   * Adds the listener.
   *
   * @param plugin the plugin
   * @param tag the tag
   * @param duration the message TTL, null to make it persistent
   * @param sync if the lambda must be applied synchronously or not
   * @param lambda the lambda
   * @return true, if successful
   */
  public static void addListener(JavaPlugin plugin, String tag, Duration duration, boolean sync,
      java.util.function.Consumer<Map<String, String>> lambda) {
    if (isEnabled()) {
      addListenerInternal(plugin, tag, duration, sync, lambda);
    }

    listeners.add(new ListenerHolder(plugin, tag, duration, sync, lambda));
  }

  /**
   * Adds the listener internal.
   *
   * @param plugin the plugin
   * @param tag the tag
   * @param duration the message TTL, null to make it persistent
   * @param sync if the lambda must be applied synchronously or not
   * @param lambda the lambda
   */
  private static void addListenerInternal(JavaPlugin plugin, String tag, Duration duration,
      boolean sync, java.util.function.Consumer<Map<String, String>> lambda) {
    Thread thread = new Thread(() -> {
      try {
        MPCore.get().getLogger().info("Listening for RabbitMQ updates with tag " + tag + "...");

        Channel channel = connection.createChannel();
        channels.add(channel);

        channel.exchangeDeclare(plugin.getName(), "direct");

        channel.queueDeclare(MPCore.getConfigData().getRabbitMQProfile(), false, true, true,
            new HashMap<>());
        channel.queueBind(MPCore.getConfigData().getRabbitMQProfile(), plugin.getName(), tag);

        channel.basicConsume(MPCore.getConfigData().getRabbitMQProfile(), true, new Consumer() {
          @Override
          public void handleCancel(String consumer) throws IOException {
            MPCore.get().getLogger().info("Consumer " + consumer + " was cancelled (other).");
          }

          @Override
          public void handleCancelOk(String consumer) {
            MPCore.get().getLogger().info("Consumer " + consumer + " was cancelled.");
          }

          @Override
          public void handleConsumeOk(String consumer) {
            MPCore.get().getLogger().info("Consumer " + consumer + " registered.");
          }

          @Override
          public void handleDelivery(String consumer, Envelope envelope, BasicProperties properties,
              byte[] body) throws IOException {
            String message = new String(body, "UTF-8");
            String[] parts = message.split(":", 2);

            if (parts[0].equalsIgnoreCase(MPCore.getConfigData().getRabbitMQProfile())) {
              return;
            }

            if (!plugin.isEnabled()) {
              return;
            }

            try {
              Map<String, String> map = (Map<String, String>) new JSONParser().parse(parts[1]);

              String filter = map.get("tag");

              if (filter.equals(tag)) {
                String timestamp = map.get("timestamp");

                String key = plugin.getName() + tag + timestamp;
                if (received.getIfPresent(key) == null) {
                  Long parsed = Long.valueOf(timestamp);

                  if (duration == null
                      || parsed + duration.toMilliseconds() >= System.currentTimeMillis()) {
                    if (sync) {
                      Bukkit.getScheduler().callSyncMethod(MPCore.get(), () -> {
                        try {
                          lambda.accept(map);
                        } catch (Exception e) {
                          e.printStackTrace();
                        }

                        return true;
                      });
                    } else {
                      try {
                        lambda.accept(map);
                      } catch (Exception e) {
                        e.printStackTrace();
                      }
                    }
                  }

                  received.put(key, System.currentTimeMillis());
                }
              }
            } catch (ParseException e) {
              e.printStackTrace();
            }
          }

          @Override
          public void handleRecoverOk(String consumer) {}

          @Override
          public void handleShutdownSignal(String consumer, ShutdownSignalException exception) {
            MPCore.get().getLogger().info("Consumer " + consumer + " was shut down.");
          }
        });
      } catch (IOException e) {
        e.printStackTrace();
      }
    });
    thread.start();

    threads.add(thread);
  }
}


@RequiredArgsConstructor
@Getter
class ListenerHolder {
  private final JavaPlugin plugin;
  private final String tag;
  private final Duration duration;
  private final boolean sync;
  private final java.util.function.Consumer<Map<String, String>> lambda;
}
