/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.internal;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Player;

import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class PlayerConnectionWrapper.
 */
public class PlayerConnectionWrapper {

  /** The packet. */
  static Class<?> packet;

  /** The craft player. */
  static Class<?> craftPlayer;

  static {
    try {
      packet = Class.forName(NMSBase.getNMS() + ".Packet");
      craftPlayer = Class.forName(NMSBase.getBukkit() + ".entity.CraftPlayer");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Send packet.
   *
   * @param player the player
   * @param cast the cast
   * @param packetToSend the packet to send
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static void sendPacket(Player player, Class<?> cast, Object packetToSend)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
    Object handle = Reflection.getInvokeMethod(craftPlayer.cast(player), "getHandle");
    Object playerConnection = Reflection.getDeclaredField(handle, "playerConnection");

    Reflection.invokeMethod(playerConnection,
        Reflection.getMethod(playerConnection, "sendPacket", packet), cast.cast(packetToSend));
  }
}
