/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data;

import java.io.IOException;
import java.lang.reflect.Method;

import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.data.configs.AbstractConfigDataManager;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class AutoDataManager.
 */
public class AutoDataManager {

  /**
   * On enable.
   *
   * @param plugin the plugin
   * @return true, if successful
   */
  public static boolean onEnable(JavaPlugin plugin) {
    boolean success = true;

    for (AbstractDataManager<?, ?> manager : AbstractDataManager.managers) {
      if (plugin == manager.getPlugin()) {
        if (manager.getLoad()) {
          plugin.getLogger().info("Loading " + manager.getFileName() + " for plugin "
              + manager.getPlugin().getName() + "...");
          manager.load();
          plugin.getLogger().info("Loaded " + manager.getFileName() + " for plugin "
              + manager.getPlugin().getName() + ".");

          if (manager.getForceSave()) {
            try {
              plugin.getLogger().info("Force-saving " + manager.getFileName() + " for plugin "
                  + manager.getPlugin().getName() + "...");
              manager.save();
              plugin.getLogger().info("Force-saved " + manager.getFileName() + " for plugin "
                  + manager.getPlugin().getName() + ".");
            } catch (IOException e) {
              plugin.getLogger().info("Could not force-save " + manager.getFileName()
                  + " for plugin " + manager.getPlugin().getName() + "!");
              e.printStackTrace();

              success = false;
            }
          }
        }
      }
    }

    for (AbstractConfigDataManager<?, ?, ?> manager : AbstractConfigDataManager.managers) {
      if (plugin == manager.getPlugin()) {
        if (manager.getType().isLoad()) {
          plugin.getLogger().info("Loading " + manager.getFile() + " for plugin "
              + manager.getPlugin().getName() + "...");
          manager.load();
          plugin.getLogger().info(
              "Loaded " + manager.getFile() + " for plugin " + manager.getPlugin().getName() + ".");

          if (manager.getType().isForceSave()) {
            for (Method method : manager.getClass().getMethods()) {
              try {
                if (manager.getProxiedMethods().contains(method.getName())) {
                  Reflection.invokeMethod(manager, method);
                }
              } catch (Exception e) {
                e.printStackTrace();
              }
            }

            try {
              plugin.getLogger().info("Force-saving " + manager.getFile().getAbsolutePath()
                  + " for plugin " + manager.getPlugin().getName() + "...");
              manager.save();
              plugin.getLogger().info("Force-saved " + manager.getFile().getAbsolutePath()
                  + " for plugin " + manager.getPlugin().getName() + ".");
            } catch (IOException e) {
              plugin.getLogger().info("Could not force-save " + manager.getFile().getAbsolutePath()
                  + " for plugin " + manager.getPlugin().getName() + "!");
              e.printStackTrace();

              success = false;
            }
          }
        }
      }
    }

    return success;

  }

  /**
   * On disable.
   *
   * @param plugin the plugin
   * @return true, if successful
   */
  public static boolean onDisable(JavaPlugin plugin) {
    boolean success = true;

    for (AbstractDataManager<?, ?> manager : AbstractDataManager.managers) {
      if (plugin == manager.getPlugin()) {
        if (manager.getSave()) {
          try {
            plugin.getLogger().info("Saving " + manager.getFileName() + " for plugin "
                + manager.getPlugin().getName() + "...");
            manager.save();
            plugin.getLogger().info("Saved " + manager.getFileName() + " for plugin "
                + manager.getPlugin().getName() + ".");
          } catch (IOException e) {
            plugin.getLogger().info("Could not save " + manager.getFileName() + " for plugin "
                + manager.getPlugin().getName() + "!");
            e.printStackTrace();

            success = false;
          }
        }
      }
    }

    for (AbstractConfigDataManager<?, ?, ?> manager : AbstractConfigDataManager.managers) {
      if (plugin == manager.getPlugin()) {
        if (manager.getType().isSave()) {
          try {
            plugin.getLogger().info("Saving " + manager.getFile().getAbsolutePath() + " for plugin "
                + manager.getPlugin().getName() + "...");
            manager.save();
            plugin.getLogger().info("Saved " + manager.getFile().getAbsolutePath() + " for plugin "
                + manager.getPlugin().getName() + ".");
          } catch (IOException e) {
            plugin.getLogger().info("Could not save " + manager.getFile().getAbsolutePath()
                + " for plugin " + manager.getPlugin().getName() + "!");
            e.printStackTrace();

            success = false;
          }
        }
      }
    }

    return success;
  }
}
