/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.integrations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

// TODO: Auto-generated Javadoc
/**
 * The Class DynamicDependency.
 */
public class DynamicDependency extends HashMap<String, Runnable> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4237714971852559916L;

  /** The Constant dependencies. */
  public static final List<DynamicDependency> dependencies = new ArrayList<>();

  /** The successes. */
  private Map<String, Boolean> successes = new HashMap<>();

  /**
   * Instantiates a new dynamic dependency.
   */
  public DynamicDependency() {
    super();

    dependencies.add(this);
  }

  /**
   * Run.
   */
  public void run() {
    for (String key : keySet()) {
      run(key);
    }
  }

  /**
   * Run.
   *
   * @param plugin the plugin
   */
  public void run(String plugin) {
    for (Plugin temp : Bukkit.getServer().getPluginManager().getPlugins()) {
      if (temp.getName().equalsIgnoreCase(plugin) && temp.isEnabled()) {
        Runnable runnable = get(plugin);

        if (runnable != null) {
          try {
            runnable.run();
            successes.put(plugin, true);
          } catch (Exception e) {
            e.printStackTrace();

            successes.put(plugin, false);
          }
        } else {
          successes.put(plugin, true);
        }

        break;
      }
    }
  }

  /**
   * Checks if is available.
   *
   * @param plugin the plugin
   * @return the optional
   */
  public Optional<Boolean> isAvailable(String plugin) {
    for (Entry<String, Boolean> entry : successes.entrySet()) {
      if (entry.getKey().equalsIgnoreCase(plugin)) {
        return Optional.of(entry.getValue());
      }
    }

    return Optional.empty();
  }

  /**
   * Sets the available.
   *
   * @param plugin the plugin
   * @param flag the flag
   */
  public void setAvailable(String plugin, boolean flag) {
    if (flag) {
      successes.put(plugin, flag);
    } else {
      for (Entry<String, Boolean> entry : successes.entrySet()) {
        if (entry.getKey().equalsIgnoreCase(plugin)) {
          entry.setValue(flag);

          return;
        }
      }
    }
  }
}
