/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data.configs;

// TODO: Auto-generated Javadoc
/**
 * The Enum ConfigDataLoaderType.
 */
public enum ConfigDataLoaderType {

  /** Detect the return type. */
  AUTODETECT,
  /** The boolean. */
  BOOLEAN,
  /** The double. */
  DOUBLE,
  /** The float. */
  FLOAT,
  /** The int. */
  INT,
  /** The list. */
  ITEMSTACK,
  /** The list. */
  LIST,
  /** The long. */
  LONG,
  /** The string. */
  STRING,
  /** The uuid. */
  UUID;
}
