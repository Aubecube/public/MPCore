/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.commands;

import org.bukkit.command.CommandSender;

import fr.minepod.mpcore.api.functions.FunctionsManager;
import fr.minepod.mpcore.api.permissions.PermissionsManager;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandsRegister.
 */
public abstract class CommandsRegister {

  /** The sender. */
  public CommandSender sender;

  /** The args. */
  public String[] args;

  /** The command. */
  public String command;

  /** The args number. */
  public int argsNumber;

  /** The permission. */
  public String permission;

  /** The types. */
  public Class<?>[] types;

  /**
   * Instantiates a new commands register.
   * 
   * @param commandsManager the commands manager
   * @param command the command
   * @param argsNumber the args number
   * @param permission the permission
   * @param types the types
   */
  public CommandsRegister(CommandsManager commandsManager, String command, int argsNumber,
      final String permission, Class<?>... types) {
    this.sender = commandsManager.getSender();
    this.args = commandsManager.getArgs();
    this.command = command;
    this.argsNumber = argsNumber;
    this.permission = permission;
    this.types = types;
  }

  /**
   * Check command.
   * 
   * @return true, if successful
   */
  private boolean checkCommand() {
    if (args.length >= 1) {
      return (args[0].equalsIgnoreCase(command));
    }

    return false;
  }

  /**
   * Check args.
   * 
   * @return true, if successful
   */
  private boolean checkArgs() {
    return argsNumber + 1 <= args.length;
  }

  /**
   * Check types.
   * 
   * @return true, if successful
   */
  private boolean checkTypes() {
    if (types.length > 0) {
      for (int i = 0; i < types.length; i++) {
        if (types[i] == Integer.class) {
          if (!FunctionsManager.isInteger(args[i - 1])) {
            return false;
          }
        } else if (types[i] == Double.class) {
          if (!FunctionsManager.isDouble(args[i - 1])) {
            return false;
          }
        } else if (types[i] == Float.class) {
          if (!FunctionsManager.isFloat(args[i - 1])) {
            return false;
          }
        } else if (types[i] == Long.class) {
          if (!FunctionsManager.isLong(args[i - 1])) {
            return false;
          }
        } else {
          if (types[i] != args[i].getClass()) {
            return false;
          }
        }
      }
    }

    return true;
  }

  /**
   * Check permission.
   * 
   * @return true, if successful
   */
  private boolean checkPerms() {
    return PermissionsManager.hasSender(sender, permission);

  }

  /**
   * Execute.
   * 
   * @return true, if successful
   */
  public boolean execute() {
    if (checkCommand() && checkPerms()) {
      if (argsNumber == 0 || (checkArgs() && checkTypes())) {
        run();
        return true;
      }
    }

    return false;
  }

  /**
   * Run.
   */
  public abstract void run();
}
