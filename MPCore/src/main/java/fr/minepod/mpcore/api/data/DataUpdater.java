/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.bukkit.Server;

// TODO: Auto-generated Javadoc
/**
 * The Class DataUpdater.
 */
public class DataUpdater {

  /** The server. */
  Server server;

  /** The logger. */
  Logger logger;

  /** The map. */
  Map<String, Method> map = new HashMap<>();

  /**
   * Instantiates a new data updater.
   *
   * @param server the server
   */
  public DataUpdater(Server server) {
    this(server, server.getLogger());
  }

  /**
   * Instantiates a new data updater.
   *
   * @param server the server
   * @param logger the logger
   */
  public DataUpdater(Server server, Logger logger) {
    this.server = server;
    this.logger = logger;
  }

  /**
   * Update data.
   * 
   * @param currentVersion the current version
   * @param expectedVersion the expected version
   * @return true, if successful
   * @throws Exception the exception
   */
  public boolean updateData(String currentVersion, String expectedVersion) throws Exception {
    logger.info("Updating config version " + currentVersion + " to version " + expectedVersion);

    logger.info("Started pre-update...");
    preUpdate();
    logger.info("Finished pre-update...");

    currentVersion = currentVersion.replace(".", "x").toLowerCase();
    while (!currentVersion.equalsIgnoreCase(expectedVersion.replace(".", "x").toLowerCase())) {
      if (map.containsKey(currentVersion)) {
        Method method = map.get(currentVersion);
        method.setAccessible(true);
        currentVersion = (String) method.invoke(this);
      } else {
        fromVersionUnknown();
        return false;
      }
    }

    logger.info("Started post-update...");
    postUpdate();
    logger.info("Finished post-update...");

    logger.info("Config version updated !");
    return true;
  }

  /**
   * Pre update.
   */
  public void preUpdate() {

  }

  /**
   * Post update.
   */
  public void postUpdate() {

  }

  /**
   * From version unknown.
   * 
   * @return the string
   */
  private String fromVersionUnknown() {
    logger.severe("Unknown version!");
    return "Unknown";
  }

  /**
   * Adds the methods.
   */
  private void addMethods() {
    for (Method temp : this.getClass().getDeclaredMethods()) {
      if (temp.getName().startsWith("fromVersion")
          && !temp.getName().equalsIgnoreCase("fromVersionUnknown")) {
        logger.info("Found method: " + temp.getName());
        map.put(temp.getName().replace("fromVersion", "").toLowerCase(), temp);
      }
    }
  }

  /**
   * Adds the method.
   * 
   * @param method the method
   */
  public void addMethod(Method method) {
    logger.info("Adding custom method: " + method.getName());
    map.put(method.getName().replace("fromVersion", "").toLowerCase(), method);
  }

  /**
   * Execute.
   * 
   * @param currentVersion the current version
   * @param expectedVersion the expected version
   * @return true, if successful
   * @throws Exception the exception
   */
  public boolean execute(String currentVersion, String expectedVersion) throws Exception {
    addMethods();
    return updateData(currentVersion, expectedVersion);
  }
}
