/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Entity;

import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class RemoveCraftEntityManipulator.
 */
public class RemoveCraftEntityManipulator {

  /** The entity class. */
  static Class<?> entityClass;

  /** The craft entity. */
  static Class<?> craftEntity;

  static {
    try {
      entityClass = Class.forName(NMSBase.getNMS() + ".Entity");
      craftEntity = Class.forName(NMSBase.getBukkit() + ".entity.CraftEntity");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Removes the craft entity.
   *
   * @param entity the entity
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static void removeCraftEntity(Entity entity)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
    Object handle = Reflection.getInvokeMethod(craftEntity.cast(entity), "getHandle");
    Object world = Reflection.getField(handle, "world");

    Reflection.invokeMethod(world, Reflection.getMethod(world, "removeEntity", entityClass),
        entityClass.cast(handle));
  }
}
