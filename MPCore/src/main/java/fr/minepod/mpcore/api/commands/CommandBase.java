/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import javax.naming.NoPermissionException;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.classes.ObjectHolder;
import fr.minepod.mpcore.api.commands.exceptions.CommandArgumentException;
import fr.minepod.mpcore.api.commands.exceptions.WrongArgumentException;
import fr.minepod.mpcore.api.commands.exceptions.WrongCommandException;
import fr.minepod.mpcore.api.lambdas.LambdaFive;
import fr.minepod.mpcore.api.permissions.PermissionsManager;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandBase.
 */
public class CommandBase {

  /** The help. */
  private String help;

  /** The pattern. */
  private String pattern;

  /** The permission. */
  private String permission;

  /** The type. */
  private CommandType type;

  /** The lambda. */
  private LambdaFive<CommandSender, Command, String, String[], List<ObjectHolder>> lambda;

  /**
   * Instantiates a new command base.
   *
   * @param help the help
   * @param pattern the pattern
   * @param permission the permission
   * @param lambda the lambda
   */
  public CommandBase(String help, String pattern, String permission,
      LambdaFive<CommandSender, Command, String, String[], List<ObjectHolder>> lambda) {
    this.help = help;
    this.pattern = pattern;
    this.permission = permission;
    this.lambda = lambda;
  }

  /**
   * Instantiates a new command base.
   *
   * @param help the help
   * @param pattern the pattern
   * @param type the type
   * @param lambda the lambda
   */
  public CommandBase(String help, String pattern, CommandType type,
      LambdaFive<CommandSender, Command, String, String[], List<ObjectHolder>> lambda) {
    this.help = help;
    this.pattern = pattern;
    this.type = type;
    this.lambda = lambda;
  }

  /**
   * Process.
   *
   * @param args the args
   * @return the list
   * @throws CommandArgumentException the command argument exception
   * @throws WrongArgumentException the wrong argument exception
   * @throws WrongCommandException the wrong command exception
   */
  public List<ObjectHolder> process(String[] args)
      throws CommandArgumentException, WrongArgumentException, WrongCommandException {
    List<ObjectHolder> list = new ArrayList<>();

    int i = 0;
    for (String part : pattern.split(" ")) {
      if (args.length <= i) {
        throw new WrongArgumentException("There should be at least " + args.length + " arguments");
      }

      ObjectHolder holder = new ObjectHolder();

      /*
       * To create a variable: "[type=name]" or the shorthand "[name]" if type == String; to create
       * a constant: "name".
       * 
       * Everything that is not properly formatted will be treated as a constant.
       */
      if (part.startsWith("[") && part.endsWith("]") && part.length() > 2) {
        String stripped = part.substring(1, part.length() - 1);

        String[] tags = stripped.split("=");
        try {
          Switch: switch (tags[0].toLowerCase()) {
            case "int":
              holder.setIntValue(Integer.parseInt(args[i]));
              break;
            case "float":
              holder.setFloatValue(Float.parseFloat(args[i]));
              break;
            case "double":
              holder.setDoubleValue(Double.parseDouble(args[i]));
              break;
            case "long":
              holder.setLongValue(Long.parseLong(args[i]));
              break;
            case "uuid":
              holder.setUuidValue(UUID.fromString(args[i]));
              break;
            case "boolean":
              holder.setBooleanValue(Boolean.parseBoolean(args[i]));
              break;
            case "player":
              for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getName().equalsIgnoreCase(args[i])) {
                  holder.setPlayerValue(player);
                  break Switch;
                }
              }

              throw new CommandArgumentException(i, tags[0]);
            case "chain":
              StringBuilder builder = new StringBuilder(args[i]);

              if (args.length > i) {
                for (int iterator = i + 1; iterator < args.length; iterator++) {
                  builder.append(" " + args[iterator]);
                }
              }

              holder.setStringValue(builder.toString());
              break;
            default:
              holder.setStringValue(args[i]);
          }
        } catch (CommandArgumentException e) {
          throw e;
        } catch (Exception e) {
          throw new CommandArgumentException(i, tags[0]);
        }
      } else {
        if (part.equalsIgnoreCase(args[i])) {
          holder.setStringValue(args[i]);
        } else {
          throw new WrongCommandException(
              "Argument " + i + " '" + args[i] + "' is not equal to " + part);
        }
      }

      list.add(holder);

      i++;
    }

    return Collections.unmodifiableList(list);
  }

  /**
   * Execute.
   *
   * @param plugin the plugin
   * @param name the name
   * @param sender the sender
   * @param command the command
   * @param label the label
   * @param args the args
   * @throws CommandArgumentException the command argument exception
   * @throws WrongArgumentException the wrong argument exception
   * @throws WrongCommandException the wrong command exception
   * @throws NoPermissionException the no permission exception
   */
  public void execute(JavaPlugin plugin, String name, CommandSender sender, Command command,
      String label, String[] args) throws CommandArgumentException, WrongArgumentException,
      WrongCommandException, NoPermissionException {
    if ((type != null && PermissionsManager.hasSender(sender,
        plugin.getName() + "." + type.toString().toLowerCase() + "." + name))
        || (permission != null && PermissionsManager.hasSender(sender, permission))) {
      lambda.apply(sender, command, label, args, process(args));
    } else {
      throw new NoPermissionException(permission);
    }
  }

  /**
   * Gets the help.
   *
   * @return the help
   */
  public String getHelp() {
    return help;
  }

  /**
   * Gets the pattern.
   *
   * @return the pattern
   */
  public String getPattern() {
    return pattern;
  }

  /**
   * Gets the permission.
   *
   * @return the permission
   */
  public String getPermission() {
    return permission;
  }

  /**
   * Gets the type.
   * 
   * @return the type
   */
  public CommandType getType() {
    return type;
  }

  /**
   * Gets the lambda.
   *
   * @return the lambda
   */
  public LambdaFive<CommandSender, Command, String, String[], List<ObjectHolder>> getLambda() {
    return lambda;
  }
}

