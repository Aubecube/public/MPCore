/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.rethinkdb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Changes;
import com.rethinkdb.gen.ast.IndexCreate;
import com.rethinkdb.gen.exc.ReqlDriverError;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.Cursor;

import fr.minepod.mpcore.MPCore;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * The Class RethinkDBRegistry.
 */
public class RethinkDBRegistry {

  /** The builder. */
  private static Connection.Builder builder;

  /**
   * Gets the connection.
   *
   * @return the connection
   */

  /**
   * Gets the connection.
   *
   * @return the connection
   */

  /**
   * Gets the connection.
   *
   * @return the connection
   */

  /**
   * Gets the connection.
   *
   * @return the connection
   */
  @Getter
  private static Connection connection;

  /** The threads. */
  private static Set<Thread> threads = Collections.synchronizedSet(new HashSet<Thread>());

  /** The listeners. */
  private static Set<ListenerHolder> listeners =
      Collections.synchronizedSet(new HashSet<ListenerHolder>());

  /** The managers. */
  private static List<AbstractRethinkDataManager<?, ?>> managers = new ArrayList<>();

  /** The enabled. */
  private static volatile boolean enabled = false;

  /**
   * Inits.
   */
  public static void init() {
    builder = RethinkDB.r.connection().hostname(MPCore.getConfigData().getRethinkDBHost())
        .port(MPCore.getConfigData().getRethinkDBPort())
        .db(MPCore.getConfigData().getRethinkDBDatabase())
        .user(MPCore.getConfigData().getRethinkDBUsername(),
            MPCore.getConfigData().getRethinkDBPassword());
  }

  /**
   * Connect.
   *
   * @throws ReqlDriverError the reql driver error
   */
  public static void connect() throws ReqlDriverError {
    try {
      connection = builder.connect();

      try {
        RethinkDB.r.dbCreate(MPCore.getConfigData().getRethinkDBDatabase()).run(connection);
      } catch (Exception ignored) {
      }

      connection.use(MPCore.getConfigData().getRethinkDBDatabase());

      for (AbstractRethinkDataManager<?, ?> manager : managers) {
        try {
          RethinkDB.r.tableCreate(manager.getName()).run(RethinkDBRegistry.getConnection());
        } catch (Exception ignored) {
        }

        for (IndexCreate index : manager.createIndexes()) {
          try {
            index.run(RethinkDBRegistry.getConnection());
          } catch (Exception ignored) {
          }
        }

        try {
          if (manager.isPull()) {
            Cursor<Map<String, Object>> cursor = manager.getTable().run(connection);
            for (Object entry : cursor) {
              @SuppressWarnings("unchecked")
              Map<String, Object> map = (Map<String, Object>) entry;

              manager.put(map);
            }
          }
        } catch (Exception e) {
          e.printStackTrace();
        }
      }

      for (ListenerHolder listener : listeners) {
        addListenerInternal(listener.getName(), listener.getChanges(), listener.getConsumer());
      }

      enabled = true;
    } catch (ReqlDriverError e) {
      enabled = false;

      throw e;
    }
  }

  /**
   * Reconnect.
   *
   * @throws ReqlDriverError the reql driver error
   */
  public static void reconnect() throws ReqlDriverError {
    close();

    connect();

    if (!isEnabled()) {
      return;
    }
  }

  /**
   * Close.
   */
  public static void close() {
    stop();

    if (connection != null) {
      try {
        connection.close();
      } catch (Exception ignored) {
      }
    }
  }

  /**
   * Stop.
   */
  public static void stop() {
    for (Thread thread : threads) {
      thread.interrupt();

      try {
        thread.join(100);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    threads.clear();
  }

  /**
   * Clear.
   */
  public static void clear() {
    stop();

    listeners.clear();
  }

  /**
   * Checks if is enabled.
   *
   * @return true, if is enabled
   */
  public static boolean isEnabled() {
    return enabled;
  }

  /**
   * Sets the enabled.
   *
   * @param enabled the new enabled
   */
  public static void setEnabled(boolean enabled) {
    RethinkDBRegistry.enabled = enabled;
  }

  /**
   * Register.
   *
   * @param manager the manager
   */
  public static void register(AbstractRethinkDataManager<?, ?> manager) {
    managers.add(manager);
  }

  /**
   * Adds the listener.
   *
   * @param name the name
   * @param changes the changes
   * @param consumer the consumer
   * @return true, if successful
   */
  public static void addListener(String name, Changes changes,
      Consumer<Map<String, Object>> consumer) {
    if (isEnabled()) {
      addListenerInternal(name, changes, consumer);
    }

    listeners.add(new ListenerHolder(name, changes, consumer));
  }

  /**
   * Adds the listener internal.
   *
   * @param name the name
   * @param changes the changes
   * @param consumer the consumer
   */
  private static void addListenerInternal(String name, Changes changes,
      Consumer<Map<String, Object>> consumer) {
    Thread thread = new Thread(() -> {
      try {
        MPCore.get().getLogger().info("Listening for RethinkDB updates on table " + name + "...");

        Cursor<String> cursor = changes.run(connection);

        for (Object change : cursor) {
          @SuppressWarnings("unchecked")
          Map<String, Object> map = (Map<String, Object>) change;

          consumer.accept(map);
        }
      } catch (ReqlDriverError ignored) {
      }
    });
    thread.start();

    threads.add(thread);
  }
}


@RequiredArgsConstructor
@Getter
class ListenerHolder {
  private final String name;
  private final Changes changes;
  private final Consumer<Map<String, Object>> consumer;
}
