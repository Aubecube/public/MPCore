/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Entity;

import fr.minepod.mpcore.api.nms.internal.EnumEntityAttributeEnumerator;
import fr.minepod.mpcore.api.nms.internal.IEntityAttributeManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityAttributeInstanceManipulator.
 */
public class EntityAttributeInstanceManipulator {

  /** The attribute instance. */
  static Class<?> attributeInstance;

  /** The entity insentient. */
  static Class<?> entityInsentient;

  /** The i attribute. */
  static Class<?> iAttribute;

  /** The generic attributes. */
  static Class<?> genericAttributes;

  /** The craft entity. */
  static Class<?> craftEntity;

  /** The internal. */
  static IEntityAttributeManipulator internal;

  static {
    try {
      internal = NMSBase.getVersionedClass("EntityAttributeManipulator",
          IEntityAttributeManipulator.class);

      attributeInstance = Class.forName(NMSBase.getNMS() + ".AttributeInstance");
      entityInsentient = Class.forName(NMSBase.getNMS() + ".EntityInsentient");
      iAttribute = Class.forName(NMSBase.getNMS() + ".IAttribute");
      genericAttributes = Class.forName(NMSBase.getNMS() + ".GenericAttributes");
      craftEntity = Class.forName(NMSBase.getBukkit() + ".entity.CraftEntity");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Sets the entity attribute.
   *
   * @param entity the entity
   * @param type the attribute type
   * @param value the value
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static void setEntityAttribute(Entity entity, EnumEntityAttributeEnumerator type,
      double value) throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
    Object handle = Reflection.getInvokeDeclaredMethod(craftEntity.cast(entity), "getHandle");
    Object insentient = entityInsentient.cast(handle);

    Object attributes = Reflection.invokeMethod(insentient,
        Reflection.getMethod(insentient, "getAttributeInstance", iAttribute),
        Reflection.getDeclaredField(genericAttributes,
            (String) Reflection.getInvokeMethod(internal, type.name())));
    Reflection.getInvokeMethod(attributeInstance.cast(attributes), "setValue", value);
  }
}
