/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.logger;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

// TODO: Auto-generated Javadoc
/**
 * The Class LogManager.
 */
public class LogManager {

  /** The file. */
  File file;

  /**
   * Instantiates a new log manager.
   * 
   * @param file the file
   */
  @Deprecated
  public LogManager(File file) {
    this.file = file;
  }

  /**
   * Convenience method.
   *
   * @param message the message
   * @throws IOException Signals that an I/O exception has occurred.
   * @see #log(String message)
   */
  @Deprecated
  public void log(String message) throws IOException {
    log(message, true);
  }

  /**
   * Write to the log the given message.
   *
   * @param message the message
   * @param date whether to include the date as prefix or not
   * @throws IOException Signals that an I/O exception has occurred.
   */
  @Deprecated
  public void log(String message, boolean date) throws IOException {
    logToFile(file, message, date);
  }

  /**
   * Write to the log file the given message.
   *
   * @param file the log file
   * @param message the message
   * @param date whether to include the date as prefix or not
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public static void logToFile(File file, String message, boolean date) throws IOException {
    if (!file.exists()) {
      if (!file.getParentFile().exists()) {
        file.getParentFile().mkdirs();
      }

      file.createNewFile();
    }

    FileWriter fw = new FileWriter(file, true);
    PrintWriter pw = new PrintWriter(fw);

    if (date) {
      pw.println(new Date().toString() + " - " + message);
    } else {
      pw.println(message);
    }
    pw.close();
  }
}
