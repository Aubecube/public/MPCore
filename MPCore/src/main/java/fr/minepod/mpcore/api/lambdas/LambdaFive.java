/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.lambdas;

// TODO: Auto-generated Javadoc
/**
 * The Interface LambdaFive.
 *
 * @param <A> the generic type
 * @param <B> the generic type
 * @param <C> the generic type
 * @param <D> the generic type
 * @param <E> the element type
 */
@FunctionalInterface
public interface LambdaFive<A, B, C, D, E> {

  /**
   * Apply.
   *
   * @param a the a
   * @param b the b
   * @param c the c
   * @param d the d
   * @param e the e
   */
  public void apply(A a, B b, C c, D d, E e);
}
