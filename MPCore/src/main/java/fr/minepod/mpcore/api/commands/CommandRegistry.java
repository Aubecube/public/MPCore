/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.commands;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.naming.NoPermissionException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.ObjectHolder;
import fr.minepod.mpcore.api.commands.exceptions.CommandArgumentException;
import fr.minepod.mpcore.api.commands.exceptions.WrongArgumentException;
import fr.minepod.mpcore.api.commands.exceptions.WrongCommandException;
import fr.minepod.mpcore.api.lambdas.LambdaFive;
import fr.minepod.mpcore.api.permissions.PermissionsManager;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandRegistry.
 *
 * @param <P> the generic type
 */
public class CommandRegistry<P extends JavaPlugin> extends LinkedHashMap<String, CommandBase>
    implements CommandExecutor {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5517597358277578012L;

  /** The plugin. */
  private P plugin;

  /** The base. */
  private String base;

  /**
   * Instantiates a new command registry.
   *
   * @param plugin the plugin
   * @param base the base
   */
  public CommandRegistry(P plugin, String base) {
    super();
    this.plugin = plugin;
    this.base = base;
  }

  /**
   * Execute.
   *
   * @param sender the sender
   * @param command the command
   * @param label the label
   * @param args the args
   * @return true, if successful
   */
  public boolean execute(CommandSender sender, Command command, String label, String[] args) {
    for (Entry<String, CommandBase> entry : this.entrySet()) {
      try {
        entry.getValue().execute(plugin, entry.getKey(), sender, command, label, args);

        return true;
      } catch (CommandArgumentException ignored) {
        sender.sendMessage(MPCore.getConfigData().getCommandsRegistryWrongUsageMessage());
        sender.sendMessage(getCommandString(entry.getValue()));

        return true;
      } catch (NoPermissionException ignored) {
        sender.sendMessage(MPCore.getConfigData().getCommandsRegistryNoPermissionMessage());

        return true;
      } catch (WrongArgumentException | WrongCommandException ignored) {
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

    sender.sendMessage(MPCore.getConfigData().getCommandsRegistryCommandsListMessage());
    for (Entry<String, CommandBase> entry : this.entrySet()) {
      if (PermissionsManager.hasSender(sender, entry.getValue().getPermission())) {
        ChatColor color = ChatColor.GREEN;

        if (entry.getValue().getType() != null) {
          switch (entry.getValue().getType()) {
            case MODERATOR:
              color = ChatColor.DARK_BLUE;
              break;
            case ADMINISTRATOR:
              color = ChatColor.RED;
              break;
            default:
              break;
          }
        }

        sender.sendMessage(
            color + entry.getValue().getHelp() + " - " + getCommandString(entry.getValue()));
      }
    }

    return false;
  }

  /**
   * On command.
   *
   * @param sender the sender
   * @param command the command
   * @param label the label
   * @param args the args
   * @return true, if successful
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.command.CommandExecutor#onCommand(org.bukkit.command.CommandSender,
   * org.bukkit.command.Command, java.lang.String, java.lang.String[])
   */
  @Override
  public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    execute(sender, command, label, args);

    // Always return true
    return true;
  }

  /**
   * Register helper.
   *
   * @param help the help
   * @param pattern the pattern
   * @param permission the permission
   * @param lambda the lambda
   * @return the command base
   */
  public CommandBase register(String help, String pattern, String permission,
      LambdaFive<CommandSender, Command, String, String[], List<ObjectHolder>> lambda) {
    return put(pattern, new CommandBase(help, pattern, permission, lambda));
  }

  /**
   * Register helper.
   *
   * @param help the help
   * @param pattern the pattern
   * @param type the type
   * @param lambda the lambda
   * @return the command base
   */
  public CommandBase register(String help, String pattern, CommandType type,
      LambdaFive<CommandSender, Command, String, String[], List<ObjectHolder>> lambda) {
    return put(pattern, new CommandBase(help, pattern, type, lambda));
  }

  /**
   * Gets the command string.
   *
   * @param command the command
   * @return the command string
   */
  private String getCommandString(CommandBase command) {
    StringBuilder builder = new StringBuilder(ChatColor.GOLD + "/" + base);

    for (String part : command.getPattern().split(" ")) {
      if (part.startsWith("[") && part.endsWith("]") && part.length() > 2) {
        builder.append(
            " [" + part.substring(1, part.length() - 1).split("=")[1].replace("_", " ") + "]");
      } else {
        builder.append(" " + part);
      }
    }

    return builder.toString();
  }
}
