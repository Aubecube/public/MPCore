/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.classes;

import java.lang.reflect.Field;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

// TODO: Auto-generated Javadoc
/**
 * The Class ObjectHolder.
 */
public class ObjectHolder {

  /** The int value. */
  private int intValue;

  /** The long value. */
  private long longValue;

  /** The double value. */
  private double doubleValue;

  /** The float value. */
  private float floatValue;

  /** The boolean value. */
  private boolean booleanValue;

  /** The player value. */
  private Player playerValue;

  /** The string value. */
  private String stringValue;

  /** The item stack value. */
  private ItemStack itemStackValue;

  /** The uuid value. */
  private UUID uuidValue;

  /** The object value. */
  private Object objectValue;

  /**
   * Checks for value.
   *
   * @return true, if successful
   */
  public boolean hasValue() {
    for (Field field : getClass().getDeclaredFields()) {
      field.setAccessible(true);

      try {
        if (field.get(this) != null) {
          return true;
        }
      } catch (IllegalArgumentException | IllegalAccessException e) {
        e.printStackTrace();
      }
    }

    return false;
  }

  /**
   * Gets the int value.
   *
   * @return the int value
   */
  public int getIntValue() {
    return intValue;
  }

  /**
   * Sets the int value.
   *
   * @param intValue the new int value
   */
  public void setIntValue(int intValue) {
    this.intValue = intValue;
  }

  /**
   * Gets the long value.
   *
   * @return the long value
   */
  public long getLongValue() {
    return longValue;
  }

  /**
   * Sets the long value.
   *
   * @param longValue the new long value
   */
  public void setLongValue(long longValue) {
    this.longValue = longValue;
  }

  /**
   * Gets the double value.
   *
   * @return the double value
   */
  public double getDoubleValue() {
    return doubleValue;
  }

  /**
   * Sets the double value.
   *
   * @param doubleValue the new double value
   */
  public void setDoubleValue(double doubleValue) {
    this.doubleValue = doubleValue;
  }

  /**
   * Gets the float value.
   *
   * @return the float value
   */
  public float getFloatValue() {
    return floatValue;
  }

  /**
   * Sets the float value.
   *
   * @param floatValue the new float value
   */
  public void setFloatValue(float floatValue) {
    this.floatValue = floatValue;
  }

  /**
   * Gets the boolean value.
   *
   * @return the boolean value
   */
  public boolean getBooleanValue() {
    return booleanValue;
  }

  /**
   * Sets the boolean value.
   *
   * @param booleanValue the new boolean value
   */
  public void setBooleanValue(boolean booleanValue) {
    this.booleanValue = booleanValue;
  }

  /**
   * Gets the player value.
   *
   * @return the player value
   */
  public Player getPlayerValue() {
    return playerValue;
  }

  /**
   * Sets the player value.
   *
   * @param playerValue the new player value
   */
  public void setPlayerValue(Player playerValue) {
    this.playerValue = playerValue;
  }

  /**
   * Gets the string value.
   *
   * @return the string value
   */
  public String getStringValue() {
    return stringValue;
  }

  /**
   * Sets the string value.
   *
   * @param stringValue the new string value
   */
  public void setStringValue(String stringValue) {
    this.stringValue = stringValue;
  }

  /**
   * Gets the item stack value.
   *
   * @return the item stack value
   */
  public ItemStack getItemStackValue() {
    return itemStackValue;
  }

  /**
   * Sets the item stack value.
   *
   * @param itemStackValue the new item stack value
   */
  public void setItemStackValue(ItemStack itemStackValue) {
    this.itemStackValue = itemStackValue;
  }

  /**
   * Gets the uuid value.
   *
   * @return the uuid value
   */
  public UUID getUuidValue() {
    return uuidValue;
  }

  /**
   * Sets the uuid value.
   *
   * @param uuidValue the new uuid value
   */
  public void setUuidValue(UUID uuidValue) {
    this.uuidValue = uuidValue;
  }

  /**
   * Gets the object value.
   *
   * @return the object value
   */
  public Object getObjectValue() {
    return objectValue;
  }

  /**
   * Sets the object value.
   *
   * @param objectValue the new object value
   */
  public void setObjectValue(Object objectValue) {
    this.objectValue = objectValue;
  }
}
