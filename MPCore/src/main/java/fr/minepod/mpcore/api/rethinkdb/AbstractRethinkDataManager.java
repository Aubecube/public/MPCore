/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.rethinkdb;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.base.Function;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.gen.ast.Changes;
import com.rethinkdb.gen.ast.IndexCreate;
import com.rethinkdb.gen.ast.Table;

import lombok.Getter;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractRethinkDataManager.
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public abstract class AbstractRethinkDataManager<K, V extends RethinkDataHolder> {

  /**
   * Gets the plugin.
   *
   * @return the plugin
   */
  @Getter
  private final JavaPlugin plugin;

  /**
   * Gets the name.
   *
   * @return the name
   */
  @Getter
  private final String name;

  /**
   * Gets the table.
   *
   * @return the table
   */
  @Getter
  private final Table table;

  /**
   * Checks if the data is always cached.
   *
   * @return true, if always cached
   */
  @Getter
  private final boolean cache;

  /**
   * Checks if the data should be pulled on initialization.
   *
   * @return true, if should be pulled
   */
  @Getter
  private final boolean pull;

  /**
   * Gets the data.
   *
   * @return the data
   */
  @Getter
  private final Map<K, V> data = new ConcurrentHashMap<>();

  /**
   * Instantiates a new abstract Rethink data manager.
   *
   * @param plugin the plugin
   * @param table the table
   * @param function the function to listen to changes
   * @param cache if the data should always be cached
   * @param pull if the data should be pulled on initialization
   */
  public AbstractRethinkDataManager(JavaPlugin plugin, String table,
      Function<Table, Changes> function, boolean cache, boolean pull) {
    this.plugin = plugin;
    this.name = table;
    this.table = RethinkDB.r.table(table);
    this.cache = cache;
    this.pull = pull;

    RethinkDBRegistry.register(this);
    RethinkDBRegistry.addListener(name, function.apply(this.table), change -> changed(change));
  }

  /**
   * Gets the for.
   *
   * @param key the key
   * @return the for
   */
  public V getFor(K key) {
    V value = data.get(key);

    if (value == null) {
      value = provide(getFromDatabase(key));

      if (value != null) {
        data.put(key, value);
      }
    }

    return value;
  }

  /**
   * Gets the from database.
   *
   * @param key the key
   * @return the from database
   */
  public Map<String, Object> getFromDatabase(K key) {
    if (!RethinkDBRegistry.isEnabled()) {
      getPlugin().getLogger().severe("RethinkDB is not enabled!");
      return null;
    }

    return getTable().get(keyToString(key)).run(RethinkDBRegistry.getConnection());
  }

  /**
   * Changed.
   *
   * @param map the map
   */
  public void changed(Map<String, Object> map) {
    if (map.get("new_val") != null) {
      @SuppressWarnings("unchecked")
      Map<String, Object> new_val = (Map<String, Object>) map.get("new_val");
      K key = stringToKey((String) new_val.get("id"));

      // Atomic operations
      if (cache) {
        // cache = true -> always compute
        getData().compute(key, (k, v) -> {
          return compute(provide(new_val), v);
        });
      } else {
        // cache = false -> compute only if present
        getData().computeIfPresent(key, (k, v) -> {
          return compute(provide(new_val), v);
        });
      }
    } else {
      @SuppressWarnings("unchecked")
      Map<String, Object> old_val = (Map<String, Object>) map.get("old_val");
      K key = stringToKey((String) old_val.get("id"));

      getData().remove(key);
    }
  }

  /**
   * Replace.
   *
   * @param key the key
   * @param value the value
   */
  public void replace(K key, V value) {
    if (!RethinkDBRegistry.isEnabled()) {
      getPlugin().getLogger().severe("RethinkDB is not enabled!");
      return;
    }

    if (value != null) {
      getTable().get(keyToString(key)).replace(value.asRethinkHashMap())
          .run(RethinkDBRegistry.getConnection());

      getData().put(key, value);
    } else {
      getTable().get(keyToString(key)).delete().run(RethinkDBRegistry.getConnection());

      getData().remove(key);
    }
  }

  /**
   * Put.
   * 
   * @param map the map
   */
  public void put(Map<String, Object> map) {
    K key = stringToKey((String) map.get("id"));
    V value = provide(map);

    getData().put(key, value);
  }

  /**
   * Compute.
   *
   * @param provided the provided
   * @param old the old
   * @return the v
   */
  private V compute(V provided, V old) {
    if (provided != null) {
      return provided;
    } else {
      return old;
    }
  }

  /**
   * Creates the indexes.
   *
   * @return the list
   */
  public abstract List<IndexCreate> createIndexes();

  /**
   * Key to string.
   *
   * @param key the key
   * @return the string
   */
  public abstract String keyToString(K key);

  /**
   * String to key.
   *
   * @param key the key
   * @return the k
   */
  public abstract K stringToKey(String key);

  /**
   * Provide.
   *
   * @param map the map
   * @return the v
   */
  public abstract V provide(Map<String, Object> map);
}
