/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.plugins;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.APIVersion;
import fr.minepod.mpcore.MPCoreDepends;
import fr.minepod.mpcore.api.commands.CommandRegistry;
import fr.minepod.mpcore.api.data.AutoDataManager;
import fr.minepod.mpcore.api.integrations.DynamicDependency;

// TODO: Auto-generated Javadoc
/**
 * The Class MPCorePluginBase.
 */
public abstract class MPCorePluginBase extends JavaPlugin implements MPCoreDepends {

  /**
   * Gets the API version.
   *
   * @return the API version
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.MPCoreDepends#getAPIVersion()
   */
  @Override
  public abstract APIVersion getAPIVersion();

  /** The commands. */
  public CommandRegistry<MPCorePluginBase> commands;

  /** The dependencies. */
  public DynamicDependency dependencies = new DynamicDependency();

  /**
   * On enable.
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
   */
  @Override
  public void onEnable() {
    getLogger().info("Enabling...");

    for (Entry<String, Map<String, Object>> entry : getDescription().getCommands().entrySet()) {
      Map<String, Object> properties = entry.getValue();

      Object aliases = properties.get("aliases");
      if (aliases != null && aliases instanceof String) {
        commands = new CommandRegistry<>(this, (String) aliases);
      } else {
        getLogger().severe(
            "Found a list of aliases or no aliases at all instead of a single one! Defaulting to: "
                + getDescription().getName().toLowerCase());

        commands = new CommandRegistry<>(this, getDescription().getName().toLowerCase());
      }

      getLogger().info("Registering base command as: " + entry.getKey());
      getCommand(entry.getKey()).setExecutor(commands);

      break;
    }

    if (commands == null) {
      getLogger().warning("No base command was registered");
    }

    onEnableBefore();

    AutoDataManager.onEnable(this);

    onEnableAfter();

    getLogger().info("Enabled!");
  }

  /**
   * On disable.
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
   */
  @Override
  public void onDisable() {
    getLogger().info("Disabling...");

    onDisableBefore();

    AutoDataManager.onDisable(this);

    onDisableAfter();

    getLogger().info("Disabled!");
  }

  /**
   * Method to be executed before the data initialization.
   */
  public abstract void onEnableBefore();

  /**
   * Method to be executed after the data initialization.
   */
  public abstract void onEnableAfter();

  /**
   * Method to be executed before the data saving and disabling.
   */
  public abstract void onDisableBefore();

  /**
   * Method to be executed after the data saving and disabling.
   */
  public abstract void onDisableAfter();
}
