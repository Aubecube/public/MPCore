/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.v1_8_R3;

import fr.minepod.mpcore.api.nms.internal.IPacketPlayOutWorldBorderManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;

// TODO: Auto-generated Javadoc
/**
 * The Class PacketPlayOutWorldBorderManipulator.
 */
public class PacketPlayOutWorldBorderManipulator implements IPacketPlayOutWorldBorderManipulator {

  /**
   * Gets the world border class.
   *
   * @return the world border class
   * @throws ClassNotFoundException the class not found exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator# getWorldBorderClass()
   */
  public Class<?> getWorldBorderClass() throws ClassNotFoundException {
    return Class.forName(NMSBase.getNMS() + ".WorldBorder");
  }

  /**
   * Gets the enum world border action class.
   *
   * @return the enum world border action class
   * @throws ClassNotFoundException the class not found exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator# getEnumWorldBorderActionClass()
   */
  public Class<?> getEnumWorldBorderActionClass() throws ClassNotFoundException {
    return Class.forName(NMSBase.getNMS() + ".PacketPlayOutWorldBorder$EnumWorldBorderAction");
  }

  /**
   * Gets the packet play out world border class.
   *
   * @return the packet play out world border class
   * @throws ClassNotFoundException the class not found exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator#
   * getPacketPlayOutWorldBorderClass()
   */
  public Class<?> getPacketPlayOutWorldBorderClass() throws ClassNotFoundException {
    return Class.forName(NMSBase.getNMS() + ".PacketPlayOutWorldBorder");
  }

  /**
   * Sets the center method.
   *
   * @param x the x
   * @param z the z
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator#setCenterMethod( double, double)
   */
  public String setCenterMethod(double x, double z) {
    return "setCenter";
  }

  /**
   * Sets the initial size method.
   *
   * @param size the size
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator# setInitialSizeMethod(double)
   */
  public String setInitialSizeMethod(double size) {
    return "setSize";
  }

  /**
   * Change size method.
   *
   * @param oldSize the old size
   * @param newSize the new size
   * @param speed the speed
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator#changeSizeMethod( double, double,
   * long)
   */
  public String changeSizeMethod(double oldSize, double newSize, long speed) {
    return "transitionSizeBetween";
  }

  /**
   * Sets the warning blocks method.
   *
   * @param blocks the blocks
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator# setWarningBlocksMethod(int)
   */
  public String setWarningBlocksMethod(int blocks) {
    return "setWarningDistance";
  }

  /**
   * Sets the warning time method.
   *
   * @param time the time
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IWBPacketManipulator# setWarningTimeMethod(int)
   */
  public String setWarningTimeMethod(int time) {
    return "setWarningTime";
  }
}
