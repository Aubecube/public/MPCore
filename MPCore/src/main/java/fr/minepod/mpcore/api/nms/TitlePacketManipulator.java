/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.bukkit.entity.Player;

import fr.minepod.mpcore.api.nms.internal.EnumTitleActionEnumerator;
import fr.minepod.mpcore.api.nms.internal.IPacketPlayOutTitleManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.api.nms.internal.PlayerConnectionWrapper;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class TitlePacketManipulator.
 */
public class TitlePacketManipulator {

  /** The internal. */
  static IPacketPlayOutTitleManipulator internal;

  static {
    try {
      internal = NMSBase.getVersionedClass("PacketPlayOutTitleManipulator",
          IPacketPlayOutTitleManipulator.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Gets the packet.
   *
   * @param type the type
   * @param text the text
   * @param fadeIn the fade in
   * @param stay the stay
   * @param fadeOut the fade out
   * @return the packet
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws ClassNotFoundException the class not found exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object getPacket(EnumTitleActionEnumerator type, String text, int fadeIn, int stay,
      int fadeOut) throws IllegalAccessException, IllegalArgumentException,
      InvocationTargetException, InstantiationException, ClassNotFoundException,
      NoSuchMethodException, SecurityException, NoSuchFieldException {
    Object serialized = ChatSerializerManipulator.getIChatBaseComponent(text);

    Class<?> action = internal.getEnumTitleActionClass();
    Constructor<?> packet = internal.getPacketPlayOutTitleClass().getDeclaredConstructor(action,
        ChatSerializerManipulator.getIChatComponentClass(), int.class, int.class, int.class);

    return packet.newInstance(Reflection.getDeclaredField(action, type.name()), serialized, fadeIn,
        stay, fadeOut);
  }

  /**
   * Gets the packet.
   *
   * @param type the type
   * @param text the text
   * @return the packet
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws InstantiationException the instantiation exception
   * @throws ClassNotFoundException the class not found exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws NoSuchFieldException the no such field exception
   */
  public static Object getPacket(EnumTitleActionEnumerator type, String text)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      InstantiationException, ClassNotFoundException, NoSuchMethodException, SecurityException,
      NoSuchFieldException {
    Object serialized = ChatSerializerManipulator.getIChatBaseComponent(text);

    Class<?> action = internal.getEnumTitleActionClass();
    Constructor<?> packet = internal.getPacketPlayOutTitleClass().getDeclaredConstructor(action,
        ChatSerializerManipulator.getIChatComponentClass());

    return packet.newInstance(Reflection.getDeclaredField(action, type.name()), serialized);
  }

  /**
   * Send packet.
   *
   * @param player the player
   * @param type the type
   * @param text the text
   * @param fadeIn the fade in
   * @param stay the stay
   * @param fadeOut the fade out
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   * @throws ClassNotFoundException the class not found exception
   * @throws InstantiationException the instantiation exception
   */
  public static void sendPacket(Player player, EnumTitleActionEnumerator type, String text,
      int fadeIn, int stay, int fadeOut) throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      NoSuchFieldException, ClassNotFoundException, InstantiationException {
    PlayerConnectionWrapper.sendPacket(player, internal.getPacketPlayOutTitleClass(),
        getPacket(type, text, fadeIn, stay, fadeOut));
  }

  /**
   * Send packet.
   *
   * @param player the player
   * @param type the type
   * @param text the text
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   * @throws ClassNotFoundException the class not found exception
   * @throws InstantiationException the instantiation exception
   */
  public static void sendPacket(Player player, EnumTitleActionEnumerator type, String text)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchFieldException,
      ClassNotFoundException, InstantiationException {
    PlayerConnectionWrapper.sendPacket(player, internal.getPacketPlayOutTitleClass(),
        getPacket(type, text));
  }

  /**
   * Gets the packet play out title class.
   *
   * @return the packet play out title class
   * @throws ClassNotFoundException the class not found exception
   */
  public static Class<?> getPacketPlayOutTitleClass() throws ClassNotFoundException {
    return internal.getPacketPlayOutTitleClass();
  }
}
