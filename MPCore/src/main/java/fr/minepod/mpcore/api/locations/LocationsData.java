/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.locations;

import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

// TODO: Auto-generated Javadoc
/**
 * The Class LocationsData.
 */
public class LocationsData {

  /**
   * Gets the data.
   * 
   * @param server the server
   * @param data the data
   * @param path the path
   * @return the data
   * @deprecated This method is not optimized. You should use SerializableLocation instead.
   *             {@link SerializableLocation(Location)}
   */
  @Deprecated
  public Location getData(Server server, FileConfiguration data, String path) {

    if (data != null) {
      if (path.endsWith(".")) {
        path = path.substring(0, path.length() - 2);
      }

      if (data.isSet(path)) {
        String world = data.getString(path + ".world");
        double x = data.getDouble(path + ".x");
        double y = data.getDouble(path + ".y");
        double z = data.getDouble(path + ".z");

        boolean loaded = false;
        for (World temp : server.getWorlds()) {
          if (temp.getName().equalsIgnoreCase(world)) {
            loaded = true;
            return new Location(temp, x, y, z);
          }
        }

        if (!loaded) {
          server.getLogger().severe("Could not load location:");
          server.getLogger().severe("(" + world + ", " + x + ", " + y + ", " + z + ")");
        }
      }
    }

    return null;
  }

  /**
   * Sets the data.
   * 
   * @param data the data
   * @param path the path
   * @param location the location
   * @deprecated This method is not optimized. You should use SerializableLocation instead.
   *             {@link SerializableLocation(Location)}
   */
  @Deprecated
  public void setData(FileConfiguration data, String path, Location location) {
    if (data != null) {
      data.set(path + ".world", location.getWorld().getName());
      data.set(path + ".x", location.getBlockX());
      data.set(path + ".y", location.getBlockY());
      data.set(path + ".z", location.getBlockZ());
    }
  }
}
