/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.locations;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;

// TODO: Auto-generated Javadoc
/**
 * The Class SerializableLocation.
 */
@SerializableAs("Location")
public class SerializableLocation extends Location implements ConfigurationSerializable {
  /**
   * Instantiates a new serializable location.
   * 
   * @param location the location
   */
  public SerializableLocation(Location location) {
    super(location.getWorld(), location.getX(), location.getY(), location.getZ(), location.getYaw(),
        location.getPitch());
  }

  /**
   * Instantiates a new serializable location.
   * 
   * @param map the map
   */
  public SerializableLocation(Map<String, Object> map) {
    super(Bukkit.getWorld((String) map.get("world")), (double) map.get("x"), (double) map.get("y"),
        (double) map.get("z"), (float) (double) map.get("yaw"), (float) (double) map.get("pitch"));

    if (Bukkit.getWorld((String) map.get("world")) == null) {
      throw new IllegalArgumentException("World not found: " + map.get("world"));
    }
  }

  /**
   * Serialize.
   *
   * @return the map
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.configuration.serialization.ConfigurationSerializable# serialize()
   */
  @Override
  public Map<String, Object> serialize() {
    Map<String, Object> map = new HashMap<>();

    map.put("world", getWorld().getName());
    map.put("x", getX());
    map.put("y", getY());
    map.put("z", getZ());
    map.put("yaw", getYaw());
    map.put("pitch", getPitch());

    return map;
  }
}
