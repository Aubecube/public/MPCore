/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.classes;

import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import com.google.common.base.Joiner;
import com.rethinkdb.RethinkDB;
import com.rethinkdb.model.MapObject;

import fr.minepod.mpcore.api.rethinkdb.RethinkDataHolder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

// TODO: Auto-generated Javadoc
/**
 * The Class PlayerClass.
 */

/**
 * Instantiates a new player class.
 *
 * @param UUID the uuid
 * @param name the name
 * @param oldNames the old names
 * @param firstSeen the first seen
 * @param lastSeen the last seen
 * @param playTimes the play times
 */

/**
 * Instantiates a new player class.
 *
 * @param UUID the uuid
 * @param name the name
 * @param oldNames the old names
 * @param firstSeen the first seen
 * @param lastSeen the last seen
 * @param playTimes the play times
 */

/**
 * Instantiates a new player class.
 *
 * @param UUID the uuid
 * @param name the name
 * @param oldNames the old names
 * @param firstSeen the first seen
 * @param lastSeen the last seen
 * @param playTimes the play times
 */

/**
 * Instantiates a new player class.
 *
 * @param UUID the uuid
 * @param name the name
 * @param oldNames the old names
 * @param firstSeen the first seen
 * @param lastSeen the last seen
 * @param playTimes the play times
 */
@AllArgsConstructor

/**
 * Gets the play times.
 *
 * @return the play times
 */

/**
 * Gets the play times.
 *
 * @return the play times
 */

/**
 * Gets the play times.
 *
 * @return the play times
 */
@Getter

/**
 * Sets the play times.
 *
 * @param playTimes the play times
 */

/**
 * Sets the play times.
 *
 * @param playTimes the play times
 */

/**
 * Sets the play times.
 *
 * @param playTimes the play times
 */
@Setter
public class PlayerClass extends RethinkDataHolder {

  /** The uuid. */
  private UUID UUID;

  /** The name. */
  private String name;

  /** The old names. */
  private Map<String, Long> oldNames;

  /** The first seen. */
  private long firstSeen;

  /** The last seen. */
  private long lastSeen;

  /** The play time. */
  private Map<String, Long> playTimes;

  /**
   * As rethink hash map.
   *
   * @return the map object
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.rethinkdb.RethinkDataHolder#asRethinkHashMap()
   */
  @Override
  public MapObject asRethinkHashMap() {
    MapObject names = RethinkDB.r.hashMap();
    for (Entry<String, Long> temp : oldNames.entrySet()) {
      names.with(temp.getKey().toLowerCase(), temp.getValue());
    }

    MapObject times = RethinkDB.r.hashMap();
    for (Entry<String, Long> temp : playTimes.entrySet()) {
      times.with(temp.getKey(), temp.getValue());
    }

    return RethinkDB.r.hashMap("id", UUID.toString()).with("name", name.toLowerCase())
        .with("oldNames", names).with("firstSeen", firstSeen).with("lastSeen", lastSeen)
        .with("playTimes", times);
  }

  /**
   * To string.
   *
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.rethinkdb.RethinkDataHolder#toString()
   */
  @Override
  public String toString() {
    return "{UUID: " + UUID.toString() + ", name: " + name + ", oldNames: {"
        + Joiner.on(", ").withKeyValueSeparator(": ").join(oldNames) + "}, firstSeen: " + firstSeen
        + ", lastSeen: " + lastSeen + ", playTimes: {"
        + Joiner.on(", ").withKeyValueSeparator(": ").join(playTimes) + "}}";
  }
}
