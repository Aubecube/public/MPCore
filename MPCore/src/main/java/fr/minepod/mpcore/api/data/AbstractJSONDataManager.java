/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractJSONDataManager.
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public abstract class AbstractJSONDataManager<K, V> extends AbstractDataManager<K, V> {

  /** Raw data. */
  private Map<?, ?> config = new HashMap<>();

  /**
   * Instantiates a new abstract json data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   */
  public AbstractJSONDataManager(JavaPlugin plugin, String fileName) {
    super(plugin, fileName, false);
  }

  /**
   * Instantiates a new abstract json data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   * @param autosave the autosave
   */
  public AbstractJSONDataManager(JavaPlugin plugin, String fileName, boolean autosave) {
    super(plugin, fileName, autosave);
  }

  /**
   * Instantiates a new abstract json data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   * @param autosave the autosave
   * @param load the load
   * @param forceSave the force save
   * @param save the save
   */
  public AbstractJSONDataManager(JavaPlugin plugin, String fileName, boolean autosave, boolean load,
      boolean forceSave, boolean save) {
    super(plugin, fileName, autosave, load, forceSave, save);
  }

  /**
   * Load.
   * 
   * @param get get or not
   */
  public void load(boolean get) {
    try {
      dataFile = new File(plugin.getDataFolder(), fileName);

      if (dataFile.exists() && dataFile.length() > 0) {
        config = (Map<?, ?>) new JSONParser().parse(new FileReader(dataFile));
      } else {
        config = new HashMap<>();
      }

      if (get) {
        get();
      }
    } catch (IOException | ParseException e) {
      e.printStackTrace();
    }
  }

  /**
   * Save.
   *
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public void save() throws IOException {
    set();

    if (!dataFile.getParentFile().exists()) {
      dataFile.getParentFile().mkdirs();
    }

    if (!dataFile.exists()) {
      dataFile.createNewFile();
    }

    FileWriter file = new FileWriter(dataFile);
    file.write(new JSONObject(config).toJSONString());
    file.flush();
    file.close();
  }

  /**
   * Gets the config.
   *
   * @return the config
   */
  public Map<?, ?> getJSONConfig() {
    return config;
  }

  /**
   * Sets the config.
   *
   * @param map the map
   */
  public void setJSONConfig(Map<K, V> map) {
    config = map;
  }
}
