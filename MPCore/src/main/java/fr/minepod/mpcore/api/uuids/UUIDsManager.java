/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.uuids;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Cursor;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.rethinkdb.RethinkDBRegistry;

// TODO: Auto-generated Javadoc
/**
 * The Class UUIDsManager.
 */
public class UUIDsManager {

  /** The pattern. */
  private static Pattern pattern = Pattern.compile("^[a-zA-Z0-9_-]{3,16}$");

  /**
   * The plugin.
   * 
   * @deprecated use the static method "MPCore.get()" instead of this field
   */
  @Deprecated
  MPCore plugin;

  /**
   * Instantiates a new UUIDs manager using static accessor.
   * 
   * @deprecated use the static methods instead
   */
  @Deprecated
  public UUIDsManager() {
    this.plugin = MPCore.get();
  }

  /**
   * Instantiates a new UUIDs manager.
   * 
   * @param plugin the plugin
   * @deprecated use the static methods instead
   */
  @Deprecated
  public UUIDsManager(MPCore plugin) {
    this.plugin = plugin;
  }

  /**
   * Gets the player class.
   * 
   * @param name the player name
   * @return the player class
   */
  public static PlayerClass getPlayerClass(String name) {
    return getPlayerClass(name, false);
  }

  /**
   * Gets the player class.
   * 
   * @param name the player name
   * @param searchOlds whether to also search old names or not
   * @return the player class
   */
  public static PlayerClass getPlayerClass(String name, boolean searchOlds) {
    try {
      return getPlayerClass(UUID.fromString(name));
    } catch (Exception ignored) {
    }

    if (!pattern.matcher(name).matches()) {
      // Invalid username
      return null;
    }

    String lower = name.toLowerCase();

    for (Player online : Bukkit.getServer().getOnlinePlayers()) {
      if (online.getName().equalsIgnoreCase(lower)) {
        return getPlayerClass(online.getUniqueId());
      }
    }

    for (Entry<UUID, PlayerClass> entry : MPCore.get().players.getData().entrySet()) {
      if (entry.getValue() != null && entry.getValue().getName().equalsIgnoreCase(lower)) {
        return entry.getValue();
      }
    }

    Cursor<?> cursor = MPCore.get().players.getTable().getAll(lower).optArg("index", "name")
        .run(RethinkDBRegistry.getConnection());
    if (cursor.hasNext()) {
      PlayerClass value = MPCore.get().players.provide((Map<String, Object>) cursor.next());

      if (value != null) {
        MPCore.get().players.getData().put(value.getUUID(), value);

        return value;
      }
    }

    if (searchOlds) {
      List<Map<String, Object>> list =
          MPCore.get().players.getTable().filter(row -> row.g("oldNames").keys().contains(lower))
              .orderBy(RethinkDB.r.desc(row -> row.g("oldNames").g(lower))).limit(1)
              .run(RethinkDBRegistry.getConnection());

      if (list.size() > 0) {
        PlayerClass player = MPCore.get().players.provide(list.get(0));

        if (player != null) {
          MPCore.get().players.getData().put(player.getUUID(), player);

          return player;
        }
      }
    }

    return null;
  }

  /**
   * Gets the player class.
   * 
   * @param uuid the player uuid
   * @return the player class
   */
  public static PlayerClass getPlayerClass(UUID uuid) {
    return MPCore.get().players.getFor(uuid);
  }
}
