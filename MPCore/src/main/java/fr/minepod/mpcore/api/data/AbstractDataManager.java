/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractDataManager.
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public abstract class AbstractDataManager<K, V> {

  /** All AbstractDataManager. */
  public static List<AbstractDataManager<?, ?>> managers = new LinkedList<>();

  /** The plugin. */
  JavaPlugin plugin;

  /** The file name. */
  String fileName;

  /** The autosave. */
  boolean autosave;

  /** The load. */
  boolean load;

  /** The force save. */
  boolean forceSave;

  /** The save. */
  boolean save;

  /** The data file. */
  File dataFile;

  /** The config. */
  FileConfiguration config;

  /** The data. */
  public Map<K, V> data;

  /**
   * Instantiates a new abstract data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   */
  public AbstractDataManager(JavaPlugin plugin, String fileName) {
    this(plugin, fileName, false);
  }

  /**
   * Instantiates a new abstract data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   * @param type the data type
   */
  public AbstractDataManager(JavaPlugin plugin, String fileName, DataPolicy type) {
    this(plugin, fileName, type.isAutosave(), type.isLoad(), type.isForceSave(), type.isSave());
  }

  /**
   * Instantiates a new abstract data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   * @param autosave the autosave
   */
  public AbstractDataManager(JavaPlugin plugin, String fileName, boolean autosave) {
    this(plugin, fileName, autosave, false, false, false);
  }

  /**
   * Instantiates a new abstract data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   * @param autosave the autosave
   * @param load the load
   * @param forceSave the force save
   * @param save the save
   */
  public AbstractDataManager(JavaPlugin plugin, String fileName, boolean autosave, boolean load,
      boolean forceSave, boolean save) {
    this.plugin = plugin;
    this.fileName = fileName;
    this.autosave = autosave;
    this.load = load;
    this.forceSave = forceSave;
    this.save = save;

    managers.add(this);
  }

  /**
   * Load.
   */
  public void load() {
    load(true);
  }

  /**
   * Load.
   * 
   * @param get get or not
   */
  public void load(boolean get) {
    dataFile = new File(plugin.getDataFolder(), fileName);
    config = YamlConfiguration.loadConfiguration(dataFile);

    if (get) {
      get();
    }
  }

  /**
   * Save.
   *
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public void save() throws IOException {
    set();

    if (!dataFile.getParentFile().exists()) {
      dataFile.getParentFile().mkdirs();
    }

    if (!dataFile.exists()) {
      dataFile.createNewFile();
    }

    config.save(dataFile);
  }

  /**
   * Gets the plugin.
   *
   * @return the plugin
   */
  public JavaPlugin getPlugin() {
    return plugin;
  }

  /**
   * Gets the file name.
   *
   * @return the file name
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * Checks if load is enabled.
   * 
   * @return true, if load is enabled
   */
  public boolean getLoad() {
    return load;
  }

  /**
   * Checks if force save is enabled.
   * 
   * @return true, if force save is enabled
   */
  public boolean getForceSave() {
    return forceSave;
  }

  /**
   * Checks if save is enabled.
   * 
   * @return true, if save is enabled
   */
  public boolean getSave() {
    return save;
  }

  /**
   * Checks if autosave is enabled.
   * 
   * @return true, if autosave is enabled
   */
  public boolean getAutosave() {
    return autosave;
  }

  /**
   * Gets the data file.
   *
   * @return the data file
   */
  public File getDataFile() {
    return dataFile;
  }

  /**
   * Gets the config.
   *
   * @return the config
   */
  public FileConfiguration getConfig() {
    return config;
  }

  /**
   * Sets the data.
   *
   * @param data the data
   */
  protected void setData(Map<K, V> data) {
    this.data = data;
  }

  /**
   * Gets the data.
   *
   * @return the data
   */
  public Map<K, V> getData() {
    return data;
  }

  /**
   * Contains the key.
   *
   * @param key the key
   * @return true, if successful
   */
  public boolean containsKey(K key) {
    return data.containsKey(key);
  }

  /**
   * Contains the value.
   *
   * @param value the value
   * @return true, if successful
   */
  public boolean containsValue(V value) {
    return data.containsValue(value);
  }

  /**
   * Sets the entry by key.
   *
   * @param key the key
   * @param value the value
   * @return the v
   */
  public V setFor(K key, V value) {
    return data.put(key, value);
  }

  /**
   * Gets the entry by key.
   *
   * @param key the key
   * @return the for
   */
  public V getFor(K key) {
    return data.get(key);
  }

  /**
   * Removes the entry by key.
   *
   * @param key the key
   * @return the v
   */
  public V removeFor(K key) {
    return data.remove(key);
  }

  /**
   * Gets.
   */
  public abstract void get();

  /**
   * Sets.
   */
  public abstract void set();
}
