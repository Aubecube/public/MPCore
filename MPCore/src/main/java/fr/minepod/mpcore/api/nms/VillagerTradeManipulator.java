/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.MerchantRecipe;

import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class ChatSerializerManipulator.
 */
public class VillagerTradeManipulator {

  /** The world. */
  static Class<?> world;

  /** The craft world. */
  static Class<?> craftWorld;

  /** The craft entity. */
  static Class<?> craftEntity;

  /** The entity villager. */
  static Class<?> entityVillager;

  /** The craft villager. */
  static Class<?> craftVillager;

  /** The i merchant. */
  static Class<?> iMerchant;

  /** The entity human. */
  static Class<?> entityHuman;

  /** The craft player. */
  static Class<?> craftPlayer;

  static {
    try {
      world = Class.forName(NMSBase.getNMS() + ".World");
      craftWorld = Class.forName(NMSBase.getBukkit() + ".CraftWorld");

      craftEntity = Class.forName(NMSBase.getBukkit() + ".entity.CraftEntity");

      entityVillager = Class.forName(NMSBase.getNMS() + ".EntityVillager");
      craftVillager = Class.forName(NMSBase.getBukkit() + ".entity.CraftVillager");
      iMerchant = Class.forName(NMSBase.getNMS() + ".IMerchant");


      entityHuman = Class.forName(NMSBase.getNMS() + ".EntityHuman");
      craftPlayer = Class.forName(NMSBase.getBukkit() + ".entity.CraftPlayer");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Open merchant.
   *
   * @param player the player
   * @param name the name
   * @param recipes the recipes
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchFieldException the no such field exception
   * @throws InstantiationException the instantiation exception
   */
  public static void openMerchant(Player player, String name, List<MerchantRecipe> recipes)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException, NoSuchFieldException,
      InstantiationException {
    Object worldObject =
        world.cast(Reflection.getInvokeMethod(craftWorld.cast(player.getWorld()), "getHandle"));

    Object villager = entityVillager.getConstructor(world).newInstance(worldObject);
    Object craftVillagerObject =
        craftVillager.cast(Reflection.getInvokeMethod(villager, "getBukkitEntity"));

    Villager entity = (Villager) craftVillagerObject;
    entity.setCustomName(name);
    entity.setRecipes(recipes);

    Object craftPlayerObject = craftPlayer.cast(player);
    Object handle = Reflection.getInvokeMethod(craftPlayerObject, "getHandle");
    Object human = entityHuman.cast(handle);

    Reflection.invokeMethod(villager,
        Reflection.getMethod(villager, "setTradingPlayer", entityHuman), human);

    Reflection.invokeMethod(handle, Reflection.getMethod(handle, "openTrade", iMerchant),
        iMerchant.cast(villager));
  }
}
