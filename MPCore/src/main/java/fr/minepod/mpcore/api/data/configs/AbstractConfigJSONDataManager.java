/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data.configs;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import fr.minepod.mpcore.api.data.DataPolicy;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractJSONDataManager.
 *
 * @param <P> the generic type
 * @param <K> the key type
 * @param <V> the value type
 */
public abstract class AbstractConfigJSONDataManager<P extends JavaPlugin, K, V>
    extends AbstractConfigDataManager<P, K, V> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5924871911155866013L;

  /** Raw data. */
  private Map<?, ?> config = new HashMap<>();

  /**
   * Instantiates a new abstract config JSON data manager.
   *
   * @param plugin the plugin
   * @param file the file
   * @param type the type
   */
  public AbstractConfigJSONDataManager(P plugin, String file, DataPolicy type) {
    super(plugin, file, type);
  }

  /**
   * Load.
   * 
   * @param get get or not
   */
  public void load(boolean get) {
    try {
      if (getFile().exists() && getFile().length() > 0) {
        config = (Map<?, ?>) new JSONParser().parse(new FileReader(getFile()));
      } else {
        config = new HashMap<>();
      }

      if (get) {
        get();
      }
    } catch (IOException | ParseException e) {
      e.printStackTrace();
    }
  }

  /**
   * Save.
   *
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public void save() throws IOException {
    set();

    getFile().getParentFile().mkdirs();
    getFile().createNewFile();

    FileWriter file = new FileWriter(getFile());
    file.write(new JSONObject(config).toJSONString());
    file.flush();
    file.close();
  }

  /**
   * Gets the config.
   *
   * @return the config
   */
  public Map<?, ?> getJSONConfig() {
    return config;
  }

  /**
   * Sets the config.
   *
   * @param map the map
   */
  public void setJSONConfig(Map<K, V> map) {
    config = map;
  }
}
