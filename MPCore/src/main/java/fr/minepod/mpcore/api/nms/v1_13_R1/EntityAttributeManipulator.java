/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.v1_13_R1;

import fr.minepod.mpcore.api.nms.internal.IEntityAttributeManipulator;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityAttributeManipulator.
 */
public class EntityAttributeManipulator implements IEntityAttributeManipulator {

  /**
   * Attack damage.
   *
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IEntityAttributeManipulator#ATTACK_DAMAGE()
   */
  @Override
  public String ATTACK_DAMAGE() {
    return "ATTACK_DAMAGE";
  }

  /**
   * Follow range.
   *
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IEntityAttributeManipulator#FOLLOW_RANGE()
   */
  @Override
  public String FOLLOW_RANGE() {
    return "ATTACK_DAMAGE";
  }

  /**
   * Movement speed.
   *
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.nms.internal.IEntityAttributeManipulator#MOVEMENT_SPEED()
   */
  @Override
  public String MOVEMENT_SPEED() {
    return "MOVEMENT_SPEED";
  }
}
