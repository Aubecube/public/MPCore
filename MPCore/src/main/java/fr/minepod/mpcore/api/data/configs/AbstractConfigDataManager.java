/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data.configs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.data.DataPolicy;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractConfigDataManager.
 *
 * @param <P> the generic type
 * @param <K> the key type
 * @param <V> the value type
 */
public abstract class AbstractConfigDataManager<P extends JavaPlugin, K, V>
    extends ConcurrentHashMap<K, V> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6791592079315274756L;

  /** All AbstractImprovedDataManager. */
  public static List<AbstractConfigDataManager<?, ?, ?>> managers = new LinkedList<>();

  /** The plugin. */
  private P plugin;

  /** The file. */
  private File file;

  /** The type. */
  private DataPolicy type;

  /** The config. */
  private FileConfiguration config;

  /** The proxied. */
  private boolean proxied = false;

  /** The proxied methods. */
  private List<String> proxiedMethods = new ArrayList<>();

  /**
   * Instantiates a new abstract config data manager.
   *
   * @param plugin the plugin
   * @param file the file
   * @param type the type
   */
  public AbstractConfigDataManager(P plugin, String file, DataPolicy type) {
    this.plugin = plugin;
    this.file = new File(plugin.getDataFolder(), file);
    this.type = type;

    managers.add(this);
  }

  /**
   * Load.
   */
  public void load() {
    load(true);
  }

  /**
   * Load.
   * 
   * @param get get or not
   */
  public void load(boolean get) {
    config = YamlConfiguration.loadConfiguration(file);

    if (get) {
      get();
    }
  }

  /**
   * Save.
   *
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public void save() throws IOException {
    set();

    file.getParentFile().mkdirs();
    file.createNewFile();

    config.save(file);
  }

  /**
   * Gets the plugin.
   *
   * @return the plugin
   */
  public P getPlugin() {
    return plugin;
  }


  /**
   * Gets the file.
   *
   * @return the file
   */
  public File getFile() {
    return file;
  }

  /**
   * Gets the type.
   *
   * @return the type
   */
  public DataPolicy getType() {
    return type;
  }

  /**
   * Gets the config.
   *
   * @return the config
   */
  public FileConfiguration getConfig() {
    return config;
  }

  /**
   * Checks if is proxied.
   *
   * @return true, if is proxied
   */
  public boolean isProxied() {
    return proxied;
  }

  /**
   * Sets the proxied.
   *
   * @param proxied the new proxied
   */
  public void setProxied(boolean proxied) {
    this.proxied = proxied;
  }

  /**
   * Gets the proxied methods.
   *
   * @return the proxied methods
   */
  public List<String> getProxiedMethods() {
    return proxiedMethods;
  }

  /**
   * Sets the proxied methods.
   *
   * @param proxiedMethods the new proxied methods
   */
  public void setProxiedMethods(List<String> proxiedMethods) {
    this.proxiedMethods = proxiedMethods;
  }

  /**
   * Gets.
   */
  public abstract void get();

  /**
   * Sets.
   */
  public abstract void set();
}
