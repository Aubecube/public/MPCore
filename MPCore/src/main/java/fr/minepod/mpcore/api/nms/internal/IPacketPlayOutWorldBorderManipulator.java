/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.internal;

// TODO: Auto-generated Javadoc

/**
 * The Interface IPacketPlayOutWorldBorderManipulator.
 */
public interface IPacketPlayOutWorldBorderManipulator {

  /**
   * Gets the border class.
   *
   * @return the border class
   * @throws ClassNotFoundException the class not found exception
   */
  Class<?> getWorldBorderClass() throws ClassNotFoundException;

  /**
   * Gets the enum class.
   *
   * @return the enum class
   * @throws ClassNotFoundException the class not found exception
   */
  Class<?> getEnumWorldBorderActionClass() throws ClassNotFoundException;

  /**
   * Gets the packet class.
   *
   * @return the packet class
   * @throws ClassNotFoundException the class not found exception
   */
  Class<?> getPacketPlayOutWorldBorderClass() throws ClassNotFoundException;

  /**
   * Sets the center method.
   *
   * @param x the x
   * @param z the z
   * @return the string
   */
  String setCenterMethod(double x, double z);

  /**
   * Sets the initial size method.
   *
   * @param size the size
   * @return the string
   */
  String setInitialSizeMethod(double size);

  /**
   * Change size method.
   *
   * @param oldSize the old size
   * @param newSize the new size
   * @param speed the speed
   * @return the string
   */
  String changeSizeMethod(double oldSize, double newSize, long speed);

  /**
   * Sets the warning blocks method.
   *
   * @param blocks the blocks
   * @return the string
   */
  String setWarningBlocksMethod(int blocks);

  /**
   * Sets the warning time method.
   *
   * @param time the time
   * @return the string
   */
  String setWarningTimeMethod(int time);
}
