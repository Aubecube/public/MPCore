/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.commands.exceptions;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandArgumentException.
 */
public class CommandArgumentException extends Exception {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 714126451526416716L;

  /** The number. */
  private int number;

  /** The type. */
  private String type;

  /**
   * Instantiates a new command argument exception.
   *
   * @param number the number
   * @param type the type
   */
  public CommandArgumentException(int number, String type) {
    super("Wrong argument type " + type + " (" + number + ")");

    this.number = number;
    this.type = type;
  }

  /**
   * Gets the number.
   *
   * @return the number
   */
  public int getNumber() {
    return number;
  }

  /**
   * Gets the type.
   *
   * @return the type
   */
  public String getType() {
    return type;
  }
}
