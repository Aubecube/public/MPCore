/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.data;

// TODO: Auto-generated Javadoc
/**
 * The Enum DataPolicy.
 */
public enum DataPolicy {

  /** The config. */
  CONFIG(false, true, true, false),
  /** The load only. */
  LOAD_ONLY(false, true, false, false),
  /** The nothing. */
  NOTHING(false, false, false, false),
  /** The regular. */
  REGULAR(true, true, false, true);

  /** The autosave. */
  private boolean autosave;

  /** The load. */
  private boolean load;

  /** The force save. */
  private boolean forceSave;

  /** The save. */
  private boolean save;

  /**
   * Instantiates a new data policy.
   *
   * @param autosave the autosave
   * @param load the load
   * @param forceSave the force save
   * @param save the save
   */
  private DataPolicy(boolean autosave, boolean load, boolean forceSave, boolean save) {
    this.autosave = autosave;
    this.load = load;
    this.forceSave = forceSave;
    this.save = save;
  }

  /**
   * Checks if is autosave.
   *
   * @return true, if is autosave
   */
  public boolean isAutosave() {
    return autosave;
  }

  /**
   * Checks if is load.
   *
   * @return true, if is load
   */
  public boolean isLoad() {
    return load;
  }

  /**
   * Checks if is force save.
   *
   * @return true, if is force save
   */
  public boolean isForceSave() {
    return forceSave;
  }

  /**
   * Checks if is save.
   *
   * @return true, if is save
   */
  public boolean isSave() {
    return save;
  }
}
