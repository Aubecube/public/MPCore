/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.permissions;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.MPCore;
import lombok.Data;

// TODO: Auto-generated Javadoc
/**
 * The Class PermissionsManager.
 */
public class DynamicPermissionsRegistry {

  /** The holders. */
  static Map<String, DynamicPermissionsHolder> holders = new HashMap<>();

  /**
   * Register.
   *
   * @param key the key
   * @param plugin the plugin
   * @param listener the listener
   */
  public static void register(String key, JavaPlugin plugin, DynamicPermissions listener) {
    holders.put(key, new DynamicPermissionsHolder(plugin, listener));
  }

  /**
   * Unregister.
   *
   * @param key the key
   */
  public static void unregister(String key) {
    holders.remove(key);
  }

  /**
   * Checks for permission.
   *
   * @param player the player
   * @param permission the permission
   * @return true, if successful
   */
  public static boolean hasPermission(Player player, String permission) {
    // length of "dynamic:" = 8
    permission = permission.substring(8);

    for (String pattern : permission.split(";")) {
      if (!pattern.isEmpty()) {
        String[] parts = pattern.split(":", 2);

        if (parts.length < 2) {
          MPCore.get().getLogger().severe("The permission pattern '" + pattern + "' is invalid.");
          return false;
        }

        for (Entry<String, DynamicPermissionsHolder> holder : holders.entrySet()) {
          if (holder.getKey().equalsIgnoreCase(parts[0])
              && (!holder.getValue().getPlugin().isEnabled()
                  || !holder.getValue().getListener().hasPermission(player, parts[1]))) {
            return false;
          }
        }
      }
    }

    return true;
  }
}


@Data
class DynamicPermissionsHolder {
  private final JavaPlugin plugin;
  private final DynamicPermissions listener;
}
