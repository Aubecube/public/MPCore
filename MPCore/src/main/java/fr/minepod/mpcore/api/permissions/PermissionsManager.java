/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.permissions;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minepod.mpcore.MPCore;

// TODO: Auto-generated Javadoc
/**
 * The Class PermissionsManager.
 */
public class PermissionsManager {

  /** The sender. */
  CommandSender sender;

  /**
   * Instantiates a new permissions manager.
   * 
   * @param sender the sender
   * @deprecated use the static method instead
   */
  @Deprecated
  public PermissionsManager(CommandSender sender) {
    this.sender = sender;
  }

  /**
   * Instantiates a new permissions manager.
   * 
   * @param sender the sender
   * @deprecated use the static method instead
   */
  @Deprecated
  public PermissionsManager(Player sender) {
    this.sender = sender;
  }

  /**
   * Checks if the player has the given permission.
   * 
   * @param permission the permission
   * @return true, if successful
   */
  public boolean has(String permission) {
    return hasSender(sender, permission);
  }

  /**
   * Static method. Checks if the player has the given permission.
   * 
   * @param sender the sender
   * @param permission the permission
   * @return true, if successful
   */
  public static boolean hasSender(CommandSender sender, String permission) {
    if (permission == null || permission.isEmpty()) {
      return true;
    } else if (permission.startsWith("dynamic:") && sender instanceof Player) {
      return DynamicPermissionsRegistry.hasPermission((Player) sender, permission);
    } else if (MPCore.permission != null) {
      return MPCore.permission.has(sender, permission);
    } else {
      return sender.hasPermission(permission);
    }
  }
}
