/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.commands.exceptions;

// TODO: Auto-generated Javadoc
/**
 * The Class WrongCommandException.
 */
// TODO: Auto-generated Javadoc
public class WrongCommandException extends Exception {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5165840242252823848L;

  /**
   * Instantiates a new wrong command exception.
   *
   * @param message the message
   */
  public WrongCommandException(String message) {
    super(message);
  }
}
