/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.messages;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

// TODO: Auto-generated Javadoc
/**
 * The Class FancyChat.
 */
public class FancyChat {

  /** The builder. */
  private StringBuilder builder = new StringBuilder(ChatColor.DARK_AQUA.toString());

  /** The first. */
  private boolean first = true;

  /**
   * Factory helper.
   *
   * @return the fancy chat
   */
  public static FancyChat create() {
    return new FancyChat();
  }

  /**
   * Appends a message with no formatting but a space before if needed.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat append(String message) {
    if (!first) {
      builder.append(" ");
    } else {
      first = false;
    }

    builder.append(message);

    return this;
  }

  /**
   * Appends a message with no formatting at all.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat raw(String message) {
    if (first) {
      first = false;
    }

    builder.append(message);

    return this;
  }

  /**
   * Appends a message formatted as 'normal'.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat normal(String message) {
    builder.append(ChatColor.DARK_AQUA);
    append(message);

    return this;
  }

  /**
   * Appends a message formatted as a 'success'.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat success(String message) {
    builder.append(ChatColor.GREEN);
    append(message);

    return this;
  }

  /**
   * Appends a message formatted as an 'error'.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat error(String message) {
    builder.append(ChatColor.RED);
    append(message);

    return this;
  }

  /**
   * Appends a message formatted as a 'value'.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat value(String message) {
    builder.append(ChatColor.YELLOW);
    append(message);

    return this;
  }

  /**
   * Appends an highlighted message.
   *
   * @param message the message
   * @return the fancy chat
   */
  public FancyChat highlight(String message) {
    builder.append(ChatColor.GOLD);
    append(message);

    return this;
  }

  /**
   * Sends the message.
   *
   * @param sender the sender
   * @return the fancy chat
   */
  public FancyChat send(CommandSender sender) {
    sender.sendMessage(builder.toString());

    return this;
  }
}
