/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.integrations;

import java.lang.reflect.Method;
import java.util.logging.Logger;

import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

// TODO: Auto-generated Javadoc
/**
 * The Class IntegrationsManager.
 */
public class IntegrationsManager {

  /** The server. */
  Server server;

  /** The object. */
  Object object;

  /** The logger. */
  Logger logger;

  /** The debug enabled. */
  boolean debugEnabled;

  /** The debug level. */
  int debugLevel;

  /**
   * Instantiates a new integrations manager.
   * 
   * @param server the server
   * @param object the object
   */
  public IntegrationsManager(Server server, Object object) {
    this(server, object, server.getLogger());
  }

  /**
   * Instantiates a new integrations manager.
   * 
   * @param server the server
   * @param object the object
   * @param logger the logger
   */
  public IntegrationsManager(Server server, Object object, Logger logger) {
    this(server, object, logger, false, 0);
  }

  /**
   * Instantiates a new integrations manager.
   *
   * @param server the server
   * @param object the object
   * @param logger the logger
   * @param debugEnabled the debug enabled
   * @param debugLevel the debug level
   */
  public IntegrationsManager(Server server, Object object, Logger logger, boolean debugEnabled,
      int debugLevel) {
    this.server = server;
    this.object = object;
    this.logger = logger;
    this.debugEnabled = debugEnabled;
    this.debugLevel = debugLevel;
  }

  /**
   * Integrate.
   * 
   * @param plugin the plugin to integrate
   * @param enabled if the integration is enabled or not
   * @param integrate the method to execute when integrated
   * @return true, if successful
   */
  public boolean integrate(String plugin, boolean enabled, Method integrate) {
    try {
      debug(2, "Integration with " + plugin + " is set to: " + enabled);

      if (enabled) {
        debug(1, "Trying to enable integration with " + plugin + "...");

        boolean exists = false;
        for (Plugin temp : server.getPluginManager().getPlugins()) {
          if (temp != null && temp.getName().equalsIgnoreCase(plugin) && temp.isEnabled()) {
            exists = true;
          }
        }

        if (exists) {
          debug(1, "Integration with " + plugin + " is enabled!");
          integrate.setAccessible(true);
          if ((boolean) integrate.invoke(object, plugin)) {
            debug(1, "Integration with " + plugin + " is done!");
            return true;
          } else {
            debug(1, "Integration with " + plugin + " has failed!");
            return false;
          }
        } else {
          logger.warning("Integration with " + plugin + " is disabled: plugin not found!");
        }
      } else {
        debug(1, "Integration with " + plugin + " is disabled: config");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return false;
  }

  /**
   * Debug.
   * 
   * @param level the debug level, 0 for disabling
   * @param message the message
   */
  public void debug(int level, String message) {
    if (debugEnabled && level <= debugLevel) {
      logger.info("[IntegrationsManager] " + message);
    }
  }
}
