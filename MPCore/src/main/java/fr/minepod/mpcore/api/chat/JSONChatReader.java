/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.chat;

import org.bukkit.ChatColor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

// TODO: Auto-generated Javadoc
/**
 * The Class JSONChatReader.
 */
public class JSONChatReader {

  /** The json. */
  JSONObject json;

  /**
   * Instantiates a new JSON chat reader.
   *
   * @param json the json
   */
  public JSONChatReader(JSONObject json) {
    this.json = json;
  }

  /**
   * Gets the chat string.
   *
   * @return the chat string
   */
  public String getChatString() {
    StringBuilder builder = new StringBuilder();

    if (json.containsKey("color")) {
      builder.append(getChatColor((String) json.get("color")).getChar());
    }

    builder.append(getIfStyle(json));

    if (json.containsKey("text")) {
      builder.append((String) json.get("text"));
    }

    if (json.containsKey("extra")) {
      for (Object object : (JSONArray) json.get("extra")) {
        try {
          JSONObject jsonObject = (JSONObject) object;

          if (jsonObject.containsKey("color")) {
            builder.append(getStringColor(getChatColor((String) jsonObject.get("color"))));
          }

          builder.append(getIfStyle(jsonObject));

          builder.append((String) jsonObject.get("text"));
        } catch (ClassCastException ignored) {
          builder.append((String) object);
        }
      }
    }

    return builder.toString();
  }

  /**
   * Gets the style, if present.
   *
   * @param json the json
   * @return the style, if present
   */
  private String getIfStyle(JSONObject json) {
    StringBuilder builder = new StringBuilder();

    if (json.containsKey("bold")) {
      if ((boolean) json.get("bold")) {
        builder.append(getStringColor(ChatColor.BOLD));
      }
    }

    if (json.containsKey("italic")) {
      if ((boolean) json.get("italic")) {
        builder.append(getStringColor(ChatColor.ITALIC));
      }
    }

    if (json.containsKey("underlined")) {
      if ((boolean) json.get("underlined")) {
        builder.append(getStringColor(ChatColor.UNDERLINE));
      }
    }

    if (json.containsKey("strikethrough")) {
      if ((boolean) json.get("strikethrough")) {
        builder.append(getStringColor(ChatColor.STRIKETHROUGH));
      }
    }

    if (json.containsKey("obfuscated")) {
      if ((boolean) json.get("obfuscated")) {
        builder.append(getStringColor(ChatColor.MAGIC));
      }
    }

    return builder.toString();
  }

  /**
   * Gets the chat color.
   *
   * @param color the color
   * @return the chat color
   */
  private ChatColor getChatColor(String color) {
    for (ChatColor chatColor : ChatColor.values()) {
      if (chatColor.name().equalsIgnoreCase(color)) {
        return chatColor;
      }
    }

    return ChatColor.WHITE;
  }

  /**
   * Gets the string color.
   *
   * @param color the color
   * @return the string color
   */
  private String getStringColor(ChatColor color) {
    return "§" + color.getChar();
  }
}
