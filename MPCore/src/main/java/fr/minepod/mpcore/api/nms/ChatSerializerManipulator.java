/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import fr.minepod.mpcore.api.nms.internal.IChatSerializerManipulator;
import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class ChatSerializerManipulator.
 */
public class ChatSerializerManipulator {

  /** The serialize. */
  static Method serialize;

  /** The internal. */
  static IChatSerializerManipulator internal;

  static {
    try {
      internal =
          NMSBase.getVersionedClass("ChatSerializerManipulator", IChatSerializerManipulator.class);

      serialize = internal.getChatSerializerClass().getDeclaredMethod(internal.serializeMethod(),
          String.class);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Gets the i chat base component.
   *
   * @param text the text
   * @return the i chat base component
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static Object getIChatBaseComponent(String text)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    if (text != null) {
      return Reflection.invokeMethod(null, serialize, text);
    } else {
      return null;
    }
  }

  /**
   * Gets the i chat component class.
   *
   * @return the i chat component class
   * @throws ClassNotFoundException the class not found exception
   */
  public static Class<?> getIChatComponentClass() throws ClassNotFoundException {
    return internal.getIChatComponentClass();
  }
}
