/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.functions;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.entity.Player;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.PlayerClass;

// TODO: Auto-generated Javadoc
/**
 * The Class FunctionsManager.
 */
public class FunctionsManager {

  /**
   * Returns a random int between min and max.
   * 
   * @param min the minimal value
   * @param max the maximal value
   * @return the random int
   */
  public static int randomMinMax(int min, int max) {
    return min + (int) (Math.random() * ((max - min) + 1));
  }

  /**
   * Returns the minimal value of the given array.
   * 
   * @param args the array of values
   * @return the minimal value
   */
  public static int min(int... args) {
    Integer min = null;
    for (int temp : args) {
      if (min == null || min > temp) {
        min = temp;
      }
    }

    return min;
  }

  /**
   * Returns the maximal value of the given array.
   * 
   * @param args the array of values
   * @return the maximal value
   */
  public static int max(int... args) {
    Integer max = null;
    for (int temp : args) {
      if (max == null || max < temp) {
        max = temp;
      }
    }

    return max;
  }

  /**
   * Checks if is integer.
   * 
   * @param s the string to check
   * @return true, if is integer
   */
  public static boolean isInteger(String s) {
    try {
      Integer.parseInt(s);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Checks if is double.
   * 
   * @param s the string to check
   * @return true, if is double
   */
  public static boolean isDouble(String s) {
    try {
      Double.parseDouble(s);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Checks if is long.
   * 
   * @param s the string to check
   * @return true, if is long
   */
  public static boolean isLong(String s) {
    try {
      Long.parseLong(s);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Checks if is float.
   * 
   * @param s the string to check
   * @return true, if is float
   */
  public static boolean isFloat(String s) {
    try {
      Float.parseFloat(s);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Inverts map.
   * 
   * @param <V> the value type
   * @param <K> the key type
   * @param map the map to invert
   * @return the inverted map
   */
  public static <V, K> Map<V, K> invertMap(Map<K, V> map) {
    Map<V, K> inverted = new HashMap<>();

    if (map.size() > 0) {
      for (Entry<K, V> entry : map.entrySet()) {
        inverted.put(entry.getValue(), entry.getKey());
      }
    }

    return inverted;
  }

  /**
   * Formats the given time and outputs a string.
   * 
   * @param time the time in seconds
   * @param units the units
   * @return the formatted string
   */
  public static String formatTimeToString(int time, LinkedHashMap<Integer, String> units) {
    return formatTimeToString((float) time, units);
  }

  /**
   * Formats the given time and outputs a string.
   * 
   * @param time the time in seconds
   * @param units the units
   * @return the formatted string
   */
  public static String formatTimeToString(float time, LinkedHashMap<Integer, String> units) {
    if (time != 0) {
      StringBuilder builder = new StringBuilder();

      int i = 1;
      for (Entry<Integer, String> unit : units.entrySet()) {
        if (time >= unit.getKey()) {
          float number = time / (float) unit.getKey();
          time = time % (float) unit.getKey();

          if (i != units.size()) {
            builder.append((int) Math.floor(number));
          } else {
            builder.append(number);
          }

          builder.append(unit.getValue());
        }

        i++;
      }

      return builder.toString();
    } else {
      return String.valueOf(0);
    }
  }

  /**
   * Object from string. Original method in MenuPlus.
   *
   * @param value the value
   * @return the object
   */
  public static Object objectFromString(String value) {
    String[] parts = value.split(":");

    if (parts.length == 2) {
      Object object;

      try {
        switch (parts[0].toLowerCase()) {
          case "int":
            object = Integer.parseInt(parts[1]);
            break;
          case "float":
            object = Float.parseFloat(parts[1]);
            break;
          case "double":
            object = Double.parseDouble(parts[1]);
            break;
          case "long":
            object = Long.parseLong(parts[1]);
            break;
          case "uuid":
            object = UUID.fromString(parts[1]);
            break;
          case "boolean":
            object = Boolean.parseBoolean(parts[1]);
            break;
          default:
            throw new IllegalArgumentException("Wrong type parameter.");
        }
      } catch (Exception ignored) {
        object = parts[1];
      }

      return object;
    } else {
      return value;
    }
  }

  /**
   * Checks if is command matching pattern.
   *
   * @param name the name
   * @param args the args
   * @param matches the matches
   * @return true, if is command matching pattern
   */
  public static boolean isCommandMatchingPattern(String name, String[] args, String[] matches) {
    if (args.length < matches.length - 1) {
      return false;
    }

    int i = -1;
    for (String match : matches) {
      if (i == -1) {
        if (!name.equalsIgnoreCase(match)) {
          return false;
        }
      } else {
        if (match == "*+") {
          return true;
        } else if (!match.equalsIgnoreCase(args[i]) && match != "*") {
          return false;
        }
      }

      i++;
    }

    return true;
  }

  /**
   * Gets a random entry.
   *
   * @param <T> the generic type
   * @param list the list
   * @return a random entry
   */
  public static <T> T getRandomEntry(List<T> list) {
    if (!list.isEmpty()) {
      return list.get(randomMinMax(0, list.size() - 1));
    } else {
      throw new IllegalArgumentException("Empty list!");
    }
  }

  /**
   * Gets a player's playtime in seconds.
   * 
   * @param uuid the uuid
   * @return the playtime in seconds
   */
  public static long getPlayerPlayTimeInSeconds(UUID uuid) {
    long time = 0;

    for (Entry<Player, Long> entry : MPCore.get().playerListener.playersConnectionTime.entrySet()) {
      if (entry.getKey().getUniqueId().equals(uuid)) {
        if (entry.getValue() != null) {
          time = System.currentTimeMillis() - entry.getValue();
        }

        break;
      }
    }

    PlayerClass player = MPCore.get().players.getFor(uuid);
    if (player != null) {
      Long playTime = player.getPlayTimes().get(MPCore.getConfigData().getRethinkDBProfile());
      if (playTime != null) {
        time += playTime;
      }
    }

    return time / 1000;
  }
}
