/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.nms.experimental;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import fr.minepod.mpcore.api.classes.AdvancementDisplayHolder;
import fr.minepod.mpcore.api.classes.AdvancementHolder;
import fr.minepod.mpcore.api.classes.FrameTypeHolder;
import fr.minepod.mpcore.api.nms.ChatSerializerManipulator;
import fr.minepod.mpcore.api.nms.internal.PlayerConnectionWrapper;
import net.minecraft.server.v1_14_R1.Advancement;
import net.minecraft.server.v1_14_R1.AdvancementDisplay;
import net.minecraft.server.v1_14_R1.AdvancementFrameType;
import net.minecraft.server.v1_14_R1.AdvancementProgress;
import net.minecraft.server.v1_14_R1.AdvancementRewards;
import net.minecraft.server.v1_14_R1.Criterion;
import net.minecraft.server.v1_14_R1.CriterionInstanceAbstract;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import net.minecraft.server.v1_14_R1.MinecraftKey;
import net.minecraft.server.v1_14_R1.PacketPlayOutAdvancements;

// TODO: Auto-generated Javadoc
/**
 * The Class AdvancementsPacketManipulator.
 */
public class AdvancementsPacketManipulator {

  /** The advancements. */
  private static Map<String, Object> advancements = new HashMap<>();

  /**
   * Advancement from map.
   *
   * @param map the map
   * @return the advancement holder
   */
  public static AdvancementHolder advancementFromMap(Map<String, Object> map) {
    String name = (String) map.get("name");

    AdvancementDisplayHolder display = null;
    if (map.containsKey("display")) {
      @SuppressWarnings("unchecked")
      Map<String, Object> child = (Map<String, Object>) map.get("display");

      ItemStack item = new ItemStack((Material) child.get("material"));
      if (child.containsKey("data")) {
        ItemMeta meta;
        if (item.hasItemMeta()) {
          meta = item.getItemMeta();
        } else {
          meta = Bukkit.getItemFactory().getItemMeta(item.getType());
        }

        Damageable damage = (Damageable) meta;
        damage.setDamage((int) child.get("data"));

        item.setItemMeta((ItemMeta) damage);
      }

      display = new AdvancementDisplayHolder(item, (String) child.get("title"),
          (String) child.get("description"), (String) child.get("background"),
          (FrameTypeHolder) child.get("frame"), (boolean) child.get("show_toast"),
          (boolean) child.get("hidden"), (float) child.get("x"), (float) child.get("y"));
    }

    AdvancementHolder holder = new AdvancementHolder((String) map.get("parent"), name, display);
    advancements.put(name, holder);

    return holder;
  }

  // http://wiki.vg/Protocol#Advancements
  // "reset/clear": clear
  // "advancement mapping": advancements
  // "identifiers": identifiers
  /**
   * Send packet.
   *
   * @param player the player
   * @param clear the clear
   * @param advancements the advancements
   * @param identifiers the identifiers
   * @param progress the progress
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws NoSuchFieldException the no such field exception
   */
  // "progress mapping": progress
  public static void sendPacket(Player player, boolean clear, Set<AdvancementHolder> advancements,
      Set<String> identifiers, Map<String, Boolean> progress)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException,
      NoSuchMethodException, SecurityException, NoSuchFieldException {
    Collection<Advancement> advancementMapping = new HashSet<>();
    for (AdvancementHolder holder : advancements) {
      advancementMapping.add(advancementFromHolder(holder));
    }

    Set<MinecraftKey> identifiersMapping = new HashSet<>();
    for (String identifier : identifiers) {
      identifiersMapping.add(new MinecraftKey(identifier));
    }

    // advancement name, advancement progress
    Map<MinecraftKey, AdvancementProgress> progressMapping = new HashMap<>();
    for (Entry<String, Boolean> entry : progress.entrySet()) {
      AdvancementProgress advancementProgress = new AdvancementProgress();

      // criterion name, criterion instance
      Map<String, Criterion> criterions = new HashMap<>();
      // impossible trigger
      Criterion criterion =
          new Criterion(new CriterionInstanceAbstract(new MinecraftKey("impossible")));
      // TODO: Name?!
      criterions.put(entry.getKey(), criterion);

      String[][] requirements = {{entry.getKey()}};
      advancementProgress.a(criterions, requirements);

      if (entry.getValue()) {
        advancementProgress.a(entry.getKey());
      } else {
        advancementProgress.b(entry.getKey());
      }

      progressMapping.put(new MinecraftKey(entry.getKey()), advancementProgress);
    }

    PlayerConnectionWrapper.sendPacket(player, PacketPlayOutAdvancements.class,
        new PacketPlayOutAdvancements(clear, advancementMapping, identifiersMapping,
            progressMapping));
  }

  /**
   * Advancement from holder.
   *
   * @param holder the holder
   * @return the advancement
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  private static Advancement advancementFromHolder(AdvancementHolder holder)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Object key = new MinecraftKey(holder.getName());
    Object parent = holder.getParent() == null ? null
        : advancementFromHolder((AdvancementHolder) advancements.get(holder.getParent()));

    AdvancementDisplay display = null;
    if (holder.getDisplay() != null) {
      Object item = CraftItemStack.asNMSCopy(holder.getDisplay().getItem());
      Object title =
          ChatSerializerManipulator.getIChatBaseComponent(holder.getDisplay().getTitle());
      Object description =
          ChatSerializerManipulator.getIChatBaseComponent(holder.getDisplay().getDescription());

      Object background = holder.getDisplay().getBackground() == null ? null
          : new MinecraftKey(holder.getDisplay().getBackground());

      Object frame = AdvancementFrameType.valueOf(holder.getDisplay().getFrame().name());

      display = new AdvancementDisplay((net.minecraft.server.v1_14_R1.ItemStack) item,
          (IChatBaseComponent) title, (IChatBaseComponent) description, (MinecraftKey) background,
          (AdvancementFrameType) frame, holder.getDisplay().isShow_toast(),
          holder.getDisplay().isG(), holder.getDisplay().isHidden());

      display.a(holder.getDisplay().getX(), holder.getDisplay().getY());
    }

    // TODO: Types
    Map<String, Criterion> criterions = new HashMap<>();
    // impossible trigger
    Criterion criterion =
        new Criterion(new CriterionInstanceAbstract(new MinecraftKey("impossible")));

    criterions.put(holder.getName(), criterion);

    // "a" is a static empty reward
    String[][] requirements = {{holder.getName()}};
    Advancement advancement = new Advancement((MinecraftKey) key, (Advancement) parent, display,
        AdvancementRewards.a, criterions, requirements);

    return advancement;
  }
}
