/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.api.hooks;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Server;

import fr.minepod.mpcore.api.integrations.IntegrationsManager;

// TODO: Auto-generated Javadoc
/**
 * The Class HooksManager.
 */
public class HooksManager {

  /** The server. */
  Server server;

  /**
   * Instantiates a new hooks manager.
   * 
   * @param server the server
   */
  public HooksManager(Server server) {
    this.server = server;
  }

  /**
   * Execute.
   * 
   * @param plugins the plugins
   * @return the data
   */
  public List<String> execute(String... plugins) {
    return execute(Arrays.asList(plugins));
  }

  /**
   * Execute.
   * 
   * @param plugins the plugins
   * @return the data
   */
  public List<String> execute(List<String> plugins) {
    IntegrationsManager integrationsManager = new IntegrationsManager(server, this);
    List<String> list = new ArrayList<String>();

    for (String temp : plugins) {
      try {
        if (integrationsManager.integrate(temp.replace("-", "").replace("_", ""), true,
            this.getClass().getMethod("integrate", String.class))) {
          list.add(temp);
        }
      } catch (SecurityException | NoSuchMethodException e) {
        e.printStackTrace();
      }
    }

    return list;
  }

  /**
   * Integrate.
   * 
   * @param plugin the plugin
   * @return true, if successful
   */
  public boolean integrate(String plugin) {
    for (Method temp : this.getClass().getDeclaredMethods()) {
      if (temp.getName().equalsIgnoreCase(plugin)) {
        try {
          temp.setAccessible(true);
          temp.invoke(this);

          return true;
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
          e.printStackTrace();
        }
      }
    }

    return false;
  }
}
