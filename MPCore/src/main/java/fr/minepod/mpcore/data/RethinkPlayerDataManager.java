/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import com.rethinkdb.gen.ast.IndexCreate;

import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.rethinkdb.AbstractRethinkDataManager;

// TODO: Auto-generated Javadoc
/**
 * The Class RethinkPlayerDataManager.
 */
public class RethinkPlayerDataManager extends AbstractRethinkDataManager<UUID, PlayerClass> {
  /**
   * Instantiates a new Rethink player data manager.
   *
   * @param plugin the plugin
   */
  public RethinkPlayerDataManager(JavaPlugin plugin) {
    super(plugin, "mpcore_users", table -> table.changes(), false, false);
  }

  /**
   * Creates the indexes.
   *
   * @return the list
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.rethinkdb.AbstractRethinkDataManager#createIndexes()
   */
  @Override
  public List<IndexCreate> createIndexes() {
    ArrayList<IndexCreate> indexes = new ArrayList<>();

    indexes.add(getTable().indexCreate("name"));

    return indexes;
  }

  /**
   * Key to string.
   *
   * @param key the key
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.rethinkdb.AbstractRethinkDataManager#keyToString(java.lang.Object)
   */
  @Override
  public String keyToString(UUID key) {
    return key.toString();
  }

  /**
   * String to key.
   *
   * @param key the key
   * @return the uuid
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.rethinkdb.AbstractRethinkDataManager#stringToKey(java.lang.String)
   */
  @Override
  public UUID stringToKey(String key) {
    return UUID.fromString(key);
  }

  /**
   * Provide.
   *
   * @param map the map
   * @return the player class
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.rethinkdb.AbstractRethinkDataManager#provide(java.util.Map)
   */
  @Override
  public PlayerClass provide(Map<String, Object> map) {
    if (map != null) {
      UUID uuid = UUID.fromString((String) map.get("id"));
      String name = (String) map.get("name");
      @SuppressWarnings("unchecked")
      Map<String, Long> oldNames = (Map<String, Long>) map.get("oldNames");
      long firstSeen = (long) map.get("firstSeen");
      long lastSeen = (long) map.get("lastSeen");
      @SuppressWarnings("unchecked")
      Map<String, Long> playTimes = (Map<String, Long>) map.get("playTimes");

      return new PlayerClass(uuid, name, oldNames, firstSeen, lastSeen, playTimes);
    }

    return null;
  }
}
