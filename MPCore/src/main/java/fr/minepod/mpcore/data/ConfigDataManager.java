/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.data.AbstractDataManager;

// TODO: Auto-generated Javadoc
/**
 * The Class ConfigDataManager.
 */
public class ConfigDataManager extends AbstractDataManager<String, Object> {

  /**
   * Instantiates a new config data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   * @param autosave the autosave
   * @param load the load
   * @param forceSave the force save
   * @param save the save
   */
  public ConfigDataManager(JavaPlugin plugin, String fileName, boolean autosave, boolean load,
      boolean forceSave, boolean save) {
    super(plugin, fileName, autosave, load, forceSave, save);
  }

  /**
   * Gets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#get()
   */
  @Override
  public void get() {
    Map<String, Object> data = new HashMap<>();

    data.put("messages.plugin-prefix", getConfig().getBoolean("messages.plugin-prefix", true));

    data.put("track.player-time", getConfig().getBoolean("track.player-time", true));
    data.put("track.player-afk-time", getConfig().getBoolean("track.player-afk-time", false));

    data.put("logs.retention-days.chat", getConfig().getInt("logs.retention-days.chat", 7));
    data.put("logs.retention-days.playtime", getConfig().getInt("logs.retention-days.chat", 7));

    data.put("plugins.autosave.enabled", getConfig().getBoolean("plugins.autosave.enabled", false));
    data.put("plugins.autosave.blacklist", getConfig().getStringList("plugins.autosave.blacklist"));
    data.put("plugins.autosave.delay", getConfig().getInt("plugins.autosave.delay", 3600));

    data.put("commands-registry.commands-list-message",
        getConfig().getString("commands-registry.commands-list-message", "§6Commands:"));
    data.put("commands-registry.wrong-usage-message",
        getConfig().getString("commands-registry.wrong-usage-message", "§cMissing parameters."));
    data.put("commands-registry.no-permission-message",
        getConfig().getString("commands-registry.no-permission-message",
            "§cSorry, you don't have the permission to do this."));

    data.put("commandblocks-compat.disabled",
        getConfig().getBoolean("commandblocks-compat.disabled", true));
    data.put("commandblocks-compat.block-whitelist.enabled",
        getConfig().getBoolean("commandblocks-compat.block-whitelist.enabled", false));
    data.put("commandblocks-compat.op-whitelist.enabled",
        getConfig().getBoolean("commandblocks-compat.op-whitelist.enabled", false));
    data.put("commandblocks-compat.block-whitelist.commands",
        getConfig().getStringList("commandblocks-compat.block-whitelist.commands"));
    data.put("commandblocks-compat.op-whitelist.commands",
        getConfig().getStringList("commandblocks-compat.op-whitelist.commands"));
    data.put("commandblocks-compat.op-whitelist.plugins",
        getConfig().getStringList("commandblocks-compat.op-whitelist.plugins"));
    data.put("commandblocks-compat.op-whitelist.bypass-uuids",
        getConfig().getStringList("commandblocks-compat.op-whitelist.bypass-uuids"));
    data.put("commandblocks-compat.debug-enabled",
        getConfig().getBoolean("commandblocks-compat.debug-enabled", false));
    data.put("commandblocks-compat.silent",
        getConfig().getBoolean("commandblocks-compat.silent", true));

    data.put("rabbitmq.enabled", getConfig().getBoolean("rabbitmq.enabled", false));
    data.put("rabbitmq.host", getConfig().getString("rabbitmq.host", "localhost"));
    data.put("rabbitmq.port", getConfig().getInt("rabbitmq.port", 5672));
    data.put("rabbitmq.username", getConfig().getString("rabbitmq.username", "username"));
    data.put("rabbitmq.password", getConfig().getString("rabbitmq.password", "password"));
    data.put("rabbitmq.profile", getConfig().getString("rabbitmq.profile", "default"));

    data.put("rethinkdb.host", getConfig().getString("rethinkdb.host", "localhost"));
    data.put("rethinkdb.port", getConfig().getInt("rethinkdb.port", 28015));
    data.put("rethinkdb.database", getConfig().getString("rethinkdb.database", "database"));
    data.put("rethinkdb.username", getConfig().getString("rethinkdb.username", "username"));
    data.put("rethinkdb.password", getConfig().getString("rethinkdb.password", "password"));
    data.put("rethinkdb.profile", getConfig().getString("rethinkdb.profile", "default"));

    setData(data);
  }

  /**
   * Sets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#set()
   */
  @Override
  public void set() {
    for (Entry<String, Object> entry : getData().entrySet()) {
      getConfig().set(entry.getKey(), entry.getValue());
    }
  }

  /**
   * Checks if is messages plugin prefix enabled.
   *
   * @return true, if is messages plugin prefix enabled
   */
  public boolean isMessagesPluginPrefixEnabled() {
    return (boolean) data.get("messages.plugin-prefix");
  }

  /**
   * Checks if track player time is enabled.
   *
   * @return true, if track player time is enabled
   */
  public boolean isTrackPlayerTimeEnabled() {
    return (boolean) data.get("track.player-time");
  }

  /**
   * Checks if track player afk time is enabled.
   *
   * @return true, if track player afk time is enabled
   */
  public boolean isTrackPlayerAfkTimeEnabled() {
    return (boolean) data.get("track.player-afk-time");
  }

  /**
   * Gets the logs retention days chat.
   *
   * @return the logs retention days chat
   */
  public int getLogsRetentionDaysChat() {
    return (int) data.get("logs.retention-days.chat");
  }

  /**
   * Gets the logs retention days play time.
   *
   * @return the logs retention days play time
   */
  public int getLogsRetentionDaysPlayTime() {
    return (int) data.get("logs.retention-days.playtime");
  }

  /**
   * Checks if autosave is enabled.
   * 
   * @return true, if autosave is enabled
   */
  public boolean isAutosaveEnabled() {
    return (boolean) data.get("plugins.autosave.enabled");
  }

  /**
   * Gets the autosave blacklist.
   * 
   * @return the autosave blacklist
   */
  @SuppressWarnings("unchecked")
  public List<String> getAutosaveBlacklist() {
    return (List<String>) data.get("plugins.autosave.blacklist");
  }

  /**
   * Gets the autosave delay.
   * 
   * @return the autosave delay
   */
  public int getAutosaveDelay() {
    return (int) data.get("plugins.autosave.delay");
  }

  /**
   * Gets the commands registry commands list message.
   *
   * @return the commands registry commands list message
   */
  public String getCommandsRegistryCommandsListMessage() {
    return (String) data.get("commands-registry.commands-list-message");
  }

  /**
   * Gets the commands registry wrong usage message.
   *
   * @return the commands registry wrong usage message
   */
  public String getCommandsRegistryWrongUsageMessage() {
    return (String) data.get("commands-registry.wrong-usage-message");
  }

  /**
   * Gets the commands registry no permission message.
   *
   * @return the commands registry no permission message
   */
  public String getCommandsRegistryNoPermissionMessage() {
    return (String) data.get("commands-registry.no-permission-message");
  }

  /**
   * Checks if command block compat is disabled.
   *
   * @return true, if command block compat is disabled
   */
  public boolean isCommandBlockCompatDisabled() {
    return (boolean) data.get("commandblocks-compat.disabled");
  }

  /**
   * Checks if command block compat block white list is enabled.
   *
   * @return true, if command block compat block white list is enabled
   */
  public boolean isCommandBlockCompatBlockWhiteListEnabled() {
    return (boolean) data.get("commandblocks-compat.block-whitelist.enabled");
  }

  /**
   * Checks if command block compat op white list is enabled.
   *
   * @return true, if command block compat op white list is enabled
   */
  public boolean isCommandBlockCompatOpWhiteListEnabled() {
    return (boolean) data.get("commandblocks-compat.op-whitelist.enabled");
  }

  /**
   * Gets the command block compat block white list commands.
   *
   * @return the command block compat block white list commands
   */
  @SuppressWarnings("unchecked")
  public List<String> getCommandBlockCompatBlockWhiteListCommands() {
    return (List<String>) data.get("commandblocks-compat.block-whitelist.commands");
  }

  /**
   * Gets the command block compat op white list commands.
   *
   * @return the command block compat op white list commands
   */
  @SuppressWarnings("unchecked")
  public List<String> getCommandBlockCompatOpWhiteListCommands() {
    return (List<String>) data.get("commandblocks-compat.op-whitelist.commands");
  }

  /**
   * Gets the command block compat op white list plugins.
   *
   * @return the command block compat op white list plugins
   */
  @SuppressWarnings("unchecked")
  public List<String> getCommandBlockCompatOpWhiteListPlugins() {
    return (List<String>) data.get("commandblocks-compat.op-whitelist.plugins");
  }

  /**
   * Gets the command block compat op bypass uui ds.
   *
   * @return the command block compat op bypass uui ds
   */
  @SuppressWarnings("unchecked")
  public List<String> getCommandBlockCompatOpBypassUUIDs() {
    return (List<String>) data.get("commandblocks-compat.op-whitelist.bypass-uuids");
  }

  /**
   * Checks if command block compat debug is enabled.
   *
   * @return true, if command block compat debug is enabled
   */
  public boolean isCommandBlockCompatDebugEnabled() {
    return (boolean) data.get("commandblocks-compat.debug-enabled");
  }

  /**
   * Checks if command block compat is silent.
   *
   * @return true, if command block compat is silent
   */
  public boolean isCommandBlockCompatSilent() {
    return (boolean) data.get("commandblocks-compat.silent");
  }

  /**
   * Checks if RabbitMQ is enabled.
   * 
   * @return true, if RabbitMQ is enabled
   */
  public boolean isRabbitMQEnabled() {
    return (boolean) data.get("rabbitmq.enabled");
  }

  /**
   * Gets the RabbitMQ host.
   *
   * @return the RabbitMQ host
   */
  public String getRabbitMQHost() {
    return (String) data.get("rabbitmq.host");
  }

  /**
   * Gets the RabbitMQ port.
   *
   * @return the RabbitMQ port
   */
  public int getRabbitMQPort() {
    return (int) data.get("rabbitmq.port");
  }

  /**
   * Gets the RabbitMQ username.
   *
   * @return the RabbitMQ username
   */
  public String getRabbitMQUsername() {
    return (String) data.get("rabbitmq.username");
  }

  /**
   * Gets the RabbitMQ password.
   *
   * @return the RabbitMQ password
   */
  public String getRabbitMQPassword() {
    return (String) data.get("rabbitmq.password");
  }

  /**
   * Gets the RabbitMQ profile.
   *
   * @return the RabbitMQ profile
   */
  public String getRabbitMQProfile() {
    return (String) data.get("rabbitmq.profile");
  }

  /**
   * Gets the RethinkDB host.
   *
   * @return the RethinkDB host
   */
  public String getRethinkDBHost() {
    return (String) data.get("rethinkdb.host");
  }

  /**
   * Gets the RethinkDB port.
   *
   * @return the RethinkDB port
   */
  public int getRethinkDBPort() {
    return (int) data.get("rethinkdb.port");
  }

  /**
   * Gets the RethinkDB database.
   *
   * @return the RethinkDB database
   */
  public String getRethinkDBDatabase() {
    return (String) data.get("rethinkdb.database");
  }

  /**
   * Gets the RethinkDB username.
   *
   * @return the RethinkDB username
   */
  public String getRethinkDBUsername() {
    return (String) data.get("rethinkdb.username");
  }

  /**
   * Gets the RethinkDB password.
   *
   * @return the RethinkDB password
   */
  public String getRethinkDBPassword() {
    return (String) data.get("rethinkdb.password");
  }

  /**
   * Gets the RethinkDB profile.
   *
   * @return the RethinkDB profile
   */
  public String getRethinkDBProfile() {
    return (String) data.get("rethinkdb.profile");
  }
}
