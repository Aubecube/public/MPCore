/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.data.AbstractDataManager;
import fr.minepod.mpcore.api.data.DataPolicy;

// TODO: Auto-generated Javadoc
/**
 * The Class LegacyPlayerDataManager.
 */
public class LegacyPlayerDataManager extends AbstractDataManager<String, PlayerClass> {

  /**
   * Instantiates a new player uuid data manager.
   *
   * @param plugin the plugin
   * @param file the file
   */
  public LegacyPlayerDataManager(JavaPlugin plugin, String file) {
    super(plugin, file, DataPolicy.REGULAR);
  }

  /**
   * Gets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#get()
   */
  @Override
  public void get() {
    Map<String, PlayerClass> map = new HashMap<>();

    if (getConfig().isSet("data")) {
      for (String entry : getConfig().getConfigurationSection("data").getKeys(false)) {
        UUID uuid = UUID.fromString(getConfig().getString("data." + entry + ".uuid"));
        long seen = getConfig().getLong("data." + entry + ".seen", System.currentTimeMillis());
        long playTime = getConfig().getLong("data." + entry + ".playtime", 0L);

        Map<String, Long> names = new HashMap<>();
        names.put(entry, seen);

        Map<String, Long> times = new HashMap<>();
        times.put(MPCore.getConfigData().getRethinkDBProfile(), playTime);

        map.put(entry, new PlayerClass(uuid, entry, names, seen, seen, times));
      }
    }

    setData(map);
  }

  /**
   * Sets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#set()
   */
  @Override
  public void set() {}
}
