/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.data;

import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.data.AbstractJSONDataManager;

// TODO: Auto-generated Javadoc
/**
 * The Class PlayerTimeDataManager.
 */
public class JSONTimeDataManager extends AbstractJSONDataManager<String, Long> {

  /**
   * Instantiates a new player time data manager.
   *
   * @param plugin the plugin
   * @param fileName the file name
   */
  public JSONTimeDataManager(JavaPlugin plugin, String fileName) {
    super(plugin, fileName);
  }

  /**
   * Gets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractJSONDataManager#get()
   */
  @SuppressWarnings("unchecked")
  @Override
  public void get() {
    setData((Map<String, Long>) getJSONConfig());
  }

  /**
   * Sets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractJSONDataManager#set()
   */
  @Override
  public void set() {
    setJSONConfig(getData());
  }
}
