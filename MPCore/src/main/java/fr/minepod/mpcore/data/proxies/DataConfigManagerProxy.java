/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */

package fr.minepod.mpcore.data.proxies;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.bukkit.inventory.ItemStack;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import fr.minepod.mpcore.api.data.configs.AbstractConfigDataManager;
import fr.minepod.mpcore.api.data.configs.AbstractConfigJSONDataManager;
import fr.minepod.mpcore.api.data.configs.ConfigDataLoader;
import fr.minepod.mpcore.api.data.configs.ConfigDataLoaderType;
import fr.minepod.mpcore.api.data.configs.DynamicConfigDataLoader;

// TODO: Auto-generated Javadoc
/**
 * The Class DataManagerProxy.
 */
public class DataConfigManagerProxy {

  /**
   * Adds the proxy.
   *
   * @param <T> the generic type
   * @param target the target
   * @param clazz the clazz
   * @return the object
   * @throws Exception the exception
   */
  public static <T> T addProxy(T target, Class<T> clazz) throws Exception {
    AbstractConfigDataManager.managers.remove(target);

    ProxyFactory factory = new ProxyFactory(target);
    factory
        .addAdvisor(new DefaultPointcutAdvisor(new DataManagerPointcut(), new DataManagerAdvice()));
    factory.setTarget(target);
    factory.setProxyTargetClass(true);
    factory.setOptimize(true);

    @SuppressWarnings("unchecked")
    T proxied = (T) factory.getProxy(target.getClass().getClassLoader());

    List<String> methods = new ArrayList<>();
    for (Method method : target.getClass().getMethods()) {
      if (new DataManagerPointcut().matches(method, target.getClass())) {
        methods.add(method.getName());
      }
    }

    AbstractConfigDataManager<?, ?, ?> casted = (AbstractConfigDataManager<?, ?, ?>) proxied;
    casted.setProxied(true);
    casted.setProxiedMethods(methods);

    AbstractConfigDataManager.managers.add(casted);

    return proxied;
  }
}


class DataManagerAdvice implements MethodInterceptor {
  static Map<String, DynamicConfigDataLoader> loaders = new HashMap<>();

  public Object invoke(MethodInvocation invocation) throws Throwable {
    ConfigDataLoader annotation = null;
    if (invocation.getMethod().isAnnotationPresent(ConfigDataLoader.class)) {
      annotation = invocation.getMethod().getAnnotation(ConfigDataLoader.class);
    }

    String name = invocation.getClass().getName() + "#" + invocation.getMethod().getName();

    DynamicConfigDataLoader loader = loaders.get(name);
    if (loader == null) {
      if (annotation != null) {
        loaders.put(name, loader = new DynamicConfigDataLoader(annotation));
      } else {
        loaders.put(name, loader =
            new DynamicConfigDataLoader(false, "", ConfigDataLoaderType.AUTODETECT, false));
      }
    }

    if (!loader.transform()) {
      if (loader.path().isEmpty()) {
        loader.setPath(getPath(invocation.getMethod().getName()));
      }

      if (loader.type() == ConfigDataLoaderType.AUTODETECT) {
        Class<?> clazz = invocation.getMethod().getReturnType();

        ConfigDataLoaderType type;
        if (boolean.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.BOOLEAN;
        } else if (double.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.DOUBLE;
        } else if (float.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.FLOAT;
        } else if (int.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.INT;
        } else if (ItemStack.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.ITEMSTACK;
        } else if (List.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.LIST;
        } else if (long.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.LONG;
        } else if (String.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.STRING;
        } else if (UUID.class.isAssignableFrom(clazz)) {
          type = ConfigDataLoaderType.UUID;
        } else {
          // Default as string
          type = ConfigDataLoaderType.STRING;
        }

        loader.setType(type);
      }

      loader.setTransform(true);
    }

    if (loader.special()) {
      return invocation.proceed();
    }

    if (invocation.getThis() instanceof AbstractConfigJSONDataManager) {
      @SuppressWarnings("unchecked")
      AbstractConfigJSONDataManager<?, String, Object> data =
          (AbstractConfigJSONDataManager<?, String, Object>) invocation.getThis();

      return data.get(loader.path());
    } else {
      @SuppressWarnings("unchecked")
      AbstractConfigDataManager<?, String, Object> data =
          (AbstractConfigDataManager<?, String, Object>) invocation.getThis();

      Object object;
      if (data.getConfig().contains(loader.path())) {
        switch (loader.type()) {
          case BOOLEAN:
            object = data.getConfig().getBoolean(loader.path());
            break;
          case DOUBLE:
            object = data.getConfig().getDouble(loader.path());
            break;
          case FLOAT:
            object = (float) data.getConfig().getDouble(loader.path());
            break;
          case INT:
            object = data.getConfig().getInt(loader.path());
            break;
          case ITEMSTACK:
            object = data.getConfig().getItemStack(loader.path());
            break;
          case LIST:
            object = data.getConfig().getStringList(loader.path());
            break;
          case LONG:
            object = data.getConfig().getLong(loader.path());
            break;
          case STRING:
            object = data.getConfig().getString(loader.path());
            break;
          case UUID:
            object = UUID.fromString(data.getConfig().getString(loader.path()));
            break;
          default:
            object = null;
        }
      } else {
        object = invocation.proceed();
      }

      data.put(loader.path(), object);

      return object;
    }
  }

  private String getPath(String name) {
    if (name.startsWith("$")) {
      name = name.substring(1);
    }

    String[] parts = name.split("(?=[A-Z])");

    boolean first = true;
    StringBuilder builder = new StringBuilder();
    for (String part : parts) {
      if (first) {
        if (!part.equalsIgnoreCase("get") && !part.equalsIgnoreCase("is")) {
          first = false;
        }
      } else {
        builder.append("-");
      }

      builder.append(part);
    }

    return builder.toString().toLowerCase().replace("_", ".");
  }
}


class DataManagerPointcut extends StaticMethodMatcherPointcut {
  public ClassFilter getClassFilter() {
    return new ClassFilter() {
      public boolean matches(Class<?> clazz) {
        return true;
      }
    };
  }

  @Override
  public boolean matches(Method method, Class<?> clazz) {
    boolean result = method.getParameterTypes().length == 0
        && (method.isAnnotationPresent(ConfigDataLoader.class) || method.getName().startsWith("$"));

    return result;
  }
}
