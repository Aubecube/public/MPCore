/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.api.data.AbstractDataManager;
import fr.minepod.mpcore.api.data.DataPolicy;

// TODO: Auto-generated Javadoc
/**
 * The Class KeyStoreManager.
 */
public class KeyStoreManager extends AbstractDataManager<String, Object> {

  /**
   * Instantiates a new key store manager.
   *
   * @param plugin the plugin
   * @param file the file name
   */
  public KeyStoreManager(JavaPlugin plugin, String file) {
    super(plugin, file, DataPolicy.REGULAR);
  }

  /**
   * Gets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#get()
   */
  @Override
  public void get() {
    Map<String, Object> map = new HashMap<>();

    if (getConfig().isSet("data")) {
      for (String entry : getConfig().getConfigurationSection("data").getKeys(false)) {
        int index = entry.lastIndexOf("-");
        String type = entry.substring(index + 1);

        Object object;
        switch (type) {
          case "int":
            object = getConfig().getInt("data." + entry);
            break;
          case "float":
            object = (float) getConfig().getDouble("data." + entry);
            break;
          case "double":
            object = getConfig().getDouble("data." + entry);
            break;
          case "long":
            object = getConfig().getLong("data." + entry);
            break;
          case "uuid":
            object = UUID.fromString(getConfig().getString("data." + entry));
            break;
          case "boolean":
            object = getConfig().getBoolean("data." + entry);
            break;
          case "string":
            object = getConfig().getString("data." + entry);
            break;
          default:
            throw new IllegalArgumentException("Wrong type parameter.");
        }

        map.put(entry.substring(0, index), object);
      }
    }

    setData(map);
  }

  /**
   * Sets the.
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#set()
   */
  @Override
  public void set() {
    getConfig().set("data", null);

    for (Entry<String, Object> entry : getData().entrySet()) {
      String type;
      Object object;
      if (entry.getValue() instanceof Integer) {
        type = "int";
        object = entry.getValue();
      } else if (entry.getValue() instanceof Float) {
        type = "float";
        object = entry.getValue();
      } else if (entry.getValue() instanceof Double) {
        type = "double";
        object = entry.getValue();
      } else if (entry.getValue() instanceof Long) {
        type = "long";
        object = entry.getValue();
      } else if (entry.getValue() instanceof UUID) {
        type = "uuid";
        object = entry.getValue().toString();
      } else if (entry.getValue() instanceof Boolean) {
        type = "boolean";
        object = entry.getValue();
      } else {
        type = "string";
        object = entry.getValue();
      }

      getConfig().set("data." + entry.getKey() + "-" + type, object);
    }
  }

  /**
   * Gets the value.
   *
   * @param key the key
   * @param defaultValue the default value
   * @return the value
   */
  public Object getValue(String key, Object defaultValue) {
    return getValue(key, defaultValue, true);
  }

  /**
   * Gets the value.
   *
   * @param key the key
   * @param defaultValue the default value
   * @param add the add
   * @return the value
   */
  public Object getValue(String key, Object defaultValue, boolean add) {
    if (containsValue(key)) {
      return getFor(key);
    } else {
      if (add) {
        setFor(key, defaultValue);
      }

      return defaultValue;
    }
  }

  /**
   * Contains key.
   *
   * @param key the key
   * @return true, if successful
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#containsKey(java.lang. Object)
   */
  @Override
  public boolean containsKey(String key) {
    return super.containsKey(key.replace(".", "-"));
  }

  /**
   * Sets the for.
   *
   * @param key the key
   * @param value the value
   * @return the object
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#setFor(java.lang.Object, java.lang.Object)
   */
  @Override
  public Object setFor(String key, Object value) {
    return super.setFor(key.replace(".", "-"), value);
  }

  /**
   * Gets the for.
   *
   * @param key the key
   * @return the for
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#getFor(java.lang.Object)
   */
  @Override
  public Object getFor(String key) {
    return super.getFor(key.replace(".", "-"));
  }

  /**
   * Removes the for.
   *
   * @param key the key
   * @return the object
   */
  /*
   * (non-Javadoc)
   * 
   * @see fr.minepod.mpcore.api.data.AbstractDataManager#removeFor(java.lang. Object)
   */
  @Override
  public Object removeFor(String key) {
    return super.removeFor(key.replace(".", "-"));
  }
}
