/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.betonquest;

import fr.minepod.mpcore.api.permissions.PermissionsManager;
import pl.betoncraft.betonquest.Instruction;
import pl.betoncraft.betonquest.InstructionParseException;
import pl.betoncraft.betonquest.QuestRuntimeException;
import pl.betoncraft.betonquest.api.Condition;
import pl.betoncraft.betonquest.utils.PlayerConverter;

// TODO: Auto-generated Javadoc
/**
 * The Class DynamicPermissionsCondition.
 */
public class DynamicPermissionsCondition extends Condition {

  /**
   * Instantiates a new dynamic permissions condition.
   *
   * @param instruction the instruction
   * @throws InstructionParseException the instruction parse exception
   */
  public DynamicPermissionsCondition(Instruction instruction) throws InstructionParseException {
    super(instruction);
  }

  /**
   * Check.
   *
   * @param player the player
   * @return true, if successful
   * @throws QuestRuntimeException the quest runtime exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see pl.betoncraft.betonquest.api.Condition#check(java.lang.String)
   */
  @Override
  public boolean check(String player) throws QuestRuntimeException {
    return PermissionsManager.hasSender(PlayerConverter.getPlayer(player),
        instruction.getInstruction());
  }
}
