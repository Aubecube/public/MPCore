/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.permissions;

import java.util.Map.Entry;

import org.bukkit.entity.Player;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.functions.FunctionsManager;
import fr.minepod.mpcore.api.permissions.DynamicPermissions;
import fr.minepod.mpcore.api.uuids.UUIDsManager;
import lombok.RequiredArgsConstructor;

// TODO: Auto-generated Javadoc
/**
 * Instantiates a new dynamic permissions listener.
 *
 * @see DynamicPermissionsEvent
 */

/**
 * Instantiates a new dynamic permissions listener.
 */

/**
 * Instantiates a new dynamic permissions listener.
 */

/**
 * Instantiates a new dynamic permissions listener.
 */
@RequiredArgsConstructor
public class DynamicPermissionsListener implements DynamicPermissions {

  /**
   * Checks for permission.
   *
   * @param player the player
   * @param permission the permission
   * @return true, if successful
   */
  /*
   * (non-Javadoc)
   * 
   * @see
   * fr.minepod.mpcore.api.permissions.DynamicPermissions#hasPermission(org.bukkit.entity.Player,
   * java.lang.String)
   */
  @Override
  public boolean hasPermission(Player player, String permission) {
    String[] parts = permission.split(":");

    PlayerClass data = UUIDsManager.getPlayerClass(player.getUniqueId());

    // type:operator:value
    switch (parts[0]) {
      case "playtime-local":
        if (FunctionsManager.isLong(parts[2])) {
          Long playtime = data.getPlayTimes().get(MPCore.getConfigData().getRethinkDBProfile());

          if (playtime != null) {
            return compute(parts[1], Long.parseLong(parts[2]), playtime);
          }
        }

        break;
      case "playtime-global":
        if (FunctionsManager.isLong(parts[2])) {
          long playtime = 0L;
          for (Entry<String, Long> entry : data.getPlayTimes().entrySet()) {
            playtime += entry.getValue();
          }

          return compute(parts[1], Long.parseLong(parts[2]), playtime);
        }

        break;
    }

    return false;
  }

  /**
   * Compute.
   *
   * @param operator the operator
   * @param value the value
   * @param playtime the playtime
   * @return true, if successful
   */
  private boolean compute(String operator, Long value, Long playtime) {
    switch (operator) {
      case "gt":
        return value > playtime;
      default:
      case "eq":
        return value == playtime;
      case "lw":
        return value < playtime;
    }
  }
}
