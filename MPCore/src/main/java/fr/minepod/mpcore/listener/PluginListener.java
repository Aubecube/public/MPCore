/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.listener;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import fr.minepod.mpcore.APIVersion;
import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.MPCoreDepends;
import fr.minepod.mpcore.api.data.AutoDataManager;
import fr.minepod.mpcore.api.integrations.DynamicDependency;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving player events. The class that is interested in processing a
 * player event implements this interface, and the object created with that class is registered with
 * a component using the component's <code>addPlayerListener<code> method. When the player event
 * occurs, that object's appropriate method is invoked.
 * 
 * @see PlayerEvent
 */
public class PluginListener implements Listener {

  /** The plugin. */
  MPCore plugin;

  /** The plugins. */
  Map<Plugin, APIVersion> plugins = new HashMap<>();

  /**
   * Instantiates a new plugin listener.
   * 
   * @param plugin the plugin
   */
  public PluginListener(MPCore plugin) {
    this.plugin = plugin;
  }

  /**
   * On plugin enable.
   * 
   * @param event the event
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onPluginEnable(PluginEnableEvent event) {
    Plugin enabled = event.getPlugin();

    if (enabled != plugin) {
      for (Class<?> clazz : enabled.getClass().getInterfaces()) {
        if (clazz.equals(MPCoreDepends.class)) {
          try {
            Method method = clazz.getMethod("getAPIVersion");
            method.setAccessible(true);

            APIVersion apiVersion = (APIVersion) method.invoke(enabled);
            plugins.put(enabled, apiVersion);

            if (apiVersion != plugin.getAPIVersion()) {
              plugin.getLogger()
                  .severe("Found a plugin implementing an old version of the MPCore API!");
              plugin.getLogger().severe("-- Informations --");
              plugin.getLogger().severe(enabled.getDescription().getFullName());
              plugin.getLogger()
                  .severe("Current MPCore API version: " + plugin.getAPIVersion().getInternal());
              plugin.getLogger()
                  .severe("Implemented MPCore API version: " + apiVersion.getInternal());
              plugin.getLogger().severe("-- --");
            }
          } catch (NoSuchMethodException | SecurityException | IllegalAccessException
              | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
          }
        }
      }

      AutoDataManager.onEnable((JavaPlugin) enabled);
    }

    for (DynamicDependency dependency : DynamicDependency.dependencies) {
      if (!dependency.isAvailable(enabled.getName()).isPresent()) {
        dependency.run(enabled.getName());
      }
    }
  }

  /**
   * On plugin disable.
   * 
   * @param event the event
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onPluginDisable(PluginDisableEvent event) {
    Plugin disabled = event.getPlugin();

    if (disabled != plugin) {
      AutoDataManager.onDisable((JavaPlugin) disabled);
    }

    for (DynamicDependency dependency : DynamicDependency.dependencies) {
      Optional<Boolean> available = dependency.isAvailable(disabled.getName());
      if (available.isPresent() && available.get()) {
        dependency.setAvailable(disabled.getName(), false);
      }
    }
  }

  /**
   * Gets the plugins.
   *
   * @return the plugins
   */
  public Map<Plugin, APIVersion> getPlugins() {
    return plugins;
  }
}
