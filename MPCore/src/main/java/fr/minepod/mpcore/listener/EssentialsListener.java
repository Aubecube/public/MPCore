/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEvent;

import fr.minepod.mpcore.MPCore;
import net.ess3.api.events.AfkStatusChangeEvent;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving player events. The class that is interested in processing a
 * player event implements this interface, and the object created with that class is registered with
 * a component using the component's <code>addPlayerListener<code> method. When the player event
 * occurs, that object's appropriate method is invoked.
 * 
 * @see PlayerEvent
 */
public class EssentialsListener implements Listener {
  /**
   * On afk status change.
   *
   * @param event the event
   */
  @EventHandler
  public void onAfkStatusChange(AfkStatusChangeEvent event) {
    if (!event.isCancelled() && !MPCore.getConfigData().isTrackPlayerAfkTimeEnabled()) {
      MPCore.get().playerListener.updatePlayTime(event.getAffected().getBase(), event.getValue());
    }
  }
}
