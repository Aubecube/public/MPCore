/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.listener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.rethinkdb.RethinkDB;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.rethinkdb.RethinkDBRegistry;
import fr.minepod.mpcore.api.uuids.UUIDsManager;
import fr.minepod.mpcore.data.JSONTimeDataManager;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving player events. The class that is interested in processing a
 * player event implements this interface, and the object created with that class is registered with
 * a component using the component's <code>addPlayerListener<code> method. When the player event
 * occurs, that object's appropriate method is invoked.
 * 
 * @see PlayerEvent
 */
public class PlayerListener implements Listener {

  /** The plugin. */
  MPCore plugin;

  /** The players connection time. */
  public Map<Player, Long> playersConnectionTime = new HashMap<>();

  /** The calendar. */
  public Calendar calendar;

  /** The player time data manager. */
  public JSONTimeDataManager JSONTimeDataManager;

  /** The log. */
  public boolean log;

  /**
   * Instantiates a new player listener.
   * 
   * @param plugin the plugin
   */
  public PlayerListener(MPCore plugin) {
    this.plugin = plugin;

    log = MPCore.getConfigData().isTrackPlayerTimeEnabled();
  }

  /**
   * On player join.
   * 
   * @param event the event
   */
  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlayerLogin(PlayerLoginEvent event) {
    if (event.getResult().equals(Result.ALLOWED)) {
      UUID uuid = event.getPlayer().getUniqueId();
      String name = event.getPlayer().getName().toLowerCase();
      long now = System.currentTimeMillis();

      if (!RethinkDBRegistry.isEnabled()) {
        plugin.getLogger().severe("RethinkDB is not enabled!");
        event.disallow(Result.KICK_OTHER, "Erreur temporaire, merci de signaler");

        return;
      }

      // If another player has the same username, assume the other one has changed his -> set the
      // other one's to something impossible like "@changed"
      plugin.players.getTable().getAll(name).optArg("index", "name")
          .filter(row -> RethinkDB.r.not(row.g("id").eq(uuid.toString())))
          .update(RethinkDB.r.hashMap("name", "@changed")).run(RethinkDBRegistry.getConnection());

      PlayerClass value = UUIDsManager.getPlayerClass(uuid);
      if (value == null) {
        HashMap<String, Long> names = new HashMap<>();
        names.put(name, now);

        HashMap<String, Long> times = new HashMap<>();
        times.put(MPCore.getConfigData().getRethinkDBProfile(), 0L);

        value = new PlayerClass(uuid, name, names, now, now, times);
      } else {
        value.setName(name);
        value.getOldNames().put(name, now);
        value.setLastSeen(now);
      }

      plugin.players.replace(event.getPlayer().getUniqueId(), value);

      updatePlayTime(event.getPlayer(), false);
    }
  }

  /**
   * On player quit.
   * 
   * @param event the event
   */
  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event) {
    plugin.getLogger().info("Setting data for player: " + event.getPlayer().getUniqueId() + " ("
        + event.getPlayer().getName() + ")");

    updatePlayTime(event.getPlayer(), true);

    // TODO: Unload player
  }

  /**
   * Update play time.
   *
   * @param player the player
   * @param quit if the player has disconnected (true) or connected (false)
   */
  public void updatePlayTime(Player player, boolean quit) {
    if (quit && playersConnectionTime.containsKey(player)) {
      long delta = System.currentTimeMillis() - playersConnectionTime.get(player);

      // TODO: Wtf?!
      addTime(player, playersConnectionTime.remove(player));

      if (!RethinkDBRegistry.isEnabled()) {
        plugin.getLogger().severe("RethinkDB is not enabled!");
      }

      PlayerClass value = UUIDsManager.getPlayerClass(player.getUniqueId());
      value.setLastSeen(System.currentTimeMillis());

      Long playTime = value.getPlayTimes().get(MPCore.getConfigData().getRethinkDBProfile());
      if (playTime == null) {
        playTime = delta;
      } else {
        playTime += delta;
      }

      value.getPlayTimes().put(MPCore.getConfigData().getRethinkDBProfile(), playTime);

      plugin.players.replace(player.getUniqueId(), value);
    } else if (!quit) {
      playersConnectionTime.put(player, System.currentTimeMillis());
    }
  }

  /**
   * Adds the time.
   *
   * @param player the player
   * @param time the time
   */
  public void addTime(Player player, long time) {
    if (log) {
      // TODO: Wtf?!
      try {
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
        SimpleDateFormat file = new SimpleDateFormat("yyyy-MM-dd");

        if (calendar == null || file.format(calendar.getTime()) != file.format(now.getTime())) {
          if (calendar != null && file.format(calendar.getTime()) != file.format(now.getTime())) {
            long lastMidnight =
                System.currentTimeMillis() - (now.get(Calendar.HOUR_OF_DAY) * 3600 * 1000
                    + now.get(Calendar.MINUTE) * 60 * 1000 + now.get(Calendar.SECOND) * 1000);

            if (JSONTimeDataManager != null) {
              long yesterdayTime = lastMidnight - time; // Yesterday's
                                                        // time

              String id = player.getUniqueId() + "|" + player.getName();
              if (JSONTimeDataManager.containsKey(id)) {
                yesterdayTime += JSONTimeDataManager.getFor(id);
              }

              JSONTimeDataManager.setFor(id, yesterdayTime);
            }

            time = System.currentTimeMillis() - lastMidnight; // Today's
                                                              // time
          }

          calendar = now;

          File folder = new File(plugin.getDataFolder(), "logs" + File.separator);
          if (!folder.exists()) {
            folder.mkdir();
          }

          for (File temp : folder.listFiles()) {
            if (temp.isFile()) {
              if (temp.lastModified() < now.getTimeInMillis()
                  - 1000 * 60 * 60 * 24 * MPCore.getConfigData().getLogsRetentionDaysPlayTime()) {
                temp.delete();
              }
            }
          }

          if (JSONTimeDataManager != null) {
            JSONTimeDataManager.save();
          }

          JSONTimeDataManager = new JSONTimeDataManager((JavaPlugin) plugin,
              "logs" + File.separator + file.format(calendar.getTime()) + ".txt");
          JSONTimeDataManager.load();
        }

        String id = player.getUniqueId() + "|" + player.getName();
        if (JSONTimeDataManager.containsKey(id)) {
          time += JSONTimeDataManager.getFor(id);
        }

        JSONTimeDataManager.setFor(id, time);
      } catch (Exception e) {
        plugin.getLogger().severe(
            "Exception when trying to log. The plugin will disable login to prevent errors!");
        e.printStackTrace();
        log = false;
      }
    }
  }
}
