/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.commands;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Stopwatch;
import com.rethinkdb.gen.exc.ReqlDriverError;
import com.rethinkdb.model.MapObject;

import fr.minepod.mpcore.APIVersion;
import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.classes.PlayerClass;
import fr.minepod.mpcore.api.commands.CommandBase;
import fr.minepod.mpcore.api.commands.CommandRegistry;
import fr.minepod.mpcore.api.commands.CommandType;
import fr.minepod.mpcore.api.functions.FunctionsManager;
import fr.minepod.mpcore.api.logger.LogManager;
import fr.minepod.mpcore.api.nms.TitlePacketManipulator;
import fr.minepod.mpcore.api.nms.internal.EnumTitleActionEnumerator;
import fr.minepod.mpcore.api.nms.internal.PlayerConnectionWrapper;
import fr.minepod.mpcore.api.rabbitmq.RabbitMQRegistry;
import fr.minepod.mpcore.api.rethinkdb.RethinkDBRegistry;
import fr.minepod.mpcore.api.uuids.UUIDsManager;

// TODO: Auto-generated Javadoc
/**
 * The Class MPCoreCommands.
 */
public class MPCoreCommands {

  /** The commands. */
  private CommandRegistry<MPCore> commands = new CommandRegistry<>(MPCore.get(), "mpcore");

  /**
   * Instantiates a new MP core commands.
   */
  public MPCoreCommands() {
    commands.put("version", new CommandBase("Get the plugin version", "version", CommandType.USER,
        (sender, command, label, args, processed) -> {
          MPCore.getMessages().sendFormatted(sender,
              "Version " + MPCore.get().getDescription().getVersion());
        }));

    commands.put("plugins", new CommandBase("Get the plugins implementing the API", "plugins",
        CommandType.USER, (sender, command, label, args, processed) -> {
          MPCore.getMessages().sendFormatted(sender,
              "Current API version: " + ChatColor.BLUE + MPCore.get().getAPIVersion().toString()
                  + ChatColor.GOLD + " (" + ChatColor.GREEN
                  + MPCore.get().getAPIVersion().getInternal() + ChatColor.GOLD + ")");
          MPCore.getMessages().sendFormatted(sender,
              ChatColor.GREEN + "Found " + ChatColor.GOLD
                  + MPCore.get().pluginListener.getPlugins().size() + ChatColor.GREEN
                  + " plugins implementing the API.");
          MPCore.getMessages().sendFormatted(sender, "-- Plugins --");

          for (Entry<Plugin, APIVersion> entry : MPCore.get().pluginListener.getPlugins()
              .entrySet()) {
            String color;
            if (entry.getValue() == MPCore.get().getAPIVersion()) {
              color = ChatColor.GREEN.toString();
            } else {
              color = ChatColor.RED.toString();
            }

            MPCore.getMessages().sendFormatted(sender,
                ChatColor.GREEN + entry.getKey().getDescription().getFullName() + ChatColor.GOLD
                    + ": " + ChatColor.BLUE + entry.getValue().toString() + ChatColor.GOLD + " ("
                    + color + entry.getValue().getInternal() + ChatColor.GOLD + ")");
          }
        }));

    commands.put("save", new CommandBase("Save data", "save", CommandType.ADMINISTRATOR,
        (sender, command, label, args, processed) -> {
          try {
            MPCore.getKeyStore().save();
            MPCore.getMessages().sendFormatted(sender, ChatColor.GREEN + "Key store data saved.");
          } catch (IOException e) {
            e.printStackTrace();
            MPCore.getMessages().sendFormatted(sender,
                ChatColor.RED + "Failed to save key store data.");
          }
        }));

    commands.put("autosave", new CommandBase("Trigger an autosave", "autosave",
        CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          MPCore.get().autosave();
          MPCore.getMessages().sendFormatted(sender, ChatColor.GOLD + "Autosave triggered.");
        }));

    commands.put("reload", new CommandBase("Reload the config, reconnect to RabbitMQ and RethinkDB",
        "reload", CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          MPCore.getConfigData().load();

          RabbitMQRegistry.close();
          RethinkDBRegistry.close();

          MPCore.get().cancelAutosaveTask();
          MPCore.get().startAutosaveTask();

          if (MPCore.getConfigData().isRabbitMQEnabled()) {
            RabbitMQRegistry.init();

            try {
              RabbitMQRegistry.reconnect();

              MPCore.getMessages().sendFormatted(sender,
                  ChatColor.GREEN + "Reconnected to RabbitMQ.");
            } catch (IOException | TimeoutException e) {
              e.printStackTrace();
            }
          } else {
            MPCore.getMessages().sendFormatted(sender, ChatColor.GOLD + "RabbitMQ is not enabled.");
          }

          RethinkDBRegistry.init();

          try {
            RethinkDBRegistry.reconnect();

            MPCore.getMessages().sendFormatted(sender,
                ChatColor.GREEN + "Reconnected to RethinkDB.");
          } catch (ReqlDriverError e) {
            e.printStackTrace();
          }

          MPCore.getMessages().sendFormatted(sender, ChatColor.GREEN + "Config reloaded.");
        }));

    commands.put("reloadkeys", new CommandBase("Reload the key store", "reloadkeys",
        CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          MPCore.getKeyStore().load();

          MPCore.getMessages().sendFormatted(sender, ChatColor.GREEN + "Config reloaded.");
        }));

    // TODO: Remove this later on
    commands.put("convert", new CommandBase("Convert the YAML data for the new RethinkDB backend",
        "convert", CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
          MPCore.getLegacyData().load();

          if (!RethinkDBRegistry.isEnabled()) {
            MPCore.getMessages().sendFormatted(sender, ChatColor.RED + "RethinkDB is not enabled.");
            return;
          }

          if (MPCore.getLegacyData().getData().size() > 0) {
            MapObject[] array = new MapObject[MPCore.getLegacyData().getData().size()];

            int i = 0;
            for (Entry<String, PlayerClass> entry : MPCore.getLegacyData().getData().entrySet()) {
              array[i] = entry.getValue().asRethinkHashMap();

              i++;
            }

            MPCore.get().players.getTable().insert(array).run(RethinkDBRegistry.getConnection());

            long count =
                MPCore.get().players.getTable().count().run(RethinkDBRegistry.getConnection());

            MPCore.getMessages().sendFormatted(sender,
                ChatColor.GREEN + "Data should be converted. " + ChatColor.YELLOW
                    + "Legacy count = " + MPCore.getLegacyData().getData().size() + ChatColor.GREEN
                    + ", " + ChatColor.YELLOW + "new count = " + count + ChatColor.GREEN + ".");
          } else {
            MPCore.getMessages().sendFormatted(sender, ChatColor.RED + "No data to convert.");
          }
        }));

    commands.put("getprofile",
        new CommandBase("Get the stored profile of [player]", "getprofile [string=player]",
            CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
              Stopwatch watch = Stopwatch.createStarted();
              PlayerClass player =
                  UUIDsManager.getPlayerClass(processed.get(1).getStringValue(), true);
              watch.stop();

              MPCore.getMessages().sendFormatted(sender, ChatColor.DARK_AQUA + "Elapsed time: "
                  + ChatColor.YELLOW + watch.elapsed(TimeUnit.MILLISECONDS) + "ms");

              if (player != null) {
                MPCore.getMessages().sendFormatted(sender, ChatColor.GREEN
                    + "The following profile was found: " + ChatColor.YELLOW + player.toString());
              } else {
                MPCore.getMessages().sendFormatted(sender, ChatColor.RED + "Player not found.");
              }
            }));

    commands.put("setkey",
        new CommandBase("Set the value of the key [key] to [value]",
            "setkey [string=key] [string=value]", CommandType.ADMINISTRATOR,
            (sender, command, label, args, processed) -> {
              MPCore.getKeyStore().setFor(processed.get(1).getStringValue(),
                  FunctionsManager.objectFromString(processed.get(2).getStringValue()));

              MPCore.getMessages().sendFormatted(sender, ChatColor.GREEN + "The key was set to '"
                  + ChatColor.YELLOW + processed.get(2).getStringValue() + ChatColor.GREEN + "'.");
            }));

    commands.put("getkey",
        new CommandBase("Get the value of the key [key]", "getkey [string=key]",
            CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
              // Value can be null -> containsKey(...) and not getFor(...)
              // != null
              if (MPCore.getKeyStore().containsKey(processed.get(1).getStringValue())) {
                MPCore.getMessages().sendFormatted(sender,
                    ChatColor.GREEN + "The following value was found: " + ChatColor.YELLOW
                        + MPCore.getKeyStore().getFor(processed.get(1).getStringValue()).toString()
                        + ChatColor.GREEN + ".");
              } else {
                MPCore.getMessages().sendFormatted(sender,
                    ChatColor.RED + "No value was found for this key.");
              }
            }));

    commands.put("log",
        new CommandBase("Log [chain] to the file [file]", "log [string=file] [chain=chain]",
            CommandType.ADMINISTRATOR, (sender, command, label, args, processed) -> {
              try {
                LogManager.logToFile(
                    new File(MPCore.get().getDataFolder(),
                        "logs_" + processed.get(1).getStringValue() + ".log"),
                    processed.get(2).getStringValue(), true);
              } catch (IOException e) {
                e.printStackTrace();
              }
            }));

    commands.put("title",
        new CommandBase("Send [title;subtitle] to the player [player]/all players",
            "title [string=player/all] [chain=title;subtitle]", CommandType.ADMINISTRATOR,
            (sender, command, label, args, processed) -> {
              List<Player> players = new ArrayList<>();
              if (processed.get(1).getStringValue().equalsIgnoreCase("all")) {
                players.addAll(Bukkit.getOnlinePlayers());
              } else {
                for (Player temp : Bukkit.getOnlinePlayers()) {
                  if (temp.getName().equalsIgnoreCase(processed.get(1).getStringValue())) {
                    players.add(temp);
                    break;
                  }
                }
              }

              String[] parts = processed.get(2).getStringValue().split(";");

              String title = "";
              String subtitle = "";
              if (parts.length == 1) {
                title = parts[0];
              } else if (parts.length > 1) {
                title = parts[0];
                subtitle = parts[1];
              }

              try {
                List<Object> packets = new ArrayList<>();
                packets.add(TitlePacketManipulator.getPacket(EnumTitleActionEnumerator.TITLE,
                    "{\"color\": \"dark_aqua\", \"text\": \"" + title + "\"}"));

                if (subtitle != null) {
                  packets.add(TitlePacketManipulator.getPacket(EnumTitleActionEnumerator.SUBTITLE,
                      "{\"color\": \"gold\", \"text\": \"" + subtitle + "\"}"));
                }

                packets.add(TitlePacketManipulator.getPacket(EnumTitleActionEnumerator.TIMES, null,
                    20, 100, 20));

                for (Player p : players) {
                  for (Object packet : packets) {
                    PlayerConnectionWrapper.sendPacket(p,
                        TitlePacketManipulator.getPacketPlayOutTitleClass(), packet);
                  }
                }
              } catch (NoSuchMethodException | SecurityException | IllegalAccessException
                  | IllegalArgumentException | InvocationTargetException | NoSuchFieldException
                  | ClassNotFoundException | InstantiationException e) {
                e.printStackTrace();
              }
            }));

    commands.put("say",
        new CommandBase("Send the message [message] to the player [player]/all players",
            "say [string=player/all] [chain=message]", CommandType.ADMINISTRATOR,
            (sender, command, label, args, processed) -> {
              List<Player> players = new ArrayList<>();
              if (processed.get(1).getStringValue().equalsIgnoreCase("all")) {
                players.addAll(Bukkit.getOnlinePlayers());
              } else {
                for (Player temp : Bukkit.getOnlinePlayers()) {
                  if (temp.getName().equalsIgnoreCase(processed.get(1).getStringValue())) {
                    players.add(temp);
                    break;
                  }
                }
              }

              for (Player player : players) {
                player.sendMessage(processed.get(2).getStringValue());
              }
            }));

    MPCore.get().getCommand("mpcore").setExecutor(commands);
  }
}
