/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.rethinkdb.gen.exc.ReqlDriverError;

import fr.minepod.mpcore.api.data.AbstractDataManager;
import fr.minepod.mpcore.api.data.AutoDataManager;
import fr.minepod.mpcore.api.data.configs.AbstractConfigDataManager;
import fr.minepod.mpcore.api.integrations.DynamicDependency;
import fr.minepod.mpcore.api.locations.SerializableLocation;
import fr.minepod.mpcore.api.messages.MessagesManager;
import fr.minepod.mpcore.api.nms.internal.NMSBase;
import fr.minepod.mpcore.api.permissions.DynamicPermissionsRegistry;
import fr.minepod.mpcore.api.rabbitmq.RabbitMQRegistry;
import fr.minepod.mpcore.api.rethinkdb.RethinkDBRegistry;
import fr.minepod.mpcore.api.uuids.UUIDsManager;
import fr.minepod.mpcore.betonquest.DynamicPermissionsCondition;
import fr.minepod.mpcore.commands.MPCoreCommands;
import fr.minepod.mpcore.data.ConfigDataManager;
import fr.minepod.mpcore.data.KeyStoreManager;
import fr.minepod.mpcore.data.LegacyPlayerDataManager;
import fr.minepod.mpcore.data.RethinkPlayerDataManager;
import fr.minepod.mpcore.legacy.commands.proxies.ProxyKnownCommands;
import fr.minepod.mpcore.listener.EssentialsListener;
import fr.minepod.mpcore.listener.PlayerListener;
import fr.minepod.mpcore.listener.PluginListener;
import fr.minepod.mpcore.permissions.DynamicPermissionsListener;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import pl.betoncraft.betonquest.BetonQuest;

// TODO: Auto-generated Javadoc

/**
 * The Class MPCore.
 */
public class MPCore extends JavaPlugin implements CommandExecutor, MPCoreDepends {

  // TODO: Make private
  /**
   * The economy.
   */
  public static Economy economy = null;

  // TODO: Make private
  /**
   * The permission.
   */
  public static Permission permission = null;

  /**
   * The config data manager.
   */
  private ConfigDataManager configDataManager =
      new ConfigDataManager(this, "config.yml", false, true, true, false);

  /**
   * The key store data manager.
   */
  private KeyStoreManager keyStoreManager = new KeyStoreManager(this, "keystore.yml");

  /**
   * The messages manager.
   */
  private MessagesManager messages = new MessagesManager("MPCore");

  /**
   * The player UUID data manager.
   */
  private LegacyPlayerDataManager legacy = new LegacyPlayerDataManager(this, "player_uuid.yml");

  // TODO: Make private
  /** The players. */
  public RethinkPlayerDataManager players = new RethinkPlayerDataManager(this);

  /**
   * The uuids manager.
   *
   * @deprecated use the static methods of UUIDsManager instead
   */
  @Deprecated
  public UUIDsManager uuidsManager = new UUIDsManager(this);

  // TODO: Make private
  /** The plugin listener. */
  public PluginListener pluginListener;

  // TODO: Make private
  /**
   * The player listener.
   */
  public PlayerListener playerListener;

  /** The static instance. */
  private static MPCore instance;

  /** The task. */
  private Integer task;

  /**
   * Gets the api version.
   *
   * @return the api version
   */
  @Override
  public APIVersion getAPIVersion() {
    return APIVersion.ENGELBERG;
  }

  /**
   * On load.
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.plugin.java.JavaPlugin#onLoad()
   */
  @Override
  public void onLoad() {
    instance = this;
  }

  /**
   * On enable.
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
   */
  @Override
  public void onEnable() {
    getLogger().info("MPCore is loading and enabling...");

    getLogger().info("Registering data handlers...");
    ConfigurationSerialization.registerClass(SerializableLocation.class, "Location");
    getLogger().info("Data handlers registered!");

    getLogger().info("Loading data...");
    AutoDataManager.onEnable(this);
    getLogger().info("Data loaded!");

    integrate();

    new MPCoreCommands();

    getLogger().info("Registering events...");
    getServer().getPluginManager().registerEvents(pluginListener = new PluginListener(this), this);
    getServer().getPluginManager().registerEvents(playerListener = new PlayerListener(this), this);
    getLogger().info("Events registered!");

    if (configDataManager.isRabbitMQEnabled()) {
      RabbitMQRegistry.init();

      try {
        RabbitMQRegistry.connect();
      } catch (IOException | TimeoutException e) {
        e.printStackTrace();
      }
    }

    RethinkDBRegistry.init();

    try {
      RethinkDBRegistry.connect();
    } catch (ReqlDriverError e) {
      e.printStackTrace();
    }

    DynamicPermissionsRegistry.register("mpcore", this, new DynamicPermissionsListener());

    startAutosaveTask();

    if (NMSBase.getNMSVersion() == "v_1_12_R1") { // last supported version for proxies
      getLogger().info("Adding proxies for commands...");
      ProxyKnownCommands.register();
      getLogger().info("Proxies added!");
    }

    getLogger().info("MPCore loaded and enabled!");
  }

  /**
   * On disable.
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
   */
  @Override
  public void onDisable() {
    getLogger().info("MPCore is unloading and disabling...");

    for (Player player : getServer().getOnlinePlayers()) {
      playerListener.updatePlayTime(player, true);
    }

    cancelAutosaveTask();

    RabbitMQRegistry.close();

    AutoDataManager.onDisable(this);

    try {
      getLogger().info("Saving player time data...");

      if (playerListener.log && playerListener.JSONTimeDataManager != null) {
        playerListener.JSONTimeDataManager.save();
      }

      getLogger().info("Saved player time data.");
    } catch (IOException e) {
      e.printStackTrace();
      getLogger().severe("Failed to save player time data");
    }

    RethinkDBRegistry.close();

    getLogger().info("MPCore unloaded and disabled!");
  }

  /**
   * Static instance.
   *
   * @return the current instance of MPCore
   */
  public static MPCore get() {
    return instance;
  }

  /**
   * Gets the config data.
   *
   * @return the config data
   */
  public static ConfigDataManager getConfigData() {
    return instance.configDataManager;
  }

  /**
   * Gets the key store.
   *
   * @return the key store
   */
  public static KeyStoreManager getKeyStore() {
    return instance.keyStoreManager;
  }

  /**
   * Gets the messages.
   *
   * @return the messages
   */
  public static MessagesManager getMessages() {
    return instance.messages;
  }

  /**
   * Gets the legacy data.
   *
   * @return the legacy data
   */
  public static LegacyPlayerDataManager getLegacyData() {
    return instance.legacy;
  }

  /**
   * Gets the UUIDs manager.
   *
   * @return the UUIDs manager
   * @deprecated use the static methods from UUIDsManager instead
   */
  @Deprecated
  public UUIDsManager getUUIDsManager() {
    return uuidsManager;
  }

  // TODO: Make private
  /**
   * Autosave.
   */
  public void autosave() {
    for (AbstractDataManager<?, ?> manager : AbstractDataManager.managers) {
      if (manager.getAutosave()) {
        boolean pass = true;
        for (String plugin : configDataManager.getAutosaveBlacklist()) {
          if (plugin.equalsIgnoreCase(manager.getPlugin().getName())) {
            pass = false;
            break;
          }
        }

        if (pass) {
          try {
            manager.save();
            getLogger().info(
                "Autosaved: " + manager.getPlugin().getName() + "/" + manager.getFileName() + ".");
          } catch (IOException e) {
            e.printStackTrace();
            getLogger().severe("Failed to autosave: " + manager.getPlugin().getName() + "/"
                + manager.getFileName() + ".");
          }
        }
      }
    }

    for (AbstractConfigDataManager<?, ?, ?> manager : AbstractConfigDataManager.managers) {
      if (manager.getType().isAutosave()) {
        boolean pass = true;
        for (String plugin : configDataManager.getAutosaveBlacklist()) {
          if (plugin.equalsIgnoreCase(manager.getPlugin().getName())) {
            pass = false;
            break;
          }
        }

        if (pass) {
          try {
            manager.save();
            getLogger().info("Autosaved: " + manager.getPlugin().getName() + "//"
                + manager.getFile().getAbsolutePath() + ".");
          } catch (IOException e) {
            e.printStackTrace();
            getLogger().severe("Failed to autosave: " + manager.getPlugin().getName() + "//"
                + manager.getFile().getAbsolutePath() + ".");
          }
        }
      }
    }
  }

  /**
   * Integrate.
   */
  private void integrate() {
    DynamicDependency dependencies = new DynamicDependency();

    dependencies.put("Vault", () -> {
      RegisteredServiceProvider<Permission> permissionProvider =
          getServer().getServicesManager().getRegistration(Permission.class);

      if (permissionProvider != null) {
        permission = permissionProvider.getProvider();

        getLogger().info("Using Vault for permissions.");
      }

      RegisteredServiceProvider<Economy> economyProvider =
          getServer().getServicesManager().getRegistration(Economy.class);

      if (economyProvider != null) {
        economy = economyProvider.getProvider();

        getLogger().info("Using Vault for economy.");
      }
    });

    dependencies.put("Essentials", () -> {
      getServer().getPluginManager().registerEvents(new EssentialsListener(), MPCore.get());

      getLogger().info("Using Essentials for AFK detection.");
    });

    dependencies.put("BetonQuest", () -> {
      BetonQuest.getInstance().registerConditions("mpcore", DynamicPermissionsCondition.class);

      getLogger().info("Providing conditions for BetonQuest.");
    });

    dependencies.run();
  }

  // TODO: Make private
  /**
   * Start autosave task.
   */
  public void startAutosaveTask() {
    if (configDataManager.isAutosaveEnabled()) {
      task = getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
        autosave();
      }, configDataManager.getAutosaveDelay() * 20, configDataManager.getAutosaveDelay() * 20);

      getLogger().info("Autosave task started.");
    }
  }

  // TODO: Make private
  /**
   * Cancel autosave task.
   */
  public void cancelAutosaveTask() {
    if (task != null) {
      getServer().getScheduler().cancelTask(task);
    }
  }
}
