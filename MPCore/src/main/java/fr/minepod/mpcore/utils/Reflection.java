/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class Reflection.
 */
public class Reflection {

  /** The Constant PRIMITIVE_REMAP_TYPES. */
  private static final Map<Class<?>, Class<?>> PRIMITIVE_REMAP_TYPES = getPrimitiveRemapTypes();

  /**
   * Gets the declared field.
   *
   * @param object the object
   * @param value the value
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static Object getDeclaredField(Object object, String value) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException {
    return getDeclaredField(object, value, true);
  }

  /**
   * Gets the declared field.
   *
   * @param object the object
   * @param value the value
   * @param accessible the accessible
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static Object getDeclaredField(Object object, String value, boolean accessible)
      throws NoSuchFieldException, SecurityException, IllegalArgumentException,
      IllegalAccessException {
    Field field = object.getClass().getDeclaredField(value);

    if (!accessible) {
      field.setAccessible(true);
    }

    return field.get(object);
  }

  /**
   * Gets the declared field.
   *
   * @param clazz the clazz
   * @param value the value
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static Object getDeclaredField(Class<?> clazz, String value) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException {
    return getDeclaredField(clazz, value, true);
  }

  /**
   * Gets the declared field.
   *
   * @param clazz the clazz
   * @param value the value
   * @param accessible the accessible
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static Object getDeclaredField(Class<?> clazz, String value, boolean accessible)
      throws NoSuchFieldException, SecurityException, IllegalArgumentException,
      IllegalAccessException {
    Field field = clazz.getDeclaredField(value);

    if (!accessible) {
      field.setAccessible(true);
    }

    if (clazz.isEnum()) {
      return clazz.cast(field.get(clazz));
    } else {
      return field.get(clazz);
    }
  }

  /**
   * Sets the declared field.
   *
   * @param object the object
   * @param name the name
   * @param value the value
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static void setDeclaredField(Object object, String name, Object value)
      throws NoSuchFieldException, SecurityException, IllegalArgumentException,
      IllegalAccessException {
    Field field = object.getClass().getDeclaredField(name);
    field.setAccessible(true);
    field.set(object, value);
  }

  /**
   * Gets the declared method.
   *
   * @param object the object
   * @param method the method
   * @param args the args
   * @return the method
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   */
  public static Method getDeclaredMethod(Object object, String method, Class<?>... args)
      throws NoSuchMethodException, SecurityException {
    if (object instanceof Class) {
      return ((Class<?>) object).getDeclaredMethod(method, args);
    } else {
      return object.getClass().getDeclaredMethod(method, args);
    }
  }

  /**
   * Gets the invoke declared method.
   *
   * @param object the object
   * @param method the method
   * @param args the args
   * @return the invoke method
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static Object getInvokeDeclaredMethod(Object object, String method, Object... args)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    return getInvokeDeclaredMethod(object, method, true, args);
  }

  /**
   * Gets the invoke declared method.
   *
   * @param object the object
   * @param method the method
   * @param remap if true, the classes will be replaced by their primitive type
   * @param args the args
   * @return the invoke method
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static Object getInvokeDeclaredMethod(Object object, String method, boolean remap,
      Object... args) throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    if (args.length > 0) {
      List<Class<?>> classes = new ArrayList<>();

      for (Object temp : args) {
        Class<?> clazz = temp.getClass();

        if (remap && PRIMITIVE_REMAP_TYPES.containsKey(clazz)) {
          classes.add(PRIMITIVE_REMAP_TYPES.get(clazz));
        } else {
          classes.add(clazz);
        }
      }

      return invokeMethod(object,
          getDeclaredMethod(object, method, classes.toArray(new Class<?>[classes.size()])), args);
    } else {
      return invokeMethod(object, getDeclaredMethod(object, method));
    }
  }

  /**
   * Gets the field.
   *
   * @param object the object
   * @param value the value
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static Object getField(Object object, String value) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException {
    return object.getClass().getField(value).get(object);
  }

  /**
   * Gets the field.
   *
   * @param clazz the clazz
   * @param value the value
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static Object getField(Class<?> clazz, String value) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException {
    Field field = clazz.getField(value);

    if (clazz.isEnum()) {
      return clazz.cast(field.get(clazz));
    } else {
      return field.get(clazz);
    }
  }

  /**
   * Sets the field.
   *
   * @param object the object
   * @param name the name
   * @param value the value
   * @return the field
   * @throws NoSuchFieldException the no such field exception
   * @throws SecurityException the security exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws IllegalAccessException the illegal access exception
   */
  public static void setField(Object object, String name, Object value) throws NoSuchFieldException,
      SecurityException, IllegalArgumentException, IllegalAccessException {
    Field field = object.getClass().getField(name);
    field.setAccessible(true);
    field.set(object, value);
  }

  /**
   * Gets the method.
   *
   * @param object the object
   * @param method the method
   * @param args the args
   * @return the method
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   */
  public static Method getMethod(Object object, String method, Class<?>... args)
      throws NoSuchMethodException, SecurityException {
    if (object instanceof Class) {
      return ((Class<?>) object).getMethod(method, args);
    } else {
      return object.getClass().getMethod(method, args);
    }
  }

  /**
   * Invoke method.
   *
   * @param object the object
   * @param method the method
   * @param args the args
   * @return the object
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static Object invokeMethod(Object object, Method method, Object... args)
      throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    return method.invoke(object, args);
  }

  /**
   * Gets the invoke method.
   *
   * @param object the object
   * @param method the method
   * @param args the args
   * @return the invoke method
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static Object getInvokeMethod(Object object, String method, Object... args)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    return getInvokeMethod(object, method, true, args);
  }

  /**
   * Gets the invoke method.
   *
   * @param object the object
   * @param method the method
   * @param remap if true, the classes will be replaced by their primitive type
   * @param args the args
   * @return the invoke method
   * @throws NoSuchMethodException the no such method exception
   * @throws SecurityException the security exception
   * @throws IllegalAccessException the illegal access exception
   * @throws IllegalArgumentException the illegal argument exception
   * @throws InvocationTargetException the invocation target exception
   */
  public static Object getInvokeMethod(Object object, String method, boolean remap, Object... args)
      throws NoSuchMethodException, SecurityException, IllegalAccessException,
      IllegalArgumentException, InvocationTargetException {
    if (args.length > 0) {
      List<Class<?>> classes = new ArrayList<>();

      for (Object temp : args) {
        Class<?> clazz = temp.getClass();

        if (remap && PRIMITIVE_REMAP_TYPES.containsKey(clazz)) {
          classes.add(PRIMITIVE_REMAP_TYPES.get(clazz));
        } else {
          classes.add(clazz);
        }
      }

      return invokeMethod(object,
          getMethod(object, method, classes.toArray(new Class<?>[classes.size()])), args);
    } else {
      return invokeMethod(object, getMethod(object, method));
    }
  }

  /**
   * Gets the found class.
   *
   * @param name the name
   * @return the found class
   */
  public static Class<?> getFoundClass(String name) {
    try {
      return Class.forName(name);
    } catch (ClassNotFoundException ignored) {
    }

    return null;
  }

  /**
   * Gets the primitive remap types.
   *
   * @return the primitive remap types
   */
  private static Map<Class<?>, Class<?>> getPrimitiveRemapTypes() {
    Map<Class<?>, Class<?>> map = new HashMap<>();

    map.put(Boolean.class, Boolean.TYPE);
    map.put(Character.class, Character.TYPE);
    map.put(Byte.class, Byte.TYPE);
    map.put(Short.class, Short.TYPE);
    map.put(Integer.class, Integer.TYPE);
    map.put(Long.class, Long.TYPE);
    map.put(Float.class, Float.TYPE);
    map.put(Double.class, Double.TYPE);
    map.put(Void.class, Void.TYPE);

    return map;
  }
}
