/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.utils;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;

// TODO: Auto-generated Javadoc
/**
 * The Class WrapUtils.
 */
public class WrapUtils {

  /**
   * Wrap string.
   *
   * @param str the str
   * @param colourCode the colour code
   * @param lineLength the line length
   * @param wrapLongWords the wrap long words
   * @return the string[]
   */
  public static String[] wrapString(String str, char colourCode, int lineLength,
      boolean wrapLongWords) {
    // split up into words
    String[] split = WordUtils.wrap(str, lineLength, null, wrapLongWords).split("\\r\\n");
    String[] fixed = new String[split.length];

    // set first element
    fixed[0] = split[0];

    for (int i = 1; i < split.length; i++) {
      String line = split[i];
      String previous = split[i - 1];

      // get last colour from last
      int code = previous.lastIndexOf(colourCode);

      // validate colour
      if (code != -1) {
        char cCode = previous.charAt(code == previous.length() - 1 ? code : code + 1);

        // colour has been split
        if (code == previous.length() - 1) {
          // validate code
          if (ChatColor.getByChar(line.charAt(0)) != null) {
            // remove off end of previous
            fixed[i - 1] = previous.substring(0, previous.length() - 1);

            // add § to start of line
            line = "§" + line;
            split[i] = line; // update for next iteration
          }
        } else {
          // check next line doesn't already have a colour
          if ((line.length() < 2 || line.charAt(0) != colourCode
              || ChatColor.getByChar(line.charAt(1)) == null)
              && ChatColor.getByChar(cCode) != null) {
            line = "§" + cCode + line;
          }
        }
      }

      // update the arrays
      fixed[i] = line;
      split[i] = line;
    }

    // join it all up to return a String
    return fixed;
  }
}
