/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */

package fr.minepod.mpcore.legacy.commands.proxies;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import fr.minepod.mpcore.MPCore;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandExecutorProxy.
 */
public class CommandExecutorProxy {

  /**
   * Adds the executor proxy.
   *
   * @param target the target
   * @return the object
   */
  public static Object addExecutorProxy(Object target) {
    ProxyFactory factory = new ProxyFactory(target);
    factory.addAdvisor(
        new DefaultPointcutAdvisor(new CommandExecutorPointcut(), new CommandExecutorAdvice()));
    factory.setTarget(target);

    return factory.getProxy(target.getClass().getClassLoader());
  }
}


class CommandExecutorAdvice implements MethodInterceptor {
  public Object invoke(MethodInvocation invocation) throws Throwable {
    CommandSender sender = (CommandSender) invocation.getArguments()[0];
    Command command = (Command) invocation.getArguments()[1];
    String[] args = (String[]) invocation.getArguments()[3];

    return CommandMainProxy.filterCommand(invocation, "Executor", sender, command.getName(), args);
  }
}


class CommandExecutorPointcut extends StaticMethodMatcherPointcut {
  public ClassFilter getClassFilter() {
    return new ClassFilter() {
      public boolean matches(Class<?> clazz) {
        if (JavaPlugin.class.isAssignableFrom(clazz)) {
          return true;
        }

        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> implement : interfaces) {
          if (implement == CommandExecutor.class) {
            if (MPCore.getConfigData().isCommandBlockCompatDebugEnabled()) {
              MPCore.get().getLogger().info("Plugin - " + clazz.getCanonicalName() + "-"
                  + implement.getCanonicalName() + " matches: true");
            }

            return true;
          } else {
            if (MPCore.getConfigData().isCommandBlockCompatDebugEnabled()) {
              MPCore.get().getLogger().info("Plugin - " + clazz.getCanonicalName() + "-"
                  + implement.getCanonicalName() + " matches: false");
            }
          }
        }

        return false;
      }
    };
  }

  @Override
  public boolean matches(Method method, Class<?> clazz) {
    boolean result = method.getName() == "onCommand" && method.getReturnType() == boolean.class
        && method.getParameterTypes().length == 4
        && method.getParameterTypes()[0] == CommandSender.class
        && method.getParameterTypes()[1] == Command.class
        && method.getParameterTypes()[2] == String.class
        && method.getParameterTypes()[3] == String[].class;

    if (MPCore.getConfigData().isCommandBlockCompatDebugEnabled()) {
      MPCore.get().getLogger().info(
          "Plugin - " + clazz.getCanonicalName() + "#" + method.getName() + " matches: " + result);
    }

    return result;
  }
}
