/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */

package fr.minepod.mpcore.legacy.commands.proxies;

import org.aopalliance.intercept.MethodInvocation;
import org.bukkit.ChatColor;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.api.functions.FunctionsManager;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandMainProxy.
 */
public class CommandMainProxy {

  /**
   * Filter command.
   *
   * @param invocation the invocation
   * @param sub the sub
   * @param sender the sender
   * @param command the command
   * @param args the args
   * @return the object
   * @throws Throwable the throwable
   */
  public static Object filterCommand(MethodInvocation invocation, String sub, CommandSender sender,
      String command, String[] args) throws Throwable {
    boolean pass = false;
    if (MPCore.getConfigData().isCommandBlockCompatBlockWhiteListEnabled()
        && sender instanceof BlockCommandSender) {
      for (String pattern : MPCore.getConfigData().getCommandBlockCompatBlockWhiteListCommands()) {
        if (FunctionsManager.isCommandMatchingPattern(command, args, pattern.split(" "))) {
          pass = true;
          break;
        }
      }

      debug(pass, sub, "block", command, args);

      if (!pass) {
        MPCore.getMessages().sendFormatted(sender,
            ChatColor.RED + "This command is disabled in commandblocks.");
        return true;
      }
    } else if (MPCore.getConfigData().isCommandBlockCompatOpWhiteListEnabled()
        && sender instanceof Player) {
      Player player = (Player) sender;

      if (player.isOp() && !MPCore.getConfigData().getCommandBlockCompatOpBypassUUIDs()
          .contains(player.getUniqueId().toString())) {
        for (String plugin : MPCore.getConfigData().getCommandBlockCompatOpWhiteListPlugins()) {
          if (invocation.getThis().getClass().getCanonicalName().toLowerCase()
              .startsWith(plugin.toLowerCase())) {
            pass = true;
            break;
          }
        }

        if (!pass) {
          for (String pattern : MPCore.getConfigData().getCommandBlockCompatOpWhiteListCommands()) {
            if (FunctionsManager.isCommandMatchingPattern(command, args, pattern.split(" "))) {
              pass = true;
              break;
            }
          }
        }

        if (MPCore.getConfigData().isCommandBlockCompatDebugEnabled()) {
          debug(pass, sub, "OP", command, args);
        }

        if (!pass) {
          MPCore.getMessages().sendFormatted(player,
              ChatColor.RED + "This command is disabled without specific permissions.");
          return true;
        }
      }
    }

    return invocation.proceed();
  }

  /**
   * Debug.
   *
   * @param pass the pass
   * @param sub the sub
   * @param type the type
   * @param label the label
   * @param args the args
   */
  private static void debug(boolean pass, String sub, String type, String label, String[] args) {
    if (MPCore.getConfigData().isCommandBlockCompatDebugEnabled()) {
      StringBuilder builder;
      if (!pass) {
        builder = new StringBuilder(sub + " - Prevented " + type + " command '/");
      } else {
        builder = new StringBuilder(sub + " - Allowed " + type + " command '/");
      }

      builder.append(label);

      for (String arg : args) {
        builder.append(" ");
        builder.append(arg);
      }

      builder.append("'.");

      MPCore.get().getLogger().info("Common - " + builder.toString());
    }
  }
}
