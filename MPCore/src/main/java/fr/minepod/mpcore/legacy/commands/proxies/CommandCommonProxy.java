/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */

package fr.minepod.mpcore.legacy.commands.proxies;

import java.lang.reflect.Method;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.bukkit.command.CommandSender;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

import fr.minepod.mpcore.MPCore;

// TODO: Auto-generated Javadoc
/**
 * The Class CommandCommonProxy.
 */
public class CommandCommonProxy {

  /**
   * Adds the common proxy.
   *
   * @param target the target
   * @return the object
   */
  public static Object addCommonProxy(Object target) {
    ProxyFactory factory = new ProxyFactory(target);
    factory.addAdvisor(
        new DefaultPointcutAdvisor(new CommandCommonPointcut(), new CommandCommonAdvice()));
    factory.setTarget(target);

    return factory.getProxy(target.getClass().getClassLoader());
  }
}


class CommandCommonAdvice implements MethodInterceptor {
  public Object invoke(MethodInvocation invocation) throws Throwable {
    CommandSender sender = (CommandSender) invocation.getArguments()[0];
    String label = (String) invocation.getArguments()[1];
    String[] args = (String[]) invocation.getArguments()[2];

    return CommandMainProxy.filterCommand(invocation, "Common", sender, label, args);
  }
}


class CommandCommonPointcut extends StaticMethodMatcherPointcut {
  public ClassFilter getClassFilter() {
    return new ClassFilter() {
      public boolean matches(Class<?> clazz) {
        return true;
      }
    };
  }

  @Override
  public boolean matches(Method method, Class<?> clazz) {
    boolean result = method.getName() == "execute" && method.getReturnType() == boolean.class
        && method.getParameterTypes().length == 3
        && method.getParameterTypes()[0] == CommandSender.class
        && method.getParameterTypes()[1] == String.class
        && method.getParameterTypes()[2] == String[].class;

    if (MPCore.getConfigData().isCommandBlockCompatDebugEnabled()) {
      MPCore.get().getLogger().info(
          "Common - " + clazz.getCanonicalName() + "#" + method.getName() + " matches: " + result);
    }

    return result;
  }
}
