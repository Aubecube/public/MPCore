/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore.legacy.commands.proxies;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.SimpleCommandMap;

import fr.minepod.mpcore.MPCore;
import fr.minepod.mpcore.utils.Reflection;

// TODO: Auto-generated Javadoc
/**
 * The Class ProxyKnownCommands.
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public class ProxyKnownCommands<K, V> extends HashMap<K, V> implements Map<K, V> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5954097711488033011L;

  /**
   * Put.
   *
   * @param key the key
   * @param value the value
   * @return the v
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.util.HashMap#put(java.lang.Object, java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  @Override
  public V put(K key, V value) {
    Class<?> clazz = value.getClass();

    if (!MPCore.getConfigData().isCommandBlockCompatDisabled()) {
      try {
        if (value instanceof PluginCommand) {
          PluginCommand command = (PluginCommand) value;

          if (!MPCore.getConfigData().isCommandBlockCompatSilent()) {
            MPCore.get().getLogger().info("Trying to proxy executor command '/" + key + "' for "
                + command.getPlugin().getName() + ".");
          }

          command.setExecutor(
              (CommandExecutor) CommandExecutorProxy.addExecutorProxy(command.getExecutor()));

          value = (V) command;
        } else {
          if (Modifier.isFinal(clazz.getModifiers())) {
            if (!MPCore.getConfigData().isCommandBlockCompatSilent()) {
              MPCore.get().getLogger().info("Unable to proxy final command '/" + key + "'.");
            }
          } else if (!(Command.class.isAssignableFrom(clazz))
              || clazz.getCanonicalName().startsWith("org.bukkit")
              || clazz.getCanonicalName().startsWith("org.spigotmc")) {
            if (!MPCore.getConfigData().isCommandBlockCompatSilent()) {
              MPCore.get().getLogger().info("Unable to proxy native command '/" + key + "'.");
            }
          } else {
            if (!MPCore.getConfigData().isCommandBlockCompatSilent()) {
              MPCore.get().getLogger().info("Trying to proxy common command '/" + key + "'.");
            }

            Object proxy = CommandCommonProxy.addCommonProxy(value);
            if (Command.class.isAssignableFrom(proxy.getClass())) {
              value = (V) proxy;
            } else {
              if (!MPCore.getConfigData().isCommandBlockCompatSilent()) {
                MPCore.get().getLogger().warning("Proxy class missmatch!");
              }
            }
          }
        }
      } catch (Exception e) {
        e.printStackTrace();
        MPCore.get().getLogger().warning("Error with class: " + clazz.getCanonicalName());
      }
    }

    super.put(key, value);
    return value;
  }

  /**
   * Register.
   */
  public static void register() {
    try {
      SimpleCommandMap map = (SimpleCommandMap) Reflection
          .getDeclaredField(MPCore.get().getServer().getPluginManager(), "commandMap", false);

      @SuppressWarnings("unchecked")
      Map<String, Command> commands =
          (Map<String, Command>) Reflection.getDeclaredField(map, "knownCommands", false);

      Map<String, Command> proxied = new ProxyKnownCommands<>();

      for (Entry<String, Command> entry : commands.entrySet()) {
        proxied.put(entry.getKey(), entry.getValue());
      }

      Reflection.setDeclaredField(map, "knownCommands", proxied);

    } catch (NoSuchFieldException | SecurityException | IllegalArgumentException
        | IllegalAccessException e) {
      e.printStackTrace();
    }
  }
}
