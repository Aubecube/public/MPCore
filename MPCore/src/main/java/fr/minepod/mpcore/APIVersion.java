/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.mpcore;

import fr.minepod.mpcore.api.plugins.MPCorePluginBase;
import lombok.AllArgsConstructor;
import lombok.Getter;

// TODO: Auto-generated Javadoc

/**
 * The Enum APIVersion.
 */

/**
 * Instantiates a new API version.
 *
 * @param internal the internal
 */
@AllArgsConstructor

/**
 * Gets the internal.
 *
 * @return the internal
 */
@Getter
public enum APIVersion {

  /** The Akihabara version. */
  @Deprecated
  AKIHABARA("1.5.x"),
  /** The Buzen version. */
  @Deprecated
  BUZEN("1.6.x"),
  /** The Chino version. */
  @Deprecated
  CHINO("1.7.x"),
  /** The Daikanyama version. */
  @Deprecated
  DAIKANYAMA("1.8.x"),
  /**
   * The Engelberg version (1.22.x).
   * 
   * Signals breaking changes in {@link MPCorePluginBase}.
   */
  ENGELBERG("1.22.x");

  /** The internal. */
  private String internal;
}
