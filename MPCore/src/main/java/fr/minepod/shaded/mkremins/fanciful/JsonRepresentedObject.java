/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.shaded.mkremins.fanciful;

import java.io.IOException;

import com.google.gson.stream.JsonWriter;

// TODO: Auto-generated Javadoc
/**
 * Represents an object that can be serialized to a JSON writer instance.
 */
interface JsonRepresentedObject {

  /**
   * Writes the JSON representation of this object to the specified writer.
   * 
   * @param writer The JSON writer which will receive the object.
   * @throws IOException If an error occurs writing to the stream.
   */
  public void writeJson(JsonWriter writer) throws IOException;

}
