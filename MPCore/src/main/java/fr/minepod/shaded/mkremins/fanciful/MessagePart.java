/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.shaded.mkremins.fanciful;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.gson.stream.JsonWriter;

// TODO: Auto-generated Javadoc
/**
 * Internal class: Represents a component of a JSON-serializable {@link FancyMessage}.
 */
final class MessagePart implements JsonRepresentedObject, ConfigurationSerializable, Cloneable {

  /** The color. */
  ChatColor color = ChatColor.WHITE;

  /** The styles. */
  ArrayList<ChatColor> styles = new ArrayList<ChatColor>();

  /** The hover action name. */
  String clickActionName = null, clickActionData = null, hoverActionName = null;

  /** The hover action data. */
  JsonRepresentedObject hoverActionData = null;

  /** The text. */
  TextualComponent text = null;

  /** The insertion data. */
  String insertionData = null;

  /** The translation replacements. */
  ArrayList<JsonRepresentedObject> translationReplacements = new ArrayList<JsonRepresentedObject>();

  /**
   * Instantiates a new message part.
   *
   * @param text the text
   */
  MessagePart(final TextualComponent text) {
    this.text = text;
  }

  /**
   * Instantiates a new message part.
   */
  MessagePart() {
    this.text = null;
  }

  /**
   * Checks for text.
   *
   * @return true, if successful
   */
  boolean hasText() {
    return text != null;
  }

  /**
   * Clone.
   *
   * @return the message part
   * @throws CloneNotSupportedException the clone not supported exception
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#clone()
   */
  @Override
  @SuppressWarnings("unchecked")
  public MessagePart clone() throws CloneNotSupportedException {
    MessagePart obj = (MessagePart) super.clone();
    obj.styles = (ArrayList<ChatColor>) styles.clone();
    if (hoverActionData instanceof JsonString) {
      obj.hoverActionData = new JsonString(((JsonString) hoverActionData).getValue());
    } else if (hoverActionData instanceof FancyMessage) {
      obj.hoverActionData = ((FancyMessage) hoverActionData).clone();
    }
    obj.translationReplacements =
        (ArrayList<JsonRepresentedObject>) translationReplacements.clone();
    return obj;

  }

  /** The Constant stylesToNames. */
  static final BiMap<ChatColor, String> stylesToNames;

  static {
    ImmutableBiMap.Builder<ChatColor, String> builder = ImmutableBiMap.builder();
    for (final ChatColor style : ChatColor.values()) {
      if (!style.isFormat()) {
        continue;
      }

      String styleName;
      switch (style) {
        case MAGIC:
          styleName = "obfuscated";
          break;
        case UNDERLINE:
          styleName = "underlined";
          break;
        default:
          styleName = style.name().toLowerCase();
          break;
      }

      builder.put(style, styleName);
    }
    stylesToNames = builder.build();
  }

  /**
   * Write json.
   *
   * @param json the json
   */
  /*
   * (non-Javadoc)
   * 
   * @see
   * fr.minepod.shaded.mkremins.fanciful.JsonRepresentedObject#writeJson(com.google.gson.stream.
   * JsonWriter)
   */
  public void writeJson(JsonWriter json) {
    try {
      json.beginObject();
      text.writeJson(json);
      json.name("color").value(color.name().toLowerCase());
      for (final ChatColor style : styles) {
        json.name(stylesToNames.get(style)).value(true);
      }
      if (clickActionName != null && clickActionData != null) {
        json.name("clickEvent").beginObject().name("action").value(clickActionName).name("value")
            .value(clickActionData).endObject();
      }
      if (hoverActionName != null && hoverActionData != null) {
        json.name("hoverEvent").beginObject().name("action").value(hoverActionName).name("value");
        hoverActionData.writeJson(json);
        json.endObject();
      }
      if (insertionData != null) {
        json.name("insertion").value(insertionData);
      }
      if (translationReplacements.size() > 0 && text != null
          && TextualComponent.isTranslatableText(text)) {
        json.name("with").beginArray();
        for (JsonRepresentedObject obj : translationReplacements) {
          obj.writeJson(json);
        }
        json.endArray();
      }
      json.endObject();
    } catch (IOException e) {
      Bukkit.getLogger().log(Level.WARNING, "A problem occured during writing of JSON string", e);
    }
  }

  /**
   * Serialize.
   *
   * @return the map
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.configuration.serialization.ConfigurationSerializable#serialize()
   */
  public Map<String, Object> serialize() {
    HashMap<String, Object> map = new HashMap<String, Object>();
    map.put("text", text);
    map.put("styles", styles);
    map.put("color", color.getChar());
    map.put("hoverActionName", hoverActionName);
    map.put("hoverActionData", hoverActionData);
    map.put("clickActionName", clickActionName);
    map.put("clickActionData", clickActionData);
    map.put("insertion", insertionData);
    map.put("translationReplacements", translationReplacements);
    return map;
  }

  /**
   * Deserialize.
   *
   * @param serialized the serialized
   * @return the message part
   */
  @SuppressWarnings("unchecked")
  public static MessagePart deserialize(Map<String, Object> serialized) {
    MessagePart part = new MessagePart((TextualComponent) serialized.get("text"));
    part.styles = (ArrayList<ChatColor>) serialized.get("styles");
    part.color = ChatColor.getByChar(serialized.get("color").toString());
    part.hoverActionName = (String) serialized.get("hoverActionName");
    part.hoverActionData = (JsonRepresentedObject) serialized.get("hoverActionData");
    part.clickActionName = (String) serialized.get("clickActionName");
    part.clickActionData = (String) serialized.get("clickActionData");
    part.insertionData = (String) serialized.get("insertion");
    part.translationReplacements =
        (ArrayList<JsonRepresentedObject>) serialized.get("translationReplacements");
    return part;
  }

  static {
    ConfigurationSerialization.registerClass(MessagePart.class);
  }

}
