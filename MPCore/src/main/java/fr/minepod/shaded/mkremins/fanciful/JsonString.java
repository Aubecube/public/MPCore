/*
 * MPCore - Powerful library for Bukkit plugins. Copyright (C) 2014-2018 __Asuna
 * <darkshimy00@gmail.com>
 * 
 * This file is part of MPCore.
 * 
 * MPCore is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 * 
 * MPCore is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 * the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <http://www.gnu.org/licenses/>.
 */
package fr.minepod.shaded.mkremins.fanciful;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

import com.google.gson.stream.JsonWriter;

// TODO: Auto-generated Javadoc
/**
 * Represents a JSON string value. Writes by this object will not write name values nor begin/end
 * objects in the JSON stream. All writes merely write the represented string value.
 */
final class JsonString implements JsonRepresentedObject, ConfigurationSerializable {

  /** The value. */
  private String _value;

  /**
   * Instantiates a new json string.
   *
   * @param value the value
   */
  public JsonString(CharSequence value) {
    _value = value == null ? null : value.toString();
  }

  /**
   * Write json.
   *
   * @param writer the writer
   * @throws IOException Signals that an I/O exception has occurred.
   */
  /*
   * (non-Javadoc)
   * 
   * @see
   * fr.minepod.shaded.mkremins.fanciful.JsonRepresentedObject#writeJson(com.google.gson.stream.
   * JsonWriter)
   */
  @Override
  public void writeJson(JsonWriter writer) throws IOException {
    writer.value(getValue());
  }

  /**
   * Gets the value.
   *
   * @return the value
   */
  public String getValue() {
    return _value;
  }

  /**
   * Serialize.
   *
   * @return the map
   */
  /*
   * (non-Javadoc)
   * 
   * @see org.bukkit.configuration.serialization.ConfigurationSerializable#serialize()
   */
  public Map<String, Object> serialize() {
    HashMap<String, Object> theSingleValue = new HashMap<String, Object>();
    theSingleValue.put("stringValue", _value);
    return theSingleValue;
  }

  /**
   * Deserialize.
   *
   * @param map the map
   * @return the json string
   */
  public static JsonString deserialize(Map<String, Object> map) {
    return new JsonString(map.get("stringValue").toString());
  }

  /**
   * To string.
   *
   * @return the string
   */
  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return _value;
  }

}
